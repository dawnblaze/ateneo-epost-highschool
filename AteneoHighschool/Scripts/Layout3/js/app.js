'use strict';


// Declare app level module which depends on filters, and services
// angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.controllers']).
angular.module('myApp', ['myApp.controllers','myApp.filters']).
  config(['$routeProvider', function($routeProvider) {
//    $routeProvider.when('/', {templateUrl: 'partials/home.html', controller: 'HomeController'}); //landing page after login
    $routeProvider.when('/', {templateUrl: '/Content/Communication/list.html', controller: 'CommunicationListController'}); //list of communication
    // $routeProvider.when('/communication/create', {templateUrl: 'partials/create.html', controller: 'CommunicationCreateController'});
    // $routeProvider.when('/communication/read/:id', {templateUrl: 'partials/read.html', controller: 'CommunicationReadController'}); //view one entry
    // $routeProvider.when('/communication/update/:id', {templateUrl: 'partials/update.html', controller: 'CommunicationUpdateController'});
    $routeProvider.otherwise({redirectTo: '/'});
  }]);
