'use strict';

/* Filters */

angular.module('myApp.filters', [])
    // .filter('interpolate', ['version', function(version) {
    //     return function(text) {
    //         return String(text).replace(/\%VERSION\%/mg, version);
    //     };
    // }])
    .filter('messageView', function() {
        return function(msg) {
            if (msg) {
                var output;
                output = "";
                switch (msg.type) {
                case 'Personal':
                    output += "<p>" + msg.message + "</p>";
                    break;
                case 'PNS':
                    output += "<p>This is to inform you that your child, <strong class=\"text-info\">" + msg.child.name + "</strong> of ";
                    output += "<strong class=\"text-info\">" + msg.child.grade + "-" + msg.child.section + "</strong> at this point is ";
                    output += (msg.child.status === 1) ? "<strong class=\"text-success\">performing well</strong> in " : "<strong class=\"text-danger\">having difficulty</strong> with ";
                    output += "<strong class=\"text-info\">" + msg.subject + "</strong>.</p>";
                    output += "<p>" + msg.report + "</p>";
                    break;
                }
                return output;
            }
        };
    });