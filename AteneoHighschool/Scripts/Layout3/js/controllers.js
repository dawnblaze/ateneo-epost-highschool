'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
    .controller('CommunicationListController', ['$scope', '$http', '$window', '$routeParams', '$location', function ($scope, $http, $window, $routeParams, $location) {
    	$http.get('/Communication/Messages').success(function (data) {
    		console.log(data);
    		$window.messages = data;
    		$scope.messages = $window.messages;

    		$scope.newMessage = {
    			id: $window.messages.length + 1,
    			is_unread: true,
    			date: Date.now(),
    			type: "PNS",
    			sender: "Mrs. Mae Divina Velasco",
    			recipients: [
                "Mrs. Mae Divina Velasco"
            ],
    			child: {
    				name: "",
    				grade: 5,
    				section: "Faber",
    				status: 1 // 0 = having difficulty; 1 = performing well
    			},
    			report: "",
    			subject: "CLF"
    		};
    	});
    	$scope.type = $routeParams.type;

    	$scope.mode = 'read';

    	$scope.filterMessages = function (type) {
    		$scope.mode = 'read';
    		$scope.type = type;
    		if (!type) {
    			$scope.messages = $window.messages;
    		} else {
    			$scope.messages = _.filter($window.messages, function (message) {
    				return message.type === type;
    			});
    		}
    		$scope.currentMessage = null;
    	};

    	$scope.countMessages = function (type) {
    		if (!type) {
    			return _.filter($window.messages, function (message) {
    				return (message.is_unread);
    			}).length;
    		} else {
    			return _.filter($window.messages, function (message) {
    				return (message.type === type && (message.is_unread));
    			}).length;
    		}
    	};

    	$scope.getMessageRowClass = function (is_unread) {
    		if (is_unread) {
    			return "warning text-warning";
    		}
    	};

    	$scope.currentMessage = null;

    	$scope.displayMessage = function (id) {
    		$scope.currentMessage = _.filter($window.messages, function (message) {
    			return message.id === id;
    		})[0];
    		$window.messages[_.indexOf($window.messages, $scope.currentMessage)].is_unread = false;
    		// console.log(id);
    	};

    	$scope.showCreateForm = function () {
    		$scope.mode = 'create';
    	};

    	$scope.saveNewMessage = function () {
    		$window.messages.push($scope.newMessage);
    		console.log($window.messages);
    		$scope.newMessage = {
    			id: $window.messages.length + 1,
    			is_unread: true,
    			date: Date.now(),
    			type: "PNS",
    			sender: "Mrs. Mae Divina Velasco",
    			recipients: [
                    "Mrs. Mae Divina Velasco"
                ],
    			child: {
    				name: "",
    				grade: 5,
    				section: "Faber",
    				status: 1 // 0 = having difficulty; 1 = performing well
    			},
    			report: "",
    			subject: "CLF"
    		};
    		$scope.mode = 'read'
    	};
    } ]);