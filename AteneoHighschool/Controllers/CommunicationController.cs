﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;
using AteneoHighschool.Models;
using AteneoHighschool.DataAccess.Utility;

namespace AteneoHighschool.Controllers
{
    public class CommunicationController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

            if (user != null)
            {
                if (!user.IsConfirmed)
                {
                    ViewBag.EmailConfirmation = "Not Confirmed.";

                    return RedirectToAction("Index", "Home");
                }
            }

            var pns = new List<ParentNotificationSlip>();

            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);
                pns.AddRange(_unitOfWork.ParentNotificationSlipRepository.GetList(q => q.SenderId == employee.EmployeeId));
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
            {
                var parent = _unitOfWork.ParentRepository.GetOne(q => q.PersonId == user.UserId);
                pns.AddRange(_unitOfWork.ParentNotificationSlipRepository.GetList(q => q.RecipientId == parent.PersonId));
            }
            ViewBag.CountAllUnRead = pns.Count(q => q.IsRead == false);
            ViewBag.CountPNSUnRead = pns.Count(q => q.IsRead == false);
            return View();
        }

        public ActionResult ListCommunication()
        {
            var pns = new List<ParentNotificationSlip>();
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);
                pns.AddRange(_unitOfWork.ParentNotificationSlipRepository.GetList(q => q.SenderId == employee.EmployeeId));
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
            {
                var parent = _unitOfWork.ParentRepository.GetOne(q => q.PersonId == user.UserId);
                pns.AddRange(_unitOfWork.ParentNotificationSlipRepository.GetList(q => q.RecipientId == parent.ParentId));
            }
            ViewBag.CountAllUnRead = pns.Count(q => q.IsRead == false);
            //code below is equivalent to code above
            //ViewBag.CountAllUnRead = pns.Where(q => q.IsRead == false).Count();
            ViewBag.CountPNSUnRead = pns.Count(q => q.IsRead == false);
            return PartialView("_CommunicationGrid", pns);
        }

        public ActionResult ListNotificationSlip()
        {
            var pns = new List<ParentNotificationSlip>();
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);
                pns.AddRange(_unitOfWork.ParentNotificationSlipRepository.GetList(q => q.SenderId == employee.EmployeeId));
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
            {
                var parent = _unitOfWork.ParentRepository.GetOne(q => q.PersonId == user.UserId);

                pns.AddRange(_unitOfWork.ParentNotificationSlipRepository.GetList(q => q.RecipientId == parent.ParentId));
            }
            ViewBag.CountAllUnRead = pns.Count(q => q.IsRead == false);
            ViewBag.CountPNSUnRead = pns.Count(q => q.IsRead == false);
            return PartialView("_NotificationSlipGrid", pns);
        }

        public ActionResult CommunicationDetail(Guid communicationId)
        {
            var model =
                _unitOfWork.ParentNotificationSlipRepository.GetOne(q => q.ParentNotificationSlipId == communicationId,
                    includeProperties: "CurriculumClass, Curriculum, Parent, Parent.Person");
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
            {
                if (model.IsRead == false && model.ReadDate == null)
                {
                    model.ReadDate = DateTime.Now;
                    model.IsRead = true;
                    _unitOfWork.ParentNotificationSlipRepository.Update(model);
                }
            }
            return PartialView("_NotificationDetail", model);
        }

        [HttpGet]
        public ActionResult CreateNotification()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

                var person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == user.UserId);

                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == person.PersonId);

                var pns = new ParentNotificationSlip
                {
                    SenderId = employee.EmployeeId,
                    Parent = null,
                    Student = null,
                    Curriculum = null,
                    CurriculumClass = null,
                };
                return PartialView("_CreatePNS", pns);
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new PNS.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        public ActionResult CreateNotification(ParentNotificationSlip pns)
        {
            try
            {
                #region Too much process

                var group =
                    _unitOfWork.GroupRepository.GetOne(q => q.GroupName == pns.CurriculumClass.CurriculumClassName);
                var curriculum =
                    _unitOfWork.CurriculumRepository.GetOne(q => q.CurriculumName == pns.Curriculum.CurriculumName);
                var curriculumClass =
                    _unitOfWork.CurriculumClassRepository.GetOne(
                        q => q.CurriculumId == curriculum.CurriculumId && q.GroupId == group.GroupId);
                var student = _unitOfWork.StudentRepository.GetOne(
                    q => q.Person.FullName == pns.Student.Person.FullName);
                var parents = _unitOfWork.RelationshipRepository.GetList(q => q.StudentId == student.StudentId);
                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == pns.SenderId);
                var person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == employee.PersonId);

                foreach (var parentNotificationSlip in parents.Select(item => new ParentNotificationSlip
                {
                    SenderId = pns.SenderId,
                    RecipientId = item.ParentId,
                    StudentId = student.StudentId,
                    CurriculumId = curriculum.CurriculumId,
                    CurriculumClassId = curriculumClass.CurriculumClassId,
                    MessageContent = pns.MessageContent,
                    SentDate = DateTime.Now,
                    SenderName = person.FullName,
                    IsRead = false,
                }))
                {
                    _unitOfWork.ParentNotificationSlipRepository.Insert(parentNotificationSlip);
                    SendEmialNotificationToParent(parentNotificationSlip);
                    SendEmialNotificationToTeacher(parentNotificationSlip);
                }
                //code below is equivalent to code above
                //foreach (var item in parents)
                //{
                //    var parentNotificationSlip = new ParentNotificationSlip
                //    {
                //        SenderId = pns.SenderId,
                //        RecipientId = item.ParentId,
                //        StudentId = student.StudentId,
                //        CurriculumId = curriculum.CurriculumId,
                //        CurriculumClassId = curriculumClass.CurriculumClassId,
                //    };

                //    _unitOfWork.ParentNotificationSlipRepository.Insert(parentNotificationSlip);
                //}

                #endregion Too much process

                return RedirectToAction("Index");
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("CreateNotificationFailed");
            }
        }

        public JsonResult GetStudents(string term)
        {
            try
            {
                #region Temporary Code, too much process

                var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);

                var classTeacher = _unitOfWork.ClassTeacherRepository.GetList(q => q.EmployeeId == employee.EmployeeId);

                var curriculumClassList =
                    classTeacher.Select(
                        item =>
                            _unitOfWork.CurriculumClassRepository.GetOne(
                                q => q.CurriculumClassId == item.CurriculumClassId)).ToList();

                var groupList =
                    curriculumClassList.Select(
                        item => _unitOfWork.GroupRepository.GetOne(q => q.GroupId == item.GroupId)).ToList();
                //code below is equivalent to code above
                //foreach (var item in curriculumClassList)
                //{
                //    var x = _unitOfWork.GroupRepository.GetOne(q => q.GroupId == item.GroupId);
                //    groupList.Add(x);
                //}

                var groupMemberList = new List<GroupMember>();
                foreach (var item in groupList)
                {
                    var x = _unitOfWork.GroupMemberRepository.GetList(q => q.GroupId == item.GroupId);
                    groupMemberList.AddRange(x);
                }

                var studentList =
                    groupMemberList.Select(
                        item =>
                            _unitOfWork.StudentRepository.GetOne(q => q.StudentId == item.StudentId,
                                includeProperties: "Person")).ToList();

                var results = new List<Student>();
                results.AddRange(studentList);

                var students =
                    results.Where(q => q.Person.FullName.ToLower().Contains(term.ToLower()))
                        .Select(item => item.Person.FullName)
                        .ToList();

                #endregion Temporary Code

                return Json(students, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                throw;
            }
        }

        public ActionResult GetChildSection(string childName)
        {
            string result = "";
            try
            {
                #region Temporary Code, too much process... I think. Too much process. Final.

                //var student = _personService.GetStudent(ChildName);
                //var person = _unitOfWork.PersonRepository.GetOne(q => q.FullName == childName);
                var person =
                    _unitOfWork.PersonRepository.GetOne(
                        q => q.LastName + ", " + q.FirstName + " " + q.MiddleName == childName);
                var student = _unitOfWork.StudentRepository.GetOne(q => q.PersonId == person.PersonId);
                var childId = student.StudentId;

                var groupMember = _unitOfWork.GroupMemberRepository.GetOne(q => q.StudentId == childId);
                var group = _unitOfWork.GroupRepository.GetOne(q => q.GroupId == groupMember.GroupId);

                result = group != null ? group.GroupName : "";
                //code below is equivalent to code above
                //if (group != null)
                //{
                //    result = group.GroupName;
                //}
                //else
                //{
                //    result = "";
                //}

                #endregion Temporary Code

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCurriculums(string term)
        {
            try
            {
                #region Temporary Code, too much process

                var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);
                var classTeacher = _unitOfWork.ClassTeacherRepository.GetList(q => q.EmployeeId == employee.EmployeeId);
                var curriculumClassList =
                    classTeacher.Select(
                        item =>
                            _unitOfWork.CurriculumClassRepository.GetOne(
                                q => q.CurriculumClassId == item.CurriculumClassId)).ToList();
                var curriculumList = new List<Curriculum>();
                foreach (var item in curriculumClassList)
                {
                    var x = _unitOfWork.CurriculumRepository.GetList(q => q.CurriculumId == item.CurriculumId);
                    curriculumList.AddRange(x);
                }
                var results = curriculumList.Distinct();
                var curriculums =
                    results.Where(q => q.CurriculumName.ToLower().Contains(term.ToLower()))
                        .Select(item => item.CurriculumName)
                        .ToList();

                #endregion Temporary Code, too much process

                return Json(curriculums, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                throw;
            }
        }

        public JsonResult GetStudentId(string childName)
        {
            try
            {
                var student = _unitOfWork.StudentRepository.GetOne(q => q.Person.FullName == childName);
                return Json(student.StudentId, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                throw;
            }
        }

        [HttpGet]
        public ActionResult EmailSupport()
        {
            return PartialView("_EmailSupport");
        }

        [HttpPost]
        public ActionResult EmailSupport(EmailSupport emailSupport)
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

            #region Code not secured, need revision
            emailSupport.Subject += " From: " + user.UserName;

            emailSupport.Body = emailSupport.Body.Replace("'", "''");

            SendAsEmail("ateneoepost.xhimera@gmail.com", emailSupport.Subject, emailSupport.Body);
            #endregion Code not secured, need revision

            return RedirectToAction("Index", "Home");
        }

        public void SendEmialNotificationToParent(ParentNotificationSlip pns)
        {
            #region Code not secured, need revision
            var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == pns.SenderId, includeProperties: "Person");
            var parent = _unitOfWork.ParentRepository.GetOne(q => q.ParentId == pns.RecipientId);
            var parentEmail = _unitOfWork.UserRepository.GetOne(q => q.UserId == parent.PersonId);
            var email = parentEmail.Email;
            var emailSupport = new EmailSupport();
            emailSupport.Subject += "You have a PNS Notification!";
            emailSupport.Body = "Hello, \nYou have received a notification from " + employee.Person.FullName +
                                ". Please log in to ePost to view the message. \nThanks,\nePost Support Team";
            emailSupport.Body = emailSupport.Body.Replace("'", "''");
            SendAsEmail(email, emailSupport.Subject, emailSupport.Body);
            #endregion Code not secured, need revision
        }

        public void SendEmialNotificationToTeacher(ParentNotificationSlip pns)
        {
            #region Code not secured, need revision
            var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == pns.SenderId, includeProperties: "Person");
            var parent = _unitOfWork.ParentRepository.GetOne(q => q.ParentId == pns.RecipientId, includeProperties: "Person");
            var employeeEmail = _unitOfWork.UserRepository.GetOne(q => q.UserId == employee.PersonId);
            var email = employeeEmail.Email;
            var emailSupport = new EmailSupport();
            emailSupport.Subject += "You have a sent PNS!";
            emailSupport.Body = "Hello, \nYou have sent a notification to " + parent.Person.FullName +
                                "\n- ePost Support Team";
            emailSupport.Body = emailSupport.Body.Replace("'", "''");
            SendAsEmail(email, emailSupport.Subject, emailSupport.Body);
            #endregion Code not secured, need revision
        }

        //Need to transfer SendAsEmail method to a more secure spot
        public void SendAsEmail(string recipient, string subject, string body)
        {
            const string profile = "Ateneo Epost";

            string commandText = string.Format(@"EXEC msdb.dbo.sp_send_dbmail @profile_name = '{0}',
            @recipients = '{1}', @subject = '{2}', @body = '{3}'", profile, recipient, subject, body);

            AdoUtility.ExecuteNonQuery(commandText);
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}

