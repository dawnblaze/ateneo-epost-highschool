﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Routing;
using AteneoHighschool.Data;
using AteneoHighschool.Data.Access;
using AteneoHighschool.DataAccess;
using AteneoHighschool.DataAccess.Utility;
using AteneoHighschool.Models;

namespace AteneoHighschool.Controllers
{
    public class AccountController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private GenericMembershipProvider _membershipProvider = new GenericMembershipProvider();
        private GenericRoleProvider _roleProvider = new GenericRoleProvider();

        protected override void Initialize(RequestContext requestContext)
        {
            if (_membershipProvider == null)
            {
                _membershipProvider = new GenericMembershipProvider();
            }

            if (_roleProvider == null)
            {
                _roleProvider = new GenericRoleProvider();
            }

            base.Initialize(requestContext);
        }

        [AllowAnonymous]
        public ActionResult LogIn()
        {
            var model = new LogIn();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogIn(LogIn logIn)
        {
            if (ModelState.IsValid)
            {
                if (_membershipProvider.ValidateUser(logIn.UserName, logIn.Password))
                {
                    FormsAuthentication.SetAuthCookie(logIn.UserName, logIn.RememberMe);
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(logIn);
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult SendEmailConfirmation()
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

            string domainName = /*Request.Url.Scheme + "//" + */Request.Url.DnsSafeHost + Request.ApplicationPath.TrimEnd('/') + ":55106/";

            var emailSupport = new EmailSupport();

            emailSupport.Subject += "Email Confirmation";
            emailSupport.Body =
                "\nWelcome to Ateneoepost!\n\nThank you for registering with us.\n\nYour account details are as follow:" +
                "\n\nUsername: " + user.UserName +
                "\n\nThe final step to complete your registration is to verify that you own this e-mail address by clicking here: " +
                domainName + "Account/ConfirmEmail/" + user.UserId.ToString() +
                "\n\nCheers,\n\nxhimera\nThis is an automated system generated message. Please do not reply to this e-mail message.";

            emailSupport.Body = emailSupport.Body.Replace("'", "''");

            SendAsEmail(user.Email, emailSupport.Subject, emailSupport.Body);

            return PartialView("_ResendEmailSuccess");
        }

        public ActionResult ConfirmEmail(Guid id)
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserId == id);

            user.IsConfirmed = true;

            _unitOfWork.UserRepository.Update(user);

            return RedirectToAction("Index", "Home");
        }

        public void SendAsEmail(string recipient, string subject, string body)
        {
            const string profile = "Ateneo Epost";

            string commandText = string.Format(@"EXEC msdb.dbo.sp_send_dbmail @profile_name = '{0}',
            @recipients = '{1}', @subject = '{2}', @body = '{3}'", profile, recipient, subject, body);

            AdoUtility.ExecuteNonQuery(commandText);
        }
    }
}
