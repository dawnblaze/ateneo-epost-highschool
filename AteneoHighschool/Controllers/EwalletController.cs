﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.Mvc;
using AteneoHighschool.DataAccess;
using AteneoHighschool.DataAccess.Utility;
using AteneoHighschool.Models;

namespace AteneoHighschool.Controllers
{
    public class EwalletController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StatementOfAccount()
        {
            return View();
        }

        public ActionResult StatementOfAccountGrid()
        {
            var id = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name).UserId;

            var cardStatements = GetStatementOfAccount(new Guid("A6EFE9F9-94BE-423E-8CEE-87727050DE19"));

            var statementTable = new List<StatementOfAccount>();

            var totalPurchase = 0.0;

            for (var index = 0; index < cardStatements.Rows.Count; index++)
            {
                var x = new StatementOfAccount
                {
                    TransactionDate = cardStatements.Rows[index].ItemArray[0].ToString(),
                    CompanyName = cardStatements.Rows[index].ItemArray[1].ToString(),
                    ProductName = cardStatements.Rows[index].ItemArray[2].ToString(),
                    Price = cardStatements.Rows[index].ItemArray[3].ToString(),
                    Quantity = cardStatements.Rows[index].ItemArray[4].ToString(),
                    Amount = cardStatements.Rows[index].ItemArray[5].ToString(),
                    Balance = cardStatements.Rows[index].ItemArray[6].ToString(),
                };

                totalPurchase += (Convert.ToDouble(x.Price) * Convert.ToDouble(x.Quantity));

                statementTable.Add(x);
            }

            var monthStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            var currentDate = DateTime.Now;

            ViewBag.ReportDate =
                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("MMMM", CultureInfo.InvariantCulture) +
                " " + monthStart.Day + " to " + currentDate.Day;

            ViewBag.Name = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == id).FullName;

            ViewBag.IdNumber = "";

            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                ViewBag.IdNumber = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == id).EmployeeNumber;
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Student"))
            {
                ViewBag.IdNumber = _unitOfWork.StudentRepository.GetOne(q => q.PersonId == id).StudentNumber;
            }

            ViewBag.TotalPurchase = totalPurchase;

            return PartialView("_StatementOfAccountGrid", statementTable);
        }

        public DataTable GetStatementOfAccount(Guid personId)
        {
            string query =
                string.Format(@"[dbo].[GetCardStatement] @userId = '{0}', @dateStart = '{1}', @dateEnd = '{2}'",
                    personId, new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now);

            return AdoUtility.ExecuteEwalletQuery(query);
        }
    }
}
