﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Controllers
{
    public class HomeController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public ActionResult Index()
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

            if (user != null)
            {
                if (!user.IsConfirmed)
                {
                    ViewBag.EmailConfirmation = "Not Confirmed.";
                }
            }

            return View();
        }
    }
}
