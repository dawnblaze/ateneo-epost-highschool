﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;
using AteneoHighschool.Models;
using File = AteneoHighschool.Data.File;

namespace AteneoHighschool.Controllers
{
    public class PathwayController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

            //For not confirmed emails.
            if (user != null)
            {
                if (!user.IsConfirmed)
                {
                    ViewBag.EmailConfirmation = "Not Confirmed.";

                    return RedirectToAction("Index", "Home");
                }
            }

            var dropDown = new List<string>();
                if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
                {
                    var employee =
                        _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);
                    var groupId =
                        _unitOfWork.GroupAdviserRepository.GetOne(q => q.EmployeeId == employee.EmployeeId).GroupId;
                    var group = _unitOfWork.GroupRepository.GetOne(q => q.GroupId == groupId).GroupName;
                    dropDown.Add("Advisory: " + group);
                }
                if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
                {
                    var parent = _unitOfWork.ParentRepository.GetOne(q => q.PersonId == user.UserId);
                    var relationship = _unitOfWork.RelationshipRepository.GetList(q => q.ParentId == parent.ParentId);
                    var childList =
                        relationship.Select(
                            item =>
                                _unitOfWork.StudentRepository.GetOne(q => q.StudentId == item.StudentId,
                                    includeProperties: "Person")).Select(x => "Child: " + x.Person.FullName).ToList();
                    dropDown.AddRange(childList);
                }
                ViewBag.DropDown = dropDown;
                return View();
        }

        public ActionResult GetEvents(double start, double end, string name)
        {
            var userId = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name).UserId;
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                if (!string.IsNullOrEmpty(name))
                {
                    if (name.Contains("Advisory: "))
                    {
                        //Convert to RegEx
                        name = name.Remove(0, 10);

                        var group = _unitOfWork.GroupRepository.GetOne(q => q.GroupName == name);
                        var curriculumClass =
                            _unitOfWork.CurriculumClassRepository.GetList(q => q.GroupId == group.GroupId)
                                .Select(q => q.CurriculumClassId);
                        //var curriculumClass =
                        //_unitOfWork.CurriculumClassRepository.GetList(
                        //    q => q.GroupId == _unitOfWork.GroupRepository.GetOne(x => x.GroupName == name).GroupId)
                        //    .Select(q => q.CurriculumClassId);
                        return Json(Rows(curriculumClass), JsonRequestBehavior.AllowGet);
                    }
                    if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
                    {
                        if (name.Contains("Child: "))
                        {
                            name = name.Remove(0, 7);
                            var person =
                                _unitOfWork.PersonRepository.GetOne(
                                    q => q.LastName + ", " + q.FirstName + " " + q.MiddleName == name);
                            return Json(StudentPathway(person.PersonId), JsonRequestBehavior.AllowGet);
                        }
                    }

                }
                return Json(TeacherPathway(userId), JsonRequestBehavior.AllowGet);
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Student"))
            {
                return Json(StudentPathway(userId), JsonRequestBehavior.AllowGet);
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
            {
                if (!string.IsNullOrEmpty(name))
                {
                    name = name.Remove(0, 7);
                    var person =
                        _unitOfWork.PersonRepository.GetOne(
                            q => q.LastName + ", " + q.FirstName + " " + q.MiddleName == name);
                    return Json(StudentPathway(person.PersonId), JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new PathwayItem(), JsonRequestBehavior.AllowGet);
        }

        private List<Events> TeacherPathway(Guid personId)
        {
            #region Too much process... 

            //var classTeacher = _unitOfWork.ClassTeacherRepository.GetList(q => q.Employee.PersonId == personId);
            var curriculumClassId =
                _unitOfWork.ClassTeacherRepository.GetList(q => q.Employee.PersonId == personId)
                    .Select(q => q.CurriculumClassId)
                    .ToList();
            var pathwayItemList = new List<PathwayItem>();
            if (curriculumClassId.Any())
            {
                foreach (var item in curriculumClassId)
                {
                    var pathwayItem =
                        _unitOfWork.PathwayItemRepository.GetList(
                            q => q.CurriculumClassId == item && q.IsActive == true, includeProperties: "Curriculum, CurriculumClass")
                            .OrderBy(q => q.StartDate);

                    //foreach (var item1 in pathwayItem)
                    //{
                    //    var curriculum = _unitOfWork.CurriculumRepository.GetOne(q => q.CurriculumId == item1.CurriculumId);
                    //    item1.CurriculumName = curriculum.CurriculumName;
                    //}

                    pathwayItemList.AddRange(pathwayItem);
                }
            }

            var rows = pathwayItemList.OrderBy(q => q.StartDate).Select(item => new Events
            {
                id = item.PathwayItemId.ToString(),
                title = item.Curriculum.CurriculumName,
                description =
                    "- " + item.PathwayItemType + "<br />- " + item.CurriculumClass.CurriculumClassName + "<br />- " +
                    HatsType(item.PathwayItemId),
                start = item.StartDate.ToString("s"),
                end = item.EndDate.ToString("s"),
                className = "schedule",
                allDay = false
            }).ToList();

            rows.AddRange(GetExceptions());

            #endregion Too much process...

            return rows;
        }

        private List<Events> StudentPathway(Guid personId)
        {
            var studentId = _unitOfWork.StudentRepository.GetOne(q => q.PersonId == personId).StudentId;
            var groupId = _unitOfWork.GroupMemberRepository.GetOne(q => q.StudentId == studentId).GroupId;
            var curriculumClass =
                _unitOfWork.CurriculumClassRepository.GetList(q => q.GroupId == groupId)
                    .Select(q => q.CurriculumClassId)
                    .ToList();

            return Rows(curriculumClass);
        }

        private IEnumerable<Events> GetExceptions()
        {
            #region Temporary Code, too much process, loads the entire school year instead of per week.

            var rows = new List<Events>();
            var exception = _unitOfWork.ExceptionRepository.GetList();
            if (exception != null)
            {
                var z = exception.OrderBy(q => q.StartDate);

                rows = z.OrderBy(q => q.StartDate).Select(item => new Events
                {
                    id = item.ExceptionId.ToString(),
                    title = "NO CLASS",
                    description = item.Description,
                    start = item.StartDate.ToString("s"),
                    end = item.EndDate.ToString("s"),
                    className = "exception",
                    allDay = true
                }).ToList();
            }
            var announcement = _unitOfWork.AnnouncementRepository.GetList();
            if (announcement != null)
            {
                rows.AddRange(announcement.Select(item => new Events
                {
                    id = item.AnnouncementId.ToString(),
                    title = item.AnnouncementName.ToUpper(),
                    description = item.Description,
                    start = item.StartDate.ToString("s"),
                    end = item.EndDate.ToString("s"),
                    className = "announcement",
                    allDay = false
                }));
            }
            var schoolActivity = _unitOfWork.SchoolActivityRepository.GetList();
            if (schoolActivity != null)
            {
                rows.AddRange(schoolActivity.Select(item => new Events
                {
                    id = item.SchoolActivityId.ToString(),
                    title = item.SchoolActivityName.ToUpper(),
                    description = item.Description,
                    start = item.StartDate.ToString("s"),
                    end = item.EndDate.ToString("s"),
                    className = "schoolactivity",
                    allDay = false
                }));
            }

            var academicYear =
                _unitOfWork.AcademicYearRepository.GetOne(q => q.StartDate <= DateTime.Now && q.EndDate >= DateTime.Now);
            var x =
                _unitOfWork.AcademicYearRepository.GetOne(
                    q => q.AcademicYearId == new Guid("4A872B31-5BCC-4594-9897-6D9ED5546882"));

            var date = x.StartDate;
            var gap = (x.EndDate - x.StartDate).Days;

            for (var count = 0; count <= 356; count++)
            {
                var z = date.ToString("s");
                if (rows.Count(q => q.start == z) == 0)
                {
                    var r = new Events
                    {
                        id = "",
                        title = "",
                        description = "",
                        start = date.ToString("s"),
                        end = date.ToString("s"),
                        className = "blank",
                        allDay = false
                    };
                    rows.Add(r);
                }
                date = date.AddDays(1);
            }

            #endregion

            return rows;
        }

        private List<Events> Rows(IEnumerable<Guid> curriculumClass)
        {
            #region Too much process... loads the entire school year instead of per week.

            var pathwayItemList = new List<PathwayItem>();
            foreach (var item in curriculumClass)
            {
                var x =
                    _unitOfWork.PathwayItemRepository.GetList(
                        q => q.CurriculumClassId == item,
                        includeProperties: "Curriculum, CurriculumClass");
                pathwayItemList.AddRange(x);
            }

            var rows = pathwayItemList.OrderBy(q => q.StartDate).Select(item => new Events
            {
                id = item.PathwayItemId.ToString(),
                title = item.Curriculum.CurriculumName,
                description =
                    "- " + item.PathwayItemType + "<br />- " + item.CurriculumClass.CurriculumClassName + "<br />- " +
                    HatsType(item.PathwayItemId),
                start = item.StartDate.ToString("s"),
                end = item.EndDate.ToString("s"),
                className = "schedule",
                allDay = false
            }).ToList();

            rows.AddRange(GetExceptions());

            #endregion Too much process...

            return rows;
        }

        public ActionResult CreateHats(DateTime date, Guid curriculumClassId, Guid pathwayItemId)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                var hats = new Hats
                {
                    Date = date,
                    CurriculumClassId = curriculumClassId,
                    CurriculumClass =
                        _unitOfWork.CurriculumClassRepository.GetOne(q => q.CurriculumClassId == curriculumClassId),
                    PathwayItemId = pathwayItemId,
                    PathwayItem = _unitOfWork.PathwayItemRepository.GetOne(q => q.PathwayItemId == pathwayItemId),
                };

                ViewBag.Date = date.Date.ToString("MM/dd/yyyy");
                return PartialView(HatsVm(hats));
            }
            ViewBag.ProhibitedMessage = "Sorry! you have no right to create a new HATS.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateHats(Hats hats)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //----------------------------------------------------------------------------------------------------

                    #region Temporary Code

                    var currentDate = DateTime.Now;
                    var groupId =
                        _unitOfWork.CurriculumClassRepository.GetOne(
                            q => q.CurriculumClassId == hats.CurriculumClassId).GroupId;
                    if (hats.HatsType.ToString() == "MasteryTest" || hats.HatsType.ToString() == "LongTest")
                    {
                        if ((hats.Date - currentDate).TotalDays < 5 != true)
                        {
                            if (IsAllowedToCreateHats(groupId, currentDate))
                            {
                                var x = _unitOfWork.HatsRepository.GetOne(q => q.PathwayItemId == hats.PathwayItemId);
                                if (x == null)
                                {
                                    _unitOfWork.HatsRepository.Insert(hats);
                                    return RedirectToAction("Detail",
                                        new {id = hats.PathwayItemId, message = "Successfully updated."});
                                }
                                return RedirectToAction("Detail",
                                    new
                                    {
                                        id = hats.PathwayItemId,
                                        message = "You already have scheduled an activity for this meeting."
                                    });
                            }
                            return RedirectToAction("Detail",
                                new {id = hats.PathwayItemId, message = "Unable to update. Test limit reached."});
                        }
                        return RedirectToAction("Detail",
                            new
                            {
                                id = hats.PathwayItemId,
                                message = "Unable to update. Must set schedule 5 days from today."
                            });
                    }
                    if (hats.HatsType.ToString() == "Quiz")
                    {
                        if ((hats.Date - currentDate).TotalDays >= 3 && (hats.Date - currentDate).TotalDays <= 11)
                        {
                            if (IsAllowedToCreateHats(groupId, currentDate))
                            {
                                var x = _unitOfWork.HatsRepository.GetOne(q => q.PathwayItemId == hats.PathwayItemId);
                                if (x == null)
                                {
                                    _unitOfWork.HatsRepository.Insert(hats);
                                    return RedirectToAction("Detail",
                                        new {id = hats.PathwayItemId, message = "Successfully updated."});
                                }
                                return RedirectToAction("Detail",
                                    new
                                    {
                                        id = hats.PathwayItemId,
                                        message = "You already have scheduled an activity for this meeting."
                                    });
                            }
                            return RedirectToAction("Detail",
                                new {id = hats.PathwayItemId, message = "Unable to update. Test limit reached."});
                        }
                        return RedirectToAction("Detail",
                            new
                            {
                                id = hats.PathwayItemId,
                                message = "Unable to update. Must set schedule the week before the scheduled date."
                            });
                    }
                    _unitOfWork.HatsRepository.Insert(hats);
                    return RedirectToAction("Detail", new {id = hats.PathwayItemId, message = "Successfully updated."});

                    #endregion Temporary Code

                    //----------------------------------------------------------------------------------------------------
                }
                catch (ArgumentException ae)
                {
                    ModelState.AddModelError("", ae.Message);
                    return RedirectToAction("Detail",
                        new {id = hats.PathwayItemId, message = "Unable to update. An error occured. " + ae.Message});
                }
            }
            return PartialView(HatsVm(hats));
        }

        //----------------------------------------------------------------------------------------------------

        #region Temporary Code

        public ActionResult Detail(Guid id, string message)
        {
            ViewBag.HatsType = null;
            ViewBag.PathwayItemType = null;
            var pathwayItem = _unitOfWork.PathwayItemRepository.GetOne(q => q.PathwayItemId == id,
                includeProperties: "Curriculum");
            var hats = _unitOfWork.HatsRepository.GetOne(q => q.PathwayItemId == id);
            if (hats != null)
            {
                pathwayItem.PathwayItemType = hats.PathwayItemType;
                _unitOfWork.PathwayItemRepository.Update(pathwayItem);
                ViewBag.HatsType = hats.HatsType;
                ViewBag.HatsTypeDescription = hats.Description;
                ViewBag.PathwayItemType = hats.PathwayItemType;
            }
            if (message != null)
            {
                if (message == "Successfully updated.")
                {
                    ViewBag.CreateSuccess = message;
                }
                else
                {
                    ViewBag.CreateFail = message;
                }
            }
            return PartialView(PathwayItemVm(pathwayItem));
        }

        public ActionResult Curriculum(PathwayItem pathwayItem)
        {
            var groupId =
                _unitOfWork.CurriculumClassRepository.GetOne(q => q.CurriculumClassId == pathwayItem.CurriculumClassId)
                    .GroupId;
            var proficiencyLevelId = _unitOfWork.GroupRepository.GetOne(q => q.GroupId == groupId).ProficiencyLevelId;
            var curriculumItem =
                _unitOfWork.CurriculumItemRepository.GetList(
                    q => q.CurriculumId == pathwayItem.CurriculumId && q.ProficiencyLevelId == proficiencyLevelId);
            return PartialView(curriculumItem);
        }

        #endregion Temporary Code

        //----------------------------------------------------------------------------------------------------

        public ActionResult UploadFile(Guid pathwayItemId)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                var file = new File
                {
                    FileId = pathwayItemId,
                    UploadDate = DateTime.Now
                };
                return PartialView(FileVm(file));
            }
            ViewBag.ProhibitedMessage = "Sorry! you have no right to upload files.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadFile(HttpPostedFileWrapper file, Guid id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //----------------------------------------------------------------------------------------------------

                    #region Temporary

                    string mimeType = file.ContentType;
                    Stream fileStream = file.InputStream;
                    string fileName = Path.GetFileName(file.FileName);
                    int fileLength = file.ContentLength;
                    byte[] fileData = new byte[fileLength];
                    fileStream.Read(fileData, 0, fileLength);

                    //Temporary filter, just the content to check
                    var checkFiles = _unitOfWork.FileRepository.GetList(q => q.Data == fileData);
                    if (checkFiles.Any())
                    {
                        return RedirectToAction("Detail",
                            new
                            {
                                id,
                                message =
                                    "Unable to upload. There is/are similar file(s) uploaded. Please check the resource library"
                            });
                    }

                    var _file = new File
                    {
                        FileName = fileName,
                        MimeType = mimeType,
                        Data = fileData,
                        UploadDate = DateTime.Now,
                        Status = "PENDING"
                    };

                    _unitOfWork.FileRepository.Insert(_file);

                    var pathwayItem = _unitOfWork.PathwayItemRepository.GetOne(q => q.PathwayItemId == id,
                        includeProperties: "CurriculumClass");

                    var pathwayItemFile = new PathwayItemFile
                    {
                        PathwayItemId = id,
                        CurriculumClassId = pathwayItem.CurriculumClassId,
                        FileId = _file.FileId,
                    };

                    _unitOfWork.PathwayItemFileRepository.Insert(pathwayItemFile);

                    var userId = _unitOfWork.UserRepository.GetOne(x => x.UserName == User.Identity.Name).UserId;

                    var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == userId);

                    var teacherFile = new TeacherFile
                    {
                        EmployeeId = employee.EmployeeId,
                        FileId = _file.FileId,
                    };

                    _unitOfWork.TeacherFileRepository.Insert(teacherFile);

                    #endregion Temporary

                    //----------------------------------------------------------------------------------------------------

                    return RedirectToAction("Detail",
                        new {id, message = "Successfully uploaded."});
                }
                catch (ArgumentException ae)
                {
                    ModelState.AddModelError("", ae.Message);
                    return RedirectToAction("Detail",
                        new {id, message = "Upload failed. An error occured" + ae.Message});
                }
            }
            return PartialView(FileVm(new File()));
        }

        public ActionResult Attachments(Guid id)
        {
            var pathwayItemFile = _unitOfWork.PathwayItemFileRepository.GetList(q => q.PathwayItemId == id,
                includeProperties: "PathwayItem, CurriculumClass, File");

            return PartialView(pathwayItemFile);
        }

        public FileContentResult DownloadAttachment(Guid fileId)
        {
            var file = _unitOfWork.FileRepository.GetOne(q => q.FileId == fileId);
            return File(file.Data, file.MimeType, file.FileName);
        }

        private string HatsType(Guid pathwayItemId)
        {
            var hats = _unitOfWork.HatsRepository.GetOne(q => q.PathwayItemId == pathwayItemId);
            if (hats != null)
            {
                return hats.HatsType.ToString();
            }
            return "";
        }

        private PathwayItemVM PathwayItemVm(PathwayItem pathwayItem)
        {
            var pathwayItemVm = new PathwayItemVM
            {
                PathwayItem = pathwayItem,
                CurriculumVM = new CurriculumVM
                {
                    Curriculum = pathwayItem.Curriculum,
                    CurriculumList =
                        new SelectList(_unitOfWork.CurriculumRepository.GetList().OrderBy(c => c.CurriculumName),
                            "CurriculumId", "CurriculumName")
                },
                AcademicYearVM = new AcademicYearVM
                {
                    AcademicYear = pathwayItem.AcademicYear,
                    AcademicYearList =
                        new SelectList(_unitOfWork.AcademicYearRepository.GetList().OrderBy(ay => ay.AcademicYearName),
                            "AcademicYearId", "AcademicYearName")
                },
                CurriculumClassVM = new CurriculumClassVM
                {
                    CurriculumClass = pathwayItem.CurriculumClass,
                    CurriculumClassList =
                        new SelectList(
                            _unitOfWork.CurriculumClassRepository.GetList().OrderBy(cc => cc.CurriculumClassName),
                            "CurriculumClassId", "CurriculumClassName")
                },
            };
            return pathwayItemVm;
        }

        private HatsVM HatsVm(Hats hats)
        {
            var hatsVm = new HatsVM
            {
                Hats = hats,
                CurriculumClassVM = new CurriculumClassVM
                {
                    CurriculumClass = hats.CurriculumClass,
                    CurriculumClassList =
                        new SelectList(_unitOfWork.CurriculumClassRepository.GetList(), "CurriculumClassId",
                            "CurriculumClassName")
                },
            };
            return hatsVm;
        }

        private FileVM FileVm(File file)
        {
            var fileVm = new FileVM
            {
                File = file,
            };
            return fileVm;
        }

        //private PathwayItemFileVM PathwayItemFileVm(PathwayItemFile pathwayItemFile)
        //{
        //    var pathwayItemFileVm = new PathwayItemFileVM
        //    {
        //        PathwayItemFile = pathwayItemFile,
        //        PathwayItemVM = new PathwayItemVM
        //        {
        //            PathwayItem = pathwayItemFile.PathwayItem,
        //            PathwayItemList =
        //                new SelectList(_unitOfWork.PathwayItemRepository.GetList(), "PathwayItemId",
        //                    "CurriculumName")
        //        },
        //        CurriculumClassVM = new CurriculumClassVM
        //        {
        //            CurriculumClass = pathwayItemFile.CurriculumClass,
        //            CurriculumClassList =
        //                new SelectList(_unitOfWork.CurriculumClassRepository.GetList(), "CurriculumClassId",
        //                    "CurriculumClassName")
        //        },
        //        FileVM = new FileVM
        //        {
        //            File = pathwayItemFile.File,
        //            FileList =
        //                new SelectList(_unitOfWork.FileRepository.GetList(), "FileId",
        //                    "FileName")
        //        },
        //    };

        //    return pathwayItemFileVm;
        //}

        //private IEnumerable<string> GetErrorsFromModelState()
        //{
        //    return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        //}

        //----------------------------------------------------------------------------------------------------

        #region Temporary Code

        private bool IsAllowedToCreateHats(Guid groupId, DateTime currentDate)
        {
            var startDate = currentDate.Add(new TimeSpan(0, -6, 0, 0));
            var endDate = currentDate.Add(new TimeSpan(0, 22, 0, 0));

            var hatsList = new List<Hats>();

            var curriculumClass = _unitOfWork.CurriculumClassRepository.GetList(q => q.GroupId == groupId);

            foreach (var item in curriculumClass)
            {
                var hats =
                    _unitOfWork.HatsRepository.GetList(
                        q => q.CurriculumClassId == item.CurriculumClassId && q.Date >= startDate && q.Date <= endDate,
                        includeProperties: "CurriculumClass.Group");

                hatsList.AddRange(hats);
            }

            var masteryTestCount = hatsList.Count(q => q.HatsType == Data.Enum.HatsType.MasteryTest);
            var longTestCount = hatsList.Count(q => q.HatsType == Data.Enum.HatsType.LongTest);
            var quizCount = hatsList.Count(q => q.HatsType == Data.Enum.HatsType.Quiz);

            if (masteryTestCount + longTestCount < 3 && quizCount == 0)
                return true;
            if (masteryTestCount + longTestCount < 1 && quizCount < 4)
                return true;
            if (masteryTestCount + longTestCount < 2 && quizCount < 2)
                return true;
            if (quizCount < 6 && masteryTestCount + longTestCount == 0)
                return true;

            return false;
        }

        //private DateTime GetStartOfWeekPriorToDate(DateTime date)
        //{
        //    var referenceDate = date;
        //    var dayOfWeek = referenceDate.DayOfWeek.ToString();

        //    switch (dayOfWeek)
        //    {
        //        case "Monday":
        //            return referenceDate;
        //        case "Tuesday":
        //            return referenceDate.AddDays(-1);
        //        case "Wednesday":
        //            return referenceDate.AddDays(-2);
        //        case "Thursday":
        //            return referenceDate.AddDays(-3);
        //        case "Friday":
        //            return referenceDate.AddDays(-4);
        //        default:
        //            return referenceDate;
        //    }
        //}

        #endregion Temporary Code

        //----------------------------------------------------------------------------------------------------

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
