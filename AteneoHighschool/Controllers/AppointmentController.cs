﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;
using AteneoHighschool.Models;

namespace AteneoHighschool.Controllers
{
    public class AppointmentController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);

            if (user != null)
            {
                if (!user.IsConfirmed)
                {
                    ViewBag.EmailConfirmation = "Not Confirmed.";

                    return RedirectToAction("Index", "Home");
                }
            }

            return View();
        }

        public ActionResult AppointmentSchedules()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                return PartialView("_TeacherAppointmentsGrid");
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
            {
                var dropDown = new List<string>();
                var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
                var parent = _unitOfWork.ParentRepository.GetOne(q => q.PersonId == user.UserId);
                var relationship = _unitOfWork.RelationshipRepository.GetList(q => q.ParentId == parent.ParentId);
                var childList =
                    relationship.Select(
                        item =>
                            _unitOfWork.StudentRepository.GetOne(q => q.StudentId == item.StudentId,
                                includeProperties: "Person")).Select(x => x.Person.FullName).ToList();
                dropDown.AddRange(childList);
                ViewBag.Child = dropDown;

                return PartialView("_ParentAppointmentsGrid");
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to view Appointment Schedules.";
            return PartialView("_Prohibited");
        }

        public ActionResult GetAppointmentSchedules(double start, double end, Guid? personId)
        {
            if (personId == null)
                return Json(Rows(null), JsonRequestBehavior.AllowGet);
            return Json(Rows(personId), JsonRequestBehavior.AllowGet);
        }

        public List<Events> Rows(Guid? personId)
        {
            //Employee employee;
            //if (personId == null)
            //{
            //    var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
            //    employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);
            //}
            //else
            //{
            //    var person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == personId);
            //    employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == person.PersonId);
            //}
            var rows = new List<Events>();
            if (personId != null)
            {
                var person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == personId);
                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == person.PersonId);

                var appointment =
                    _unitOfWork.AppointmentRepository.GetList(q => q.EmployeeId == employee.EmployeeId);
                //For vacant schedules
                rows =
                    appointment.Where(q => q.ParentId == null && q.StudentId == null).OrderBy(q => q.ScheduleDate)
                        .Select(item => new Events
                        {
                            id = item.AppoitmentId.ToString(),
                            title = "Vacant",
                            description =
                                "(" + (item.ScheduleDate + item.TimeStart).ToString("hh:mm tt") + " - " +
                                (item.ScheduleDate + item.TimeEnd).ToString("hh:mm tt") + ")",
                            start = (item.ScheduleDate + item.TimeStart).ToString("s"),
                            end = (item.ScheduleDate + item.TimeEnd).ToString("s"),
                            className = "vacant",
                            allDay = false
                        }).ToList();

                if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
                {
                    //For occupied schedules
                    rows.AddRange(
                        appointment.Where(q => q.ParentId != null && q.StudentId != null).Select(item => new Events
                        {
                            id = item.AppoitmentId.ToString(),
                            title = "Appointment",
                            description =
                                "(" + (item.ScheduleDate + item.TimeStart).ToString("hh:mm tt") + " - " +
                                (item.ScheduleDate + item.TimeEnd).ToString("hh:mm tt") + ")\nWith " +
                                _unitOfWork.ParentRepository.GetOne(q => q.ParentId == item.ParentId,
                                    includeProperties: "Person")
                                    .Person.FullName,
                            start = (item.ScheduleDate + item.TimeStart).ToString("s"),
                            end = (item.ScheduleDate + item.TimeEnd).ToString("s"),
                            className = "occupied",
                            allDay = false
                        }));
                }
                if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
                {
                    //For occupied schedules
                    rows.AddRange(
                        appointment.Where(q => q.ParentId != null && q.StudentId != null).Select(item => new Events
                        {
                            id = item.AppoitmentId.ToString(),
                            title = "Occupied",
                            description = "",
                            start = (item.ScheduleDate + item.TimeStart).ToString("s"),
                            end = (item.ScheduleDate + item.TimeEnd).ToString("s"),
                            className = "occupied",
                            allDay = false
                        }));
                }
            }
            return rows;
        }

        public ActionResult GetTeachersOfChild(string childName)
        {
            IEnumerable<SelectListItem> result = null;
            try
            {
                var child = _unitOfWork.PersonRepository.GetOne(q => q.FullName == childName);
                var student = _unitOfWork.StudentRepository.GetOne(q => q.PersonId == child.PersonId);
                var groupMember = _unitOfWork.GroupMemberRepository.GetOne(q => q.StudentId == student.StudentId);
                var curriculumClass =
                    _unitOfWork.CurriculumClassRepository.GetList(q => q.GroupId == groupMember.GroupId);
                var teacherList = (from item in curriculumClass
                                   select _unitOfWork.ClassTeacherRepository.GetOne(q => q.CurriculumClassId == item.CurriculumClassId).EmployeeId
                                       into employeeId
                                       select _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == employeeId).PersonId
                                           into personId
                                           select _unitOfWork.PersonRepository.GetOne(q => q.PersonId == personId)).ToList();

                result = teacherList.Select(x => new SelectListItem()
                {
                    Text = x.FullName,
                    Value = x.PersonId.ToString(),
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Update(Guid id, string message)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Parent"))
            {
                var appointment = _unitOfWork.AppointmentRepository.GetOne(q => q.AppoitmentId == id, includeProperties: "Employee, Employee.Person");
                var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
                var parent = _unitOfWork.ParentRepository.GetOne(q => q.PersonId == user.UserId);
                var relationship = _unitOfWork.RelationshipRepository.GetOne(q => q.ParentId == parent.ParentId);
                appointment.ParentId = parent.ParentId;
                appointment.StudentId = relationship.StudentId;
                if (message != null)
                {
                    if (message == "You have successfully set an appointment.")
                    {
                        ViewBag.CreateSuccess = message;
                    }
                    else
                    {
                        ViewBag.CreateFail = message;
                    }
                }
                return PartialView(AppointmentVm(appointment));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to set an Appointment.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Appointment appointment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _unitOfWork.AppointmentRepository.Update(appointment);
                    return RedirectToAction("Update",
                                new { id = appointment.AppoitmentId, message = "You have successfully set an appointment." });
                }
                catch (ArgumentException ae)
                {
                    ModelState.AddModelError("", ae.Message);
                    return RedirectToAction("Update",
                                new { id = appointment.AppoitmentId, message = "Unable to set and appointment. An error occured. " + ae.Message });
                }
            }
            return PartialView(AppointmentVm(appointment));
        }

        public ActionResult ParentSchedule()
        {
            return PartialView("_ParentScheduleGrid");
        }

        public ActionResult GetParentSchedule(double start, double end)
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
            var parent = _unitOfWork.ParentRepository.GetOne(q => q.PersonId == user.UserId);
            var appointment = _unitOfWork.AppointmentRepository.GetList(q => q.ParentId == parent.ParentId, includeProperties: "Employee, Employee.Person");
            var rows =
                appointment.Select(item => new Events
                {
                    id = item.AppoitmentId.ToString(),
                    title = "Appointment",
                    description =
                        "(" + (item.ScheduleDate + item.TimeStart).ToString("hh:mm tt") + " - " +
                        (item.ScheduleDate + item.TimeEnd).ToString("hh:mm tt") + ")\nWith " + item.Employee.Person.FullName,
                    start = (item.ScheduleDate + item.TimeStart).ToString("s"),
                    end = (item.ScheduleDate + item.TimeEnd).ToString("s"),
                    className = "occupied",
                    allDay = false
                }).ToList();
            return Json(rows, JsonRequestBehavior.AllowGet);
        }

        private AppointmentVM AppointmentVm(Appointment appointment)
        {
            var appointmentVm = new AppointmentVM
            {
                Appointment = appointment,
                EmployeeVM = new EmployeeVM
                {
                    Employee = appointment.Employee,
                    EmployeeList =
                        new SelectList(_unitOfWork.EmployeeRepository.GetList(), "EmployeeId",
                            "EmployeeId")
                },
            };
            return appointmentVm;
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
