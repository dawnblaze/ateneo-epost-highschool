﻿namespace AteneoHighschool.Models
{
    public class StatementOfAccount
    {
        public string TransactionDate { get; set; }
        public string CompanyName { get; set; }
        public string ProductName { get; set; }
        public string Price { get; set; }
        public string Quantity { get; set; }
        public string Amount { get; set; }
        public string Balance { get; set; }
    }
}