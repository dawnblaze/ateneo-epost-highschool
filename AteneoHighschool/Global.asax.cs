﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AteneoHighschool.Data.Enum;
using AteneoHighschool.DataAccess.Helpers;

namespace AteneoHighschool
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleMobileConfig.RegisterBundles(BundleTable.Bundles);
        }

        public static void RegisterBinders()
        {
            ModelBinders.Binders.Add(typeof(HatsType),
                    new EnumFlagsModelBinder());

            ModelBinders.Binders.Add(typeof(PathwayItemType),
                    new EnumFlagsModelBinder());
        }
    }
}