﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace AteneoHighschool.BusinessLayer.Repository.Declaration
{
    public interface IPathwayItemRepository
    {
        //void DeleteTermSyllabus(DateTime startDate, DateTime endDate);
        //void DeletePathwayItemsPriorToException(DateTime exceptionDate);
        bool IsAllowedToCreateHats(Guid curriculumClassId, DateTime weekStartDate, DateTime weekEndDate);
        DateTime GetStartOfWeekPriorToDate(DateTime date);
    }
}
