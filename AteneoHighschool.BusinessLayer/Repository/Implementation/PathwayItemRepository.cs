﻿using System;
using System.Linq;
using AteneoHighschool.BusinessLayer.Repository.Declaration;
//using AteneoHighschool.BusinessLayer.Utility;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.BusinessLayer.Repository.Implementation
{
    public class PathwayItemRepository : IPathwayItemRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public PathwayItemRepository()
        {
        }

        public PathwayItemRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        //public void DeleteTermSyllabus(DateTime startDate, DateTime endDate)
        //{
        //    AdoUtility.ExecuteNonQuery(string.Format(@"EXEC DeleteTermSyllabus @StartDate = '{0}', @EndDate = '{1}'",
        //        startDate, endDate));
        //}

        //public void DeletePathwayItemsPriorToException(DateTime exceptionDate)
        //{
        //    AdoUtility.ExecuteNonQuery(@"DELETE PathwayItem WHERE CONVERT(DATE, StartDate, 112) = CONVERT(DATE, '" +
        //                               exceptionDate + "', 112)");
        //}

        public bool IsAllowedToCreateHats(Guid curriculumClassId, DateTime weekStartDate, DateTime weekEndDate)
        {
            var hats =
                _unitOfWork.HatsRepository.GetList(
                    q => q.CurriculumClassId == curriculumClassId && q.Date >= weekStartDate && q.Date <= weekEndDate);

            var masteryTestCount = hats.Count(q => q.HatsType.ToString() == "MasteryTest");
            var longTestCount = hats.Count(q => q.HatsType.ToString() == "LongTest");
            var quizCount = hats.Count(q => q.HatsType.ToString() == "Quiz");

            if (masteryTestCount + longTestCount < 3)
                return true;
            if (masteryTestCount + longTestCount < 1 || quizCount < 4)
                return true;
            if (masteryTestCount + longTestCount < 2 || quizCount < 2)
                return true;
            if (quizCount < 6)
                return true;

            return false;
        }

        public DateTime GetStartOfWeekPriorToDate(DateTime date)
        {
            var referenceDate = date;
            var dayOfWeek = referenceDate.DayOfWeek.ToString();

            switch (dayOfWeek)
            {
                case "Monday":
                    return referenceDate;
                case "Tuesday":
                    return referenceDate.AddDays(-1);
                case "Wednesday":
                    return referenceDate.AddDays(-2);
                case "Thursday":
                    return referenceDate.AddDays(-3);
                case "Friday":
                    return referenceDate.AddDays(-4);
                default:
                    return referenceDate;
            }
        }
    }
}
