﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using System.Text;
using AteneoHighschool.BusinessLayer.Service.Declaration;
using System.Data.Linq.Mapping;
using AteneoHighschool.Data;
using AteneoHighschool.BusinessLayer.Repository.Declaration;

namespace AteneoHighschool.BusinessLayer.Service.Implementation
{
    public class PathwayItemService : IPathwayItemService
    {
        private readonly IPathwayItemRepository _pathwayItemRepository;

        protected PathwayItemService() 
        {
        }

        public PathwayItemService(IPathwayItemRepository pathwayItemRepository)
        {
            _pathwayItemRepository = pathwayItemRepository;
        }

        //public void DeleteTermSyllabus(DateTime startDate, DateTime endDate)
        //{
        //    _pathwayItemRepository.DeleteTermSyllabus(startDate, endDate);
        //}

        //public void DeletePathwayItemsPriorToException(DateTime exceptionDate)
        //{
        //    _pathwayItemRepository.DeletePathwayItemsPriorToException(exceptionDate);
        //}

        public bool IsAllowedToCreateHats(Guid curriculumClassId, DateTime weekStartDate, DateTime weekEndDate)
        {
            return _pathwayItemRepository.IsAllowedToCreateHats(curriculumClassId, weekStartDate, weekEndDate);
        }

        public DateTime GetStartOfWeekPriorToDate(DateTime date)
        {
            return _pathwayItemRepository.GetStartOfWeekPriorToDate(date);
        }
    }
}
