﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AteneoHighschool.BusinessLayer.Service.Declaration
{
    public interface IPathwayItemService
    {
        //void DeleteTermSyllabus(DateTime startDate, DateTime endDate);
        //void DeletePathwayItemsPriorToException(DateTime exceptionDate);
        bool IsAllowedToCreateHats(Guid curriculumClassId, DateTime weekStartDate, DateTime weekEndDate);
        DateTime GetStartOfWeekPriorToDate(DateTime date);
    }
}
