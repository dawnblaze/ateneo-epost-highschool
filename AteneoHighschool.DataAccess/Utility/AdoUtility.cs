﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AteneoHighschool.DataAccess.Utility
{
    public class AdoUtility
    {
        public static void ExecuteNonQuery(string commandText)
        {
            #region Code Example
            //SqlConnection con = connectionString == "Ateneo"
            //    ? new SqlConnection(ConfigurationManager.ConnectionStrings["Ateneo"].ConnectionString)
            //    : new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);

            //Code below is equivalent to code above and is more efficient -----------------------------------------------

            //SqlConnection con;
            //if (connectionString == "Ateneo")
            //    con = new SqlConnection(ConfigurationManager.ConnectionStrings["Ateneo"].ConnectionString);
            //else
            //    con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);
            #endregion Code Example

            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["AteneoHighschool"].ConnectionString);
            var cmd = new SqlCommand {Connection = con, CommandText = commandText};
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public static DataTable ExecuteEwalletQuery(string commandText)
        {
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["eWallet"].ConnectionString);
            var cmd = new SqlCommand { Connection = con, CommandText = commandText };
            //con.Open();
            //cmd.ExecuteNonQuery();
            //con.Close();
            //cmd.Connection = con;
            //cmd.CommandText = commandText;

            con.Open();
            var dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            var table = new DataTable();
            table.Load(dr);
            con.Close();

            return table;
        }

        public static void ExecuteProcedure(string commandText, string[] paramNames, string[] paramValues)
        {
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["AteneoHighschool"].ConnectionString);
            var cmd = new SqlCommand {Connection = con, CommandText = commandText};
            for (var i = 0; i < paramNames.Length; i++)
            {
                cmd.Parameters.AddWithValue("@" + paramNames[i], paramValues[i]);
            }

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
