﻿using System;
using System.Linq;
using System.Web.Security;

namespace AteneoHighschool.DataAccess
{
    public class GenericRoleProvider : RoleProvider
    {
        private readonly UnitOfWork _unitOfWork;

        public GenericRoleProvider()
        {
            _unitOfWork = new UnitOfWork();
        }

        #region Implemented RoleProvider Methods

        public override bool IsUserInRole(string userName, string roleName)
        {
            return
                _unitOfWork.UserRoleRepository.GetOne(ur => ur.User.UserName == userName && ur.Role.RoleName == roleName) !=
                null;
        }

        public override string[] GetRolesForUser(string userName)
        {
            try
            {
                return
                    _unitOfWork.UserRoleRepository.Get(ur => ur.User.UserName == userName,
                        includeProperties: "Role,User").Select(name => name.Role.RoleName).ToArray();
            }
            catch
            {
                return null;
            }
        }

        public bool IsUserInUserRole(string userName, string roleNames)
        {
            string[] roleNamesArr = Array.ConvertAll((roleNames.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries)), p => p.Trim());

            return
                roleNamesArr.Select(
                    roleName =>
                        _unitOfWork.UserRoleRepository.GetOne(
                            q => q.User.UserName == userName && q.IsActive && q.Role.RoleName == roleName, "User, Role") !=
                        null).Any(hasUserRole => hasUserRole);
            //code below is equivalent to code above.
            //foreach (string roleName in roleNamesArr)
            //{
            //    bool hasUserRole = _unitOfWork.UserRoleRepository.GetOne(q => q.User.UserName == userName && q.IsActive && q.Role.RoleName == roleName, "User, Role") != null;
            //    if (hasUserRole)
            //    {
            //        return true;
            //    }
            //}
            //return false;
        }

        #endregion

        #region Not Implemented
        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName { get; set; }

        //public bool IsUserInUserRoleRight(string userName, string roleNames, string rightCode)
        //{
        //    bool hasUserRuleRight = false;

        //    string[] roleNamesArr = Array.ConvertAll((roleNames.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries)), p => p.Trim());

        //    foreach (string roleName in roleNamesArr)
        //    {
        //        //fetch the USER ROLE RIGHT Details
        //        hasUserRuleRight = _unitOfWork.UserRoleRightRepository.GetOne(filter: urr => urr.User.UserName == userName && urr.IsActive == true && urr.RoleRight.Right.RightCode == rightCode && urr.RoleRight.Role.RoleName == roleName, includeProperties: "User, RoleRight") != null ? true : false;
        //        if (hasUserRuleRight == true)
        //        {
        //            return hasUserRuleRight;
        //        }
        //    }

        //    return false;

        //}
        #endregion
    }
} 
    
