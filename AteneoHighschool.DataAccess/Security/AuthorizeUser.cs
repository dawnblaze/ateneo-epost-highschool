﻿using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Routing;

namespace AteneoHighschool.DataAccess.Security
{
    public class AuthorizeUser : AuthorizeAttribute
    {
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public string UsersConfigKey { get; set; }
        public string RolesConfigKey { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                var authorizedUsers = ConfigurationManager.AppSettings[UsersConfigKey];
                var authorizedRoles = ConfigurationManager.AppSettings[RolesConfigKey];

                Users = String.IsNullOrEmpty(Users) ? authorizedUsers : Users;
                Roles = String.IsNullOrEmpty(Roles) ? authorizedRoles : Roles;

                if (!String.IsNullOrEmpty(Roles) && !String.IsNullOrEmpty(Users))
                {
                    if (!_roleProvider.IsUserInRole(Users, Roles))
                    {
                        filterContext.Result = new RedirectToRouteResult(new
                            RouteValueDictionary(new {controller = "Account", action = "LogOut"}));
                    }
                }
            }
        }
    }
}
