﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.UI.WebControls;
using AteneoHighschool.Data;

namespace AteneoHighschool.DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext()
            : base("name=AteneoHighschool")
        { }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Term> Terms { get; set; }
        public DbSet<ProficiencyLevel> ProficiencyLevels { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Relationship> Relationships { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<Exception> Exceptions { get; set; }
        public DbSet<GroupAdviser> GroupAdvisers { get; set; }
        public DbSet<CurriculumClass> CurriculumClasses { get; set; }
        public DbSet<ClassTeacher> ClassTeachers { get; set; }
        public DbSet<Campus> Campuses { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<CurriculumItem> CurriculumItems { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Recurrence> Recurrences { get; set; }
        public DbSet<PathwayItem> PathwayItems { get; set; }
        public DbSet<Hats> Hatses { get; set; }
        public DbSet<Announcement> Announcements { get; set; }
        public DbSet<PathwayItemFile> PathwayItemFiles { get; set; }
        public DbSet<TeacherFile> TeacherFiles { get; set; }
        public DbSet<SchoolActivity> SchoolActivities { get; set; }
        public DbSet<ParentNotificationSlip> ParentNotificationSlips { get; set; }
        public DbSet<SubjectCoordinator> SubjectCoordinators { get; set; }
        public DbSet<SubjectTeacher> SubjectTeacher { get; set; }
        public DbSet<AppointmentSchedule> AppointmentSchedules { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<LevelLeader> LevelLeaders { get; set; }

        public DbSet<Gender> Genders { get; set; }
        public DbSet<CivilStatus> CivilStatuses { get; set; }
        public DbSet<Prefix> Prefixes { get; set; }
        public DbSet<AcademicYear> AcademicYears { get; set; }
        public DbSet<TermType> TermTypes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Curriculum> Curriculums { get; set; }
        public DbSet<EmployeeType> EmployeeTypes { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<File> Files { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            #region Person Dependecy
            modelBuilder.Entity<Person>()
                        .HasRequired(x => x.Gender)
                        .WithMany()
                        .HasForeignKey(x => x.GenderId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                        .HasRequired(x => x.Prefix)
                        .WithMany()
                        .HasForeignKey(x => x.PrefixId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                        .HasRequired(x => x.CivilStatus)
                        .WithMany()
                        .HasForeignKey(x => x.CivilStatusId)
                        .WillCascadeOnDelete(false);
            #endregion Person Dependecy

            #region Term Dependency
            modelBuilder.Entity<Term>()
                        .HasRequired(x => x.TermType)
                        .WithMany()
                        .HasForeignKey(x => x.TermTypeId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Term>()
                        .HasRequired(x => x.AcademicYear)
                        .WithMany()
                        .HasForeignKey(x => x.AcademicYearId)
                        .WillCascadeOnDelete(false);
            #endregion Term Dependency

            #region Proficiency Level Dependency
            modelBuilder.Entity<ProficiencyLevel>()
                        .HasRequired(x => x.Category)
                        .WithMany()
                        .HasForeignKey(x => x.CategoryId)
                        .WillCascadeOnDelete(false);
            #endregion Proficiency Level Dependency

            #region Student Dependency
            modelBuilder.Entity<Student>()
                        .HasRequired(x => x.Person)
                        .WithMany()
                        .HasForeignKey(x => x.PersonId)
                        .WillCascadeOnDelete(false);
            #endregion Student Dependency

            #region Parent Dependency
            modelBuilder.Entity<Parent>()
                        .HasRequired(x => x.Person)
                        .WithMany()
                        .HasForeignKey(x => x.PersonId)
                        .WillCascadeOnDelete(false);
            #endregion Parent Dependency

            #region Relationship Dependency
            modelBuilder.Entity<Relationship>()
                        .HasRequired(x => x.Parent)
                        .WithMany()
                        .HasForeignKey(x => x.ParentId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Relationship>()
                        .HasRequired(x => x.Student)
                        .WithMany()
                        .HasForeignKey(x => x.StudentId)
                        .WillCascadeOnDelete(false);
            #endregion Relationship Dependency

            #region Group Member Dependecy
            modelBuilder.Entity<GroupMember>()
                        .HasRequired(x => x.Group)
                        .WithMany()
                        .HasForeignKey(x => x.GroupId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<GroupMember>()
                        .HasRequired(x => x.Student)
                        .WithMany()
                        .HasForeignKey(x => x.StudentId)
                        .WillCascadeOnDelete(false);
            #endregion Group Member Dependecy

            #region Exception Dependency
            modelBuilder.Entity<Exception>()
                        .HasRequired(x => x.AcademicYear)
                        .WithMany()
                        .HasForeignKey(x => x.AcademicYearId)
                        .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Exception>()
            //            .HasRequired(x => x.ExceptionType)
            //            .WithMany()
            //            .HasForeignKey(x => x.ExceptionTypeId)
            //            .WillCascadeOnDelete(false);
            #endregion Exception Dependency

            #region Group Adviser Dependency
            modelBuilder.Entity<GroupAdviser>()
                        .HasRequired(x => x.Employee)
                        .WithMany()
                        .HasForeignKey(x => x.EmployeeId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<GroupAdviser>()
                        .HasRequired(x => x.Group)
                        .WithMany()
                        .HasForeignKey(x => x.GroupId)
                        .WillCascadeOnDelete(false);
            #endregion Group Adviser Dependency

            #region Curriculum Class Dependency
            modelBuilder.Entity<CurriculumClass>()
                        .HasRequired(x => x.Group)
                        .WithMany()
                        .HasForeignKey(x => x.GroupId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<CurriculumClass>()
                        .HasRequired(x => x.Curriculum)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<CurriculumClass>()
                        .HasRequired(x => x.AcademicYear)
                        .WithMany()
                        .HasForeignKey(x => x.AcademicYearId)
                        .WillCascadeOnDelete(false);
            #endregion Curriculum Class Dependency

            #region Class Teacher Dependency
            modelBuilder.Entity<ClassTeacher>()
                        .HasRequired(x => x.CurriculumClass)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumClassId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<ClassTeacher>()
                        .HasRequired(x => x.Employee)
                        .WithMany()
                        .HasForeignKey(x => x.EmployeeId)
                        .WillCascadeOnDelete(false);
            #endregion Class Teacher Dependency

            #region Campus Dependency
            modelBuilder.Entity<Campus>()
                        .HasRequired(c => c.School)
                        .WithMany()
                        .HasForeignKey(c => c.SchoolId)
                        .WillCascadeOnDelete(false);
            #endregion Campus Dependency

            #region Course Dependecy
            modelBuilder.Entity<Course>()
                        .HasRequired(c => c.Campus)
                        .WithMany()
                        .HasForeignKey(c => c.CampusId)
                        .WillCascadeOnDelete(false);
            #endregion Course Dependency

            #region Curriculum Item Dependecy
            modelBuilder.Entity<CurriculumItem>()
                        .HasRequired(x => x.Curriculum)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumId)
                        .WillCascadeOnDelete(false);
            #endregion Curriculum Item Dependency

            #region Group Dependecy
            modelBuilder.Entity<Group>()
                        .HasRequired(x => x.ProficiencyLevel)
                        .WithMany()
                        .HasForeignKey(x => x.ProficiencyLevelId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<Group>()
                        .HasOptional(x => x.Employee)
                        .WithMany()
                        .HasForeignKey(x => x.EmployeeId)
                        .WillCascadeOnDelete(false);
            #endregion Group Dependency

            #region Curriculum Dependecy
            modelBuilder.Entity<Curriculum>()
                        .HasRequired(x => x.AcademicYear)
                        .WithMany()
                        .HasForeignKey(x => x.AcademicYearId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<Curriculum>()
                        .HasRequired(x => x.Course)
                        .WithMany()
                        .HasForeignKey(x => x.CourseId)
                        .WillCascadeOnDelete(false);
            #endregion Curriculum Dependency

            #region User Role Dependency
            modelBuilder.Entity<UserRole>()
                        .HasRequired(x => x.Role)
                        .WithMany()
                        .HasForeignKey(x => x.RoleId)
                        .WillCascadeOnDelete(false);
            #endregion User Role Dependency

            #region Recurrence Dependency
            modelBuilder.Entity<Recurrence>()
                        .HasRequired(x => x.CurriculumClass)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumClassId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<Recurrence>()
                        .HasRequired(x => x.Schedule)
                        .WithMany()
                        .HasForeignKey(x => x.ScheduleId)
                        .WillCascadeOnDelete(false);
            #endregion Recurrence Dependency

            #region Pathway Item Dependency
            modelBuilder.Entity<PathwayItem>()
                        .HasRequired(x => x.Curriculum)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<PathwayItem>()
                        .HasRequired(x => x.AcademicYear)
                        .WithMany()
                        .HasForeignKey(x => x.AcademicYearId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<PathwayItem>()
                        .HasRequired(x => x.CurriculumClass)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumClassId)
                        .WillCascadeOnDelete(false);
            #endregion Pathway Item Dependency

            #region Hats Dependency
            modelBuilder.Entity<Hats>()
                        .HasRequired(x => x.CurriculumClass)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumClassId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<Hats>()
                        .HasRequired(x => x.PathwayItem)
                        .WithMany()
                        .HasForeignKey(x => x.PathwayItemId)
                        .WillCascadeOnDelete(false);
            #endregion Hats Dependency

            #region Announcement Dependency
            modelBuilder.Entity<Announcement>()
                        .HasRequired(x => x.AcademicYear)
                        .WithMany()
                        .HasForeignKey(x => x.AcademicYearId)
                        .WillCascadeOnDelete(false);
            #endregion Announcement Dependency

            #region PathwayItemFile Dependency
            modelBuilder.Entity<PathwayItemFile>()
                        .HasRequired(x => x.PathwayItem)
                        .WithMany()
                        .HasForeignKey(x => x.PathwayItemId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<PathwayItemFile>()
                        .HasRequired(x => x.CurriculumClass)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumClassId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<PathwayItemFile>()
                        .HasRequired(x => x.File)
                        .WithMany()
                        .HasForeignKey(x => x.FileId)
                        .WillCascadeOnDelete(false);
            #endregion PathwayItemFile Dependency

            #region TeacherFile Dependency
            modelBuilder.Entity<TeacherFile>()
                        .HasRequired(x => x.File)
                        .WithMany()
                        .HasForeignKey(x => x.FileId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<TeacherFile>()
                        .HasRequired(x => x.Employee)
                        .WithMany()
                        .HasForeignKey(x => x.EmployeeId)
                        .WillCascadeOnDelete(false);
            #endregion TeacherFile Dependency

            #region SchoolActivity Dependency
            modelBuilder.Entity<SchoolActivity>()
                        .HasRequired(x => x.AcademicYear)
                        .WithMany()
                        .HasForeignKey(x => x.AcademicYearId)
                        .WillCascadeOnDelete(false);
            #endregion SchoolActivity Dependency

            #region ParentNotificationSlip Dependency
            modelBuilder.Entity<ParentNotificationSlip>()
                        .HasRequired(x => x.Parent)
                        .WithMany()
                        .HasForeignKey(x => x.RecipientId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<ParentNotificationSlip>()
                        .HasRequired(x => x.Student)
                        .WithMany()
                        .HasForeignKey(x => x.StudentId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<ParentNotificationSlip>()
                        .HasRequired(x => x.Curriculum)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<ParentNotificationSlip>()
                        .HasRequired(x => x.CurriculumClass)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumClassId)
                        .WillCascadeOnDelete(false);
            #endregion ParentNotificationSlip Dependency

            #region SubjectCoordinator Dependency
            modelBuilder.Entity<SubjectCoordinator>()
                        .HasRequired(x => x.Employee)
                        .WithMany()
                        .HasForeignKey(x => x.EmployeeId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<SubjectCoordinator>()
                        .HasRequired(x => x.Curriculum)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumId)
                        .WillCascadeOnDelete(false);
            #endregion SubjectCoordinator Dependency

            #region SubjectTeacher Dependency
            modelBuilder.Entity<SubjectTeacher>()
                        .HasRequired(x => x.Employee)
                        .WithMany()
                        .HasForeignKey(x => x.EmployeeId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<SubjectTeacher>()
                        .HasRequired(x => x.Curriculum)
                        .WithMany()
                        .HasForeignKey(x => x.CurriculumId)
                        .WillCascadeOnDelete(false);
            #endregion SubjectTeacher Dependency

            #region AppointmentSchedule Dependency
            modelBuilder.Entity<AppointmentSchedule>()
                        .HasRequired(x => x.Employee)
                        .WithMany()
                        .HasForeignKey(x => x.EmployeeId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<AppointmentSchedule>()
                        .HasRequired(x => x.Schedule)
                        .WithMany()
                        .HasForeignKey(x => x.ScheduleId)
                        .WillCascadeOnDelete(false);
            #endregion AppointmentSchedule Dependency

            #region Appointment Dependency
            modelBuilder.Entity<Appointment>()
                        .HasRequired(x => x.Employee)
                        .WithMany()
                        .HasForeignKey(x => x.EmployeeId)
                        .WillCascadeOnDelete(false);
            #endregion Appointment Dependency

            #region LevelLeader Dependency
            modelBuilder.Entity<LevelLeader>()
                        .HasRequired(x => x.Employee)
                        .WithMany()
                        .HasForeignKey(x => x.EmployeeId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<LevelLeader>()
                        .HasRequired(x => x.ProficiencyLevel)
                        .WithMany()
                        .HasForeignKey(x => x.ProficiencyLevelId)
                        .WillCascadeOnDelete(false);
            #endregion
        }
    }
}
