﻿using System;
using AteneoHighschool.Data;
using Exception = AteneoHighschool.Data.Exception;

namespace AteneoHighschool.DataAccess
{
    public class UnitOfWork
    {
        private readonly DataContext _context = new DataContext();

        private GenericRepository<Person> _personRepository;
        private GenericRepository<Term> _termRepository;
        private GenericRepository<ProficiencyLevel> _proficiencyLevelRepository;
        private GenericRepository<Student> _studentRepository;
        private GenericRepository<Employee> _employeeRepository;
        private GenericRepository<Parent> _parentRepository;
        private GenericRepository<Relationship> _relationshipRepository;
        private GenericRepository<GroupMember> _groupMemberRepository;
        private GenericRepository<GroupAdviser> _groupAdviserRepository;
        private GenericRepository<CurriculumClass> _curriculumClassRepository;
        private GenericRepository<ClassTeacher> _classTeacherRepository;
        private GenericRepository<Campus> _campusRepository;
        private GenericRepository<Course> _courseRepository;
        private GenericRepository<CurriculumItem> _curriculumItemRepository;
        private GenericRepository<UserRole> _userRoleRepository;
        private GenericRepository<Recurrence> _recurrenceRepository;
        private GenericRepository<PathwayItem> _pathwayItemRepository;
        private GenericRepository<Hats> _hatsRepository;
        private GenericRepository<Exception> _exceptionRepository;
        private GenericRepository<Announcement> _eventRepository;
        private GenericRepository<PathwayItemFile> _pathwayItemFileRepository;
        private GenericRepository<TeacherFile> _teacherFileRepository;
        private GenericRepository<SchoolActivity> _schoolActivityRepository;
        private GenericRepository<ParentNotificationSlip> _parentNotificationSlipRepository;
        private GenericRepository<SubjectCoordinator> _subjectCoordinatorRepository;
        private GenericRepository<SubjectTeacher> _subjectTeacherRepository;
        private GenericRepository<AppointmentSchedule> _appointmentSchedule;
        private GenericRepository<Appointment> _appointmentRepository;
        private GenericRepository<LevelLeader> _levelLeaderRepository; 

        private GenericRepository<Gender> _genderRepository;
        private GenericRepository<CivilStatus> _civilstatusRepository;
        private GenericRepository<Prefix> _prefixRepository;
        private GenericRepository<AcademicYear> _academicYearRepository;
        private GenericRepository<TermType> _termTypeRepository;
        private GenericRepository<Category> _categoryRepository;
        private GenericRepository<Curriculum> _curriculumRepository;
        private GenericRepository<Group> _groupRepository;
        private GenericRepository<EmployeeType> _employeeTypeRepository;
        private GenericRepository<School> _schoolRepository;
        private GenericRepository<Schedule> _scheduleRepository;
        private GenericRepository<File> _fileRepository; 

        private GenericRepository<User> _userRepository;
        private GenericRepository<Role> _roleRepository;

        public GenericRepository<Person> PersonRepository
        {
            get
            {
                return _personRepository ?? (_personRepository = new GenericRepository<Person>(_context));
                //code above is equals to code below
                //if (_personRepository == null)
                //    _personRepository = new GenericRepository<Person>(context);
                //return _personRepository;
            }
        }

        public GenericRepository<Term> TermRepository
        {
            get { return _termRepository ?? (_termRepository = new GenericRepository<Term>(_context)); }
        }

        public GenericRepository<Gender> GenderRepository
        {
            get { return _genderRepository ?? (_genderRepository = new GenericRepository<Gender>(_context)); }
        }

        public GenericRepository<CivilStatus> CivilStatusRepository
        {
            get
            {
                return _civilstatusRepository ?? (_civilstatusRepository = new GenericRepository<CivilStatus>(_context));
            }
        }

        public GenericRepository<Prefix> PrefixRepository
        {
            get { return _prefixRepository ?? (_prefixRepository = new GenericRepository<Prefix>(_context)); }
        }

        public GenericRepository<AcademicYear> AcademicYearRepository
        {
            get
            {
                return _academicYearRepository ??
                       (_academicYearRepository = new GenericRepository<AcademicYear>(_context));
            }
        }

        public GenericRepository<TermType> TermTypeRepository
        {
            get { return _termTypeRepository ?? (_termTypeRepository = new GenericRepository<TermType>(_context)); }
        }

        public GenericRepository<ProficiencyLevel> ProficiencyLevelRepository
        {
            get
            {
                return _proficiencyLevelRepository ??
                       (_proficiencyLevelRepository = new GenericRepository<ProficiencyLevel>(_context));
            }
        }

        public GenericRepository<Category> CategoryRepository
        {
            get { return _categoryRepository ?? (_categoryRepository = new GenericRepository<Category>(_context)); }
        }

        public GenericRepository<Curriculum> CurriculumRepository
        {
            get
            {
                return _curriculumRepository ?? (_curriculumRepository = new GenericRepository<Curriculum>(_context));
            }
        }

        public GenericRepository<Group> GroupRepository
        {
            get { return _groupRepository ?? (_groupRepository = new GenericRepository<Group>(_context)); }
        }

        public GenericRepository<Student> StudentRepository
        {
            get { return _studentRepository ?? (_studentRepository = new GenericRepository<Student>(_context)); }
        }

        public GenericRepository<EmployeeType> EmployeeTypeRepository
        {
            get
            {
                return _employeeTypeRepository ??
                       (_employeeTypeRepository = new GenericRepository<EmployeeType>(_context));
            }
        }

        public GenericRepository<Employee> EmployeeRepository
        {
            get { return _employeeRepository ?? (_employeeRepository = new GenericRepository<Employee>(_context)); }
        }

        public GenericRepository<Parent> ParentRepository
        {
            get { return _parentRepository ?? (_parentRepository = new GenericRepository<Parent>(_context)); }
        }

        public GenericRepository<Relationship> RelationshipRepository
        {
            get
            {
                return _relationshipRepository ??
                       (_relationshipRepository = new GenericRepository<Relationship>(_context));
            }
        }

        public GenericRepository<GroupMember> GroupMemberRepository
        {
            get
            {
                return _groupMemberRepository ?? (_groupMemberRepository = new GenericRepository<GroupMember>(_context));
            }
        }

        public GenericRepository<Exception> ExceptionRepository
        {
            get { return _exceptionRepository ?? (_exceptionRepository = new GenericRepository<Exception>(_context)); }
        }

        public GenericRepository<GroupAdviser> GroupAdviserRepository
        {
            get
            {
                return _groupAdviserRepository ??
                       (_groupAdviserRepository = new GenericRepository<GroupAdviser>(_context));
            }
        }

        public GenericRepository<CurriculumClass> CurriculumClassRepository
        {
            get
            {
                return _curriculumClassRepository ??
                       (_curriculumClassRepository = new GenericRepository<CurriculumClass>(_context));
            }
        }

        public GenericRepository<ClassTeacher> ClassTeacherRepository
        {
            get
            {
                return _classTeacherRepository ??
                       (_classTeacherRepository = new GenericRepository<ClassTeacher>(_context));
            }
        }

        public GenericRepository<School> SchoolRepository
        {
            get { return _schoolRepository ?? (_schoolRepository = new GenericRepository<School>(_context)); }
        }

        public GenericRepository<Campus> CampusRepository
        {
            get { return _campusRepository ?? (_campusRepository = new GenericRepository<Campus>(_context)); }
        }

        public GenericRepository<Course> CourseRepository
        {
            get { return _courseRepository ?? (_courseRepository = new GenericRepository<Course>(_context)); }
        }

        public GenericRepository<CurriculumItem> CurriculumItemRepository
        {
            get
            {
                return _curriculumItemRepository ??
                       (_curriculumItemRepository = new GenericRepository<CurriculumItem>(_context));
            }
        }

        public GenericRepository<User> UserRepository
        {
            get { return _userRepository ?? (_userRepository = new GenericRepository<User>(_context)); }
        }

        public GenericRepository<Role> RoleRepository
        {
            get { return _roleRepository ?? (_roleRepository = new GenericRepository<Role>(_context)); }
        }

        public GenericRepository<UserRole> UserRoleRepository
        {
            get { return _userRoleRepository ?? (_userRoleRepository = new GenericRepository<UserRole>(_context)); }
        }

        public GenericRepository<Schedule> ScheduleRepository
        {
            get { return _scheduleRepository ?? (_scheduleRepository = new GenericRepository<Schedule>(_context)); }
        }

        public GenericRepository<Recurrence> RecurrenceRepository
        {
            get
            {
                return _recurrenceRepository ?? (_recurrenceRepository = new GenericRepository<Recurrence>(_context));
            }
        }

        public GenericRepository<PathwayItem> PathwayItemRepository
        {
            get
            {
                return _pathwayItemRepository ?? (_pathwayItemRepository = new GenericRepository<PathwayItem>(_context));
            }
        }

        public GenericRepository<Hats> HatsRepository
        {
            get { return _hatsRepository ?? (_hatsRepository = new GenericRepository<Hats>(_context)); }
        }

        public GenericRepository<File> FileRepository
        {
            get { return _fileRepository ?? (_fileRepository = new GenericRepository<File>(_context)); }
        }

        public GenericRepository<Announcement> AnnouncementRepository
        {
            get
            {
                return _eventRepository ??
                       (_eventRepository = new GenericRepository<Announcement>(_context));
            }
        }

        public GenericRepository<PathwayItemFile> PathwayItemFileRepository
        {
            get
            {
                return _pathwayItemFileRepository ??
                       (_pathwayItemFileRepository = new GenericRepository<PathwayItemFile>(_context));
            }
        }

        public GenericRepository<TeacherFile> TeacherFileRepository 
        {
            get
            {
                return _teacherFileRepository ?? (_teacherFileRepository = new GenericRepository<TeacherFile>(_context));
            }
        }

        public GenericRepository<SchoolActivity> SchoolActivityRepository
        {
            get
            {
                return _schoolActivityRepository ??
                       (_schoolActivityRepository = new GenericRepository<SchoolActivity>(_context));
            }
        }

        public GenericRepository<ParentNotificationSlip> ParentNotificationSlipRepository
        {
            get
            {
                return _parentNotificationSlipRepository ??
                       (_parentNotificationSlipRepository = new GenericRepository<ParentNotificationSlip>(_context));
            }
        }

        public GenericRepository<SubjectCoordinator> SubjectCoordinatoRepository
        {
            get
            {
                return _subjectCoordinatorRepository ??
                       (_subjectCoordinatorRepository = new GenericRepository<SubjectCoordinator>(_context));
            }
        }

        public GenericRepository<SubjectTeacher> SubjectTeacherRepository
        {
            get
            {
                return _subjectTeacherRepository ??
                       (_subjectTeacherRepository = new GenericRepository<SubjectTeacher>(_context));
            }
        }

        public GenericRepository<AppointmentSchedule> AppointmentScheduleRepository
        {
            get
            {
                return _appointmentSchedule ??
                       (_appointmentSchedule = new GenericRepository<AppointmentSchedule>(_context));
            }
        }

        public GenericRepository<Appointment> AppointmentRepository
        {
            get
            {
                return _appointmentRepository ?? (_appointmentRepository = new GenericRepository<Appointment>(_context));
            }
        }

        public GenericRepository<LevelLeader> LevelLeaderRepository
        {
            get
            {
                return _levelLeaderRepository ?? (_levelLeaderRepository = new GenericRepository<LevelLeader>(_context));
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
