﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class StudentController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "Person.FullName"; break;
                    case 2: sortProperty = "StudentNumber"; break;
                    case 3: sortProperty = "Person.Gender"; break;
                    case 4: sortProperty = "Person.Age"; break;
                    default: sortProperty = "Person.FullName"; break;
                }

                var student = _unitOfWork.StudentRepository.GetList(q => q.Person.FirstName.Contains(searchText) || q.Person.MiddleName.Contains(searchText) || q.Person.LastName.Contains(searchText) || q.StudentNumber.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0, includeProperties: "Person, Person.Gender");

                var recordCount = _unitOfWork.StudentRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = student
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var studentList = _unitOfWork.StudentRepository.GetList(q => q.Person.FirstName.Contains(searchText) || q.Person.MiddleName.Contains(searchText) || q.Person.LastName.Contains(searchText) || q.StudentNumber.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.PersonId, label = q.Person.FullName });
            return Json(studentList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var student = new Student
                {
                    Person = null,
                };
                return PartialView(StudentVm(student));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Student.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Student student)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }
                _unitOfWork.StudentRepository.Insert(student);
                Guid studentGuid = student.StudentId;
                student = _unitOfWork.StudentRepository.GetOne(q => q.StudentId == studentGuid, includeProperties: "Person, Person.Gender");

                return Json(new { success = true, message = "You have successfully created a new Student : " + student.Person.FullName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Person failed.Please check if there is already an existing Student : " + student.Person.FullName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Student student = _unitOfWork.StudentRepository.GetOne(q => q.StudentId == id,
                    "Person");
                return PartialView(StudentVm(student));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Student.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Student student)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.StudentRepository.Update(student);

                return Json(new { success = true, message = "You have successfully edited a Student." });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Student.");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Student student = _unitOfWork.StudentRepository.GetOne(r => r.StudentId == id,
                    "Person, Person.Gender");
                return PartialView(StudentVm(student));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Student.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Student student)
        {
            try
            {
                _unitOfWork.StudentRepository.Delete(student);

                return Json(new { success = true, message = "You have successfully deleted a Student."});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public StudentVM StudentVm(Student student)
        {
            var studentVm = new StudentVM
            {
                Student = student,
                PersonVM = new PersonVM
                {
                    Person = student.Person,
                    PersonList = new SelectList(_unitOfWork.PersonRepository.GetList().OrderBy(p => p.FullName), "PersonId", "FullName")
                },
            };

            return studentVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
