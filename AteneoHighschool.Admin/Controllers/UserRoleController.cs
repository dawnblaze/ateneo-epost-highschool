﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserRoleController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "User.UserName"; break;
                    default: sortProperty = "User.UserName"; break;
                }

                var userRole = _unitOfWork.UserRoleRepository.GetList(q => q.UserName.Contains(searchText),
                    iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty,
                    sortOrder: param.sSortDir_0, includeProperties: "User, Role");

                var recordCount = _unitOfWork.UserRoleRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = userRole
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var userRoleList = _unitOfWork.UserRoleRepository.GetList(q => q.UserName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.UserRoleId, label = q.UserName });
            return Json(userRoleList);
        }

        public ActionResult AddRole(Guid userId)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var user = _unitOfWork.UserRepository.GetOne(q => q.UserId == userId);
                var userRole = new UserRole
                {
                    UserId = user.UserId,
                    UserName = user.UserName,
                    Role = null,
                };
                return PartialView(UserRoleVm(userRole));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to add a Role to User.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        public ActionResult AddRole(UserRole userRole)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var duplicateUserRole =
                        _unitOfWork.UserRoleRepository.GetOne(
                            q => q.UserId == userRole.UserId && q.RoleId == userRole.RoleId);

                    if (duplicateUserRole == null)
                    {
                        var user = _unitOfWork.UserRepository.GetOne(q => q.UserId == userRole.UserId);
                        var role = _unitOfWork.RoleRepository.GetOne(q => q.RoleId == userRole.RoleId);

                        userRole.UserName = user.UserName;
                        userRole.RoleName = role.RoleName;
                        userRole.IsActive = user.IsActive;

                        _unitOfWork.UserRoleRepository.Insert(userRole);

                        return RedirectToAction("Detail", "User",
                            new {id = userRole.UserId, successMessage = "Successfully added new Role"});
                    }
                }
                catch (ArgumentException ae)
                {
                    ModelState.AddModelError("", ae.Message);
                }
            }
            ViewBag.ErrorMessage = "Error creating new User Role.";
            return PartialView(UserRoleVm(userRole));
        }

        public UserRoleVM UserRoleVm(UserRole userRole)
        {
            var userRoleVm = new UserRoleVM
            {
                UserRole = userRole,
                RoleVM = new RoleVM
                {
                    Role = userRole.Role,
                    RoleList = new SelectList(_unitOfWork.RoleRepository.GetList().OrderBy(g => g.RoleName), "RoleId", "RoleName")
                },
            };
            return userRoleVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
