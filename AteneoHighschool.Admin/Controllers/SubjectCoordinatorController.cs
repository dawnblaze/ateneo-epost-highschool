﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class SubjectCoordinatorController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "Curriculum.CurriculumName";
                        break;
                    case 2:
                        sortProperty = "Employee.Person.FullName";
                        break;
                    default:
                        sortProperty = "Curriculum.CurriculumName";
                        break;
                }

                var subjectCoordinator =
                    _unitOfWork.SubjectCoordinatoRepository.GetList(
                        q =>
                            //q.Employee.Person.FullName.Contains(searchText) ||
                            q.Curriculum.CurriculumName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "Employee, Curriculum, Employee.Person");

                var recordCount = _unitOfWork.SubjectCoordinatoRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = subjectCoordinator
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var subjectCoordinatorList =
                _unitOfWork.SubjectCoordinatoRepository.GetList(
                    q =>
                        //q.Employee.Person.FullName.Contains(searchText) ||
                        q.Curriculum.CurriculumName.Contains(searchText),
                    iDisplayLength: maxResult, includeProperties: "Employee, Employee.Person")
                    .Select(q => new {id = q.SubjectCoordinatorId, label = q.Employee.Person.FullName});
            return Json(subjectCoordinatorList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var subjectCoordinator = new SubjectCoordinator
                {
                    Employee = null,
                    Curriculum = null,
                };
                return PartialView(SubjectCoordinatorVm(subjectCoordinator));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Subject Coordinator.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(SubjectCoordinator subjectCoordinator)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }
                _unitOfWork.SubjectCoordinatoRepository.Insert(subjectCoordinator);
                Guid subjectCoordinatorGuid = subjectCoordinator.SubjectCoordinatorId;
                subjectCoordinator =
                    _unitOfWork.SubjectCoordinatoRepository.GetOne(
                        q => q.SubjectCoordinatorId == subjectCoordinatorGuid,
                        includeProperties: "Employee, Employee.Person");

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully created a new Subject Coordinator : " +
                                subjectCoordinator.SubjectCoordinatorId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Subject Coordinator failed. Please check if there is already an existing Subject Coordinator : " +
                    subjectCoordinator.Employee.Person.FullName + "." + ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                SubjectCoordinator subjectCoordinator =
                    _unitOfWork.SubjectCoordinatoRepository.GetOne(q => q.SubjectCoordinatorId == id,
                        "Employee");
                return PartialView(SubjectCoordinatorVm(subjectCoordinator));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Subject Coordinator.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(SubjectCoordinator subjectCoordinator)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.SubjectCoordinatoRepository.Update(subjectCoordinator);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully edited a Subject Coordinator : " +
                                subjectCoordinator.SubjectCoordinatorId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a Subject Coordinator named: " +
                    subjectCoordinator.SubjectCoordinatorId + ".");
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                SubjectCoordinator subjectCoordinator =
                    _unitOfWork.SubjectCoordinatoRepository.GetOne(q => q.SubjectCoordinatorId == id);
                return PartialView(SubjectCoordinatorVm(subjectCoordinator));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Subject Coordinator.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(SubjectCoordinator subjectCoordinator)
        {
            try
            {
                _unitOfWork.SubjectCoordinatoRepository.Delete(subjectCoordinator);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully deleted a Subject Coordinator : " +
                                subjectCoordinator.SubjectCoordinatorId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public SubjectCoordinatorVM SubjectCoordinatorVm(SubjectCoordinator subjectCoordinator)
        {
            var subjectCoordinatorVm = new SubjectCoordinatorVM
            {
                SubjectCoordinator = subjectCoordinator,
                EmployeeVM = new EmployeeVM
                {
                    Employee = subjectCoordinator.Employee,
                    EmployeeList =
                        new SelectList(
                            _unitOfWork.EmployeeRepository.GetList(includeProperties: "Employee, Employee.Person")
                                .OrderBy(q => q.Person.FullName),
                            "EmployeeId", "Person.FullName")
                },
                CurriculumVM = new CurriculumVM
                {
                    Curriculum = subjectCoordinator.Curriculum,
                    CurriculumList =
                        new SelectList(_unitOfWork.CurriculumRepository.GetList().OrderBy(q => q.CurriculumName),
                            "CurriculumId", "CurriculumName")
                }
            };
            return subjectCoordinatorVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
