﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Admin.Models;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator, Subject Teacher, Subject Coordinator, Academic Principal, Level Leader")]
    public class EmployeeController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "Person.FullName"; break;
                    case 2: sortProperty = "EmployeeNumber"; break;
                    case 3: sortProperty = "EmployeeType.EmployeeTypeName"; break;
                    case 4: sortProperty = "Person.Gender"; break;
                    case 5: sortProperty = "Person.Age"; break;
                    default: sortProperty = "Person.FullName"; break;
                }

                var employee =
                    _unitOfWork.EmployeeRepository.GetList(
                        q =>
                            q.Person.FirstName.Contains(searchText) || q.Person.MiddleName.Contains(searchText) ||
                            q.Person.LastName.Contains(searchText) || q.EmployeeNumber.Contains(searchText) ||
                            q.EmployeeNumber.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "Person, Person.Gender, EmployeeType");

                var recordCount = _unitOfWork.EmployeeRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = employee
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var employeeList =
                _unitOfWork.EmployeeRepository.GetList(
                    q =>
                        q.Person.FirstName.Contains(searchText) || q.Person.MiddleName.Contains(searchText) ||
                        q.Person.LastName.Contains(searchText) || q.EmployeeNumber.Contains(searchText) ||
                        q.EmployeeNumber.Contains(searchText), iDisplayLength: maxResult)
                    .Select(q => new {id = q.EmployeeId, label = q.Person.FullName});
            return Json(employeeList);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var employee = new Employee
                {
                    Person = null,
                    EmployeeType = null,
                };
                return PartialView(EmployeeVm(employee));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Employee.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Employee employee)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }
                _unitOfWork.EmployeeRepository.Insert(employee);
                Guid employeeGuid = employee.EmployeeId;
                employee = _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == employeeGuid, "Person, EmployeeType");

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully created a new Employee : " + employee.Person.FullName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Employee failed.Please check if there is already an existing Employee : " +
                    employee.Person.FullName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == id,
                    "Person, EmployeeType");
                return PartialView(EmployeeVm(employee));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit an Employee.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Employee employee)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.EmployeeRepository.Update(employee);

                return Json(new { success = true, message = "You have successfully edited an Employee" });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Employee.");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Employee employee = _unitOfWork.EmployeeRepository.GetOne(r => r.EmployeeId == id);
                return PartialView(EmployeeVm(employee));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete an Employee.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Employee employee)
        {
            try
            {
                _unitOfWork.EmployeeRepository.Delete(employee);

                return Json(new {success = true, message = "You have successfully deleted an Employee"});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [Authorize(Roles = "Subject Teacher, Subject Coordinator, Academic Principal, Level Leader")]
        public ActionResult TrackSyllabus()
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
            var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);

            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher"))
            {
                return View("SubjectTeacherPage", AssignedClasses(employee));
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Coordinator"))
            {
                return View("SubjectCoordinatorPage", SubjectTeachers(employee));
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Level Leader"))
            {
                return View("LevelLeaderPage", LevelTeachers(employee));
            }
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Academic Principal"))
            {
                return View("AcademicPrincipalPage", SubjectCoordinators());
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right.";
            return PartialView("_Prohibited");
        }

        public List<ClassViewModel> AssignedClasses(Employee employee)
        {
            #region Too much process...
            var classTeacher = _unitOfWork.ClassTeacherRepository.GetList(q => q.EmployeeId == employee.EmployeeId);

            var curriculumClassList =
                classTeacher.Select(
                    item =>
                        _unitOfWork.CurriculumClassRepository.GetOne(
                            q => q.CurriculumClassId == item.CurriculumClassId)).ToList();

            var classViewModelList = (from item in curriculumClassList
                                      let studentCount = _unitOfWork.GroupMemberRepository.GetList(q => q.GroupId == item.GroupId).Count()
                                      let className = item.CurriculumClassName
                                      let topics =
                                          _unitOfWork.PathwayItemRepository.GetList(q => q.CurriculumClassId == item.CurriculumClassId)
                                      let topicsCount = topics.Count()
                                      let topicsDone = topics.Count(q => q.EndDate > DateTime.Now)
                                      select new ClassViewModel
                                      {
                                          ClassName = className,
                                          Students = studentCount,
                                          Meetings = topicsCount,
                                          MeetingsDone = topicsDone,
                                          BufferMeetings = 0,
                                          //Compression = 0,
                                          MeanScore = 0,
                                      }).ToList();
            //code below is equivalent to code above
            //foreach (var item in curriculumClassList)
            //{
            //    var studentCount = _unitOfWork.GroupMemberRepository.GetList(q => q.GroupId == item.GroupId).Count();
            //    var className = item.CurriculumClassName;
            //    var topics =
            //        _unitOfWork.PathwayItemRepository.GetList(q => q.CurriculumClassId == item.CurriculumClassId);
            //    var topicsCount = topics.Count();
            //    var topicsDone = topics.Count(q => q.EndDate > DateTime.Now);
            //    var classViewModel = new ClassViewModel()
            //    {
            //        ClassName = className,
            //        Students = studentCount,
            //        Meetings = topicsCount,
            //        MeetingsDone = topicsDone,
            //        TopicsRemain = topicsCount - topicsDone,
            //        BufferMeetings = 0,
            //        Compression = 0,
            //        MeanScore = 0,
            //    };
            //    classViewModelList.Add(classViewModel);
            //}
            #endregion Too much process...

            return classViewModelList;
        }

        public List<TeacherViewModel> SubjectTeachers(Employee employee)
        {
            #region Too much process...
            var subjectCoordinator =
                _unitOfWork.SubjectCoordinatoRepository.GetOne(q => q.EmployeeId == employee.EmployeeId);
            var subjectTeachers =
                _unitOfWork.SubjectTeacherRepository.GetList(q => q.CurriculumId == subjectCoordinator.CurriculumId);
            var classTeacherList = new List<ClassTeacher>();

            foreach (
                var x in
                    subjectTeachers.Select(
                        item => _unitOfWork.ClassTeacherRepository.GetList(q => q.EmployeeId == item.EmployeeId)))
            {
                classTeacherList.AddRange(x);
            }

            var curriculumClassList =
                classTeacherList.Select(
                    item =>
                        _unitOfWork.CurriculumClassRepository.GetOne(
                            q => q.CurriculumClassId == item.CurriculumClassId)).ToList();

            var teacherViewModelList = (from item in curriculumClassList
                                        let studentCount = _unitOfWork.GroupMemberRepository.GetList(q => q.GroupId == item.GroupId).Count()
                                        let className = item.CurriculumClassName
                                        let topics = _unitOfWork.PathwayItemRepository.GetList(q => q.CurriculumClassId == item.CurriculumClassId)
                                        let topicsCount = topics.Count()
                                        let topicsDone = topics.Count(q => q.EndDate > DateTime.Now)
                                        let teacher = _unitOfWork.ClassTeacherRepository.GetOne(q => q.CurriculumClassId == item.CurriculumClassId)
                                        let employeeTeacher = _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == teacher.EmployeeId)
                                        let person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == employeeTeacher.PersonId)
                                        let @group = _unitOfWork.GroupRepository.GetOne(q => q.GroupId == item.GroupId, includeProperties: "ProficiencyLevel").ProficiencyLevel.ProficiencyLevelName
                                        select new TeacherViewModel
                                        {
                                            TeacherName = person.FullName,
                                            ClassName = className,
                                            Level = @group,
                                            Students = studentCount,
                                            Meetings = topicsCount,
                                            MeetingsDone = topicsDone,
                                            BufferMeetings = 0,
                                            //Compression = 0,
                                            MeanScore = 0,
                                        }).ToList();
            #endregion Too much process...

            return teacherViewModelList;
        }

        public List<LevelTeacherViewModel> LevelTeachers(Employee employee)
        {
            #region Too much process...
            var levelLeader =
                _unitOfWork.LevelLeaderRepository.GetOne(q => q.EmployeeId == employee.EmployeeId);

            var classTeacher =
                _unitOfWork.ClassTeacherRepository.GetList(
                    q => q.CurriculumClass.Group.ProficiencyLevelId == levelLeader.ProficiencyLevelId,
                    includeProperties: "CurriculumClass, CurriculumClass.Group, CurriculumClass.Group.ProficiencyLevel");

            return (from item in classTeacher
                let studentCount =
                    _unitOfWork.GroupMemberRepository.GetList(q => q.GroupId == item.CurriculumClass.GroupId).Count()
                let className = item.CurriculumClass.CurriculumClassName
                let topics =
                    _unitOfWork.PathwayItemRepository.GetList(q => q.CurriculumClassId == item.CurriculumClassId)
                let topicsCount = topics.Count()
                let topicsDone = topics.Count(q => q.EndDate > DateTime.Now)
                let teacher =
                    _unitOfWork.ClassTeacherRepository.GetOne(q => q.CurriculumClassId == item.CurriculumClassId)
                let employeeTeacher = _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == teacher.EmployeeId)
                let person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == employeeTeacher.PersonId)
                let level = item.CurriculumClass.Group.ProficiencyLevel.ProficiencyLevelName
                select new LevelTeacherViewModel
                {
                    LeaderName = person.FullName,
                    ClassName = className,
                    Level = level,
                    Students = studentCount,
                    Meetings = topicsCount,
                    MeetingsDone = topicsDone,
                    BufferMeetings = 0,
                    MeanScore = 0,
                }).ToList();

            #endregion Too much process...
        }

        public ActionResult LevelLeaders()
        {
            #region Too much process...
            var levelLeader = _unitOfWork.LevelLeaderRepository.GetList();
            var levelLeaderViewModelList = (from item in levelLeader
                let teacherEmployee = _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == item.EmployeeId)
                let person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == teacherEmployee.PersonId)
                let levelTeachers = LevelTeachers(teacherEmployee)
                let level = _unitOfWork.ProficiencyLevelRepository.GetOne(q => q.ProficiencyLevelId == item.ProficiencyLevelId)
                let averageMeetings = levelTeachers.Select(q => q.Meetings).Sum()/levelTeachers.Count()
                let averageMeetingsDone = levelTeachers.Select(q => q.MeetingsDone).Sum()/levelTeachers.Count()
                let averageMeetingsRemain = levelTeachers.Select(q => q.MeetingsRemain).Sum()/levelTeachers.Count()
                let averageTopicsRemain = levelTeachers.Select(q => q.TopicsRemain).Sum()/levelTeachers.Count()
                let averageBufferMeetings = levelTeachers.Select(q => q.BufferMeetings).Sum()/levelTeachers.Count()
                let averageMeanScore = levelTeachers.Select(q => q.MeanScore).Sum()/levelTeachers.Count()
                select new LevelLeaderViewModel
                {
                    LevelLeaderName = person.FullName, Level = level.ProficiencyLevelName, AverageMeetings = averageMeetings, AverageMeetingsDone = averageMeetingsDone, AverageMeetingsRemain = averageMeetingsRemain, AverageTopicsRemain = averageTopicsRemain, AverageBufferMeetings = averageBufferMeetings, AverageMeanScore = averageMeanScore,
                }).ToList();
            #endregion Too much process...

            return PartialView("_NonAcademicPrincipalPage", levelLeaderViewModelList);
        }

        public List<SubjectCoordinatorViewModel> SubjectCoordinators()
        {
            #region Too much process...
            var subjectCoordinator = _unitOfWork.SubjectCoordinatoRepository.GetList();
            var subjectCoordinatorViewModelList = (from item in subjectCoordinator
                let teacherEmployee = _unitOfWork.EmployeeRepository.GetOne(q => q.EmployeeId == item.EmployeeId)
                let person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == teacherEmployee.PersonId)
                let subjectTeachers = SubjectTeachers(teacherEmployee)
                let curriculum = _unitOfWork.CurriculumRepository.GetOne(q => q.CurriculumId == item.CurriculumId)
                let averageMeetings = subjectTeachers.Select(q => q.Meetings).Sum()/subjectTeachers.Count()
                let averageMeetingsDone = subjectTeachers.Select(q => q.MeetingsDone).Sum()/subjectTeachers.Count()
                let averageMeetingsRemain = subjectTeachers.Select(q => q.MeetingsRemain).Sum()/subjectTeachers.Count()
                let averageTopicsRemain = subjectTeachers.Select(q => q.TopicsRemain).Sum()/subjectTeachers.Count()
                let averageBufferMeetings = subjectTeachers.Select(q => q.BufferMeetings).Sum()/subjectTeachers.Count()
                let averageMeanScore = subjectTeachers.Select(q => q.MeanScore).Sum()/subjectTeachers.Count()
                select new SubjectCoordinatorViewModel
                {
                    CoordinatorName = person.FullName,
                    Subject = curriculum.CurriculumName,
                    AverageMeetings = averageMeetings,
                    AverageMeetingsDone = averageMeetingsDone,
                    AverageMeetingsRemain = averageMeetingsRemain,
                    AverageTopicsRemain = averageTopicsRemain,
                    AverageBufferMeetings = averageBufferMeetings,
                    AverageMeanScore = averageMeanScore,
                }).ToList();
            #endregion Too much process...

            return subjectCoordinatorViewModelList;
        }

        public EmployeeVM EmployeeVm(Employee employee)
        {
            var employeeVm = new EmployeeVM
            {
                Employee = employee,
                PersonVM = new PersonVM
                {
                    Person = employee.Person,
                    PersonList =
                        new SelectList(_unitOfWork.PersonRepository.GetList().OrderBy(p => p.FullName), "PersonId",
                            "FullName")
                },
                EmployeeTypeVM = new EmployeeTypeVM
                {
                    EmployeeType = employee.EmployeeType,
                    EmployeeTypeList =
                        new SelectList(_unitOfWork.EmployeeTypeRepository.GetList().OrderBy(et => et.EmployeeTypeName),
                            "EmployeeTypeId", "EmployeeTypeName")
                }
            };

            return employeeVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
