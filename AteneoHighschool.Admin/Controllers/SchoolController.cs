﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class SchoolController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "SchoolName"; break;
                    default: sortProperty = "SchoolName"; break;
                }

                var school = _unitOfWork.SchoolRepository.GetList(q => q.SchoolName.Contains(searchText) || q.SchoolCode.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.SchoolRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = school
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var schoolList = _unitOfWork.SchoolRepository.GetList(q => q.SchoolName.Contains(searchText) || q.SchoolCode.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.SchoolId, label = q.SchoolName });
            return Json(schoolList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var school = new School
                {
                    SchoolName = null
                };
                return PartialView(SchoolVm(school));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new School.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(School school)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.SchoolRepository.Insert(school);
                Guid schoolGuid = school.SchoolId;
                school = _unitOfWork.SchoolRepository.GetOne(q => q.SchoolId == schoolGuid);

                return Json(new { success = true, message = "You have successfully created a new School : " + school.SchoolName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new School failed.Please check if there is already an existing School : " + school.SchoolName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                School school = _unitOfWork.SchoolRepository.GetOne(q => q.SchoolId == id);
                return PartialView(SchoolVm(school));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a School.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(School school)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.SchoolRepository.Update(school);

                return Json(new { success = true, message = "You have successfully edited a School : " + school.SchoolName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a School named: " + school.SchoolName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                School school = _unitOfWork.SchoolRepository.GetOne(r => r.SchoolId == id);
                return PartialView(SchoolVm(school));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a School.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(School school)
        {
            try
            {
                _unitOfWork.SchoolRepository.Delete(school);

                return Json(new { success = true, message = "You have successfully deleted a School : " + school.SchoolName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public SchoolVM SchoolVm(School school)
        {
            var schoolVm = new SchoolVM
            {
                School = school
            };
            return schoolVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
