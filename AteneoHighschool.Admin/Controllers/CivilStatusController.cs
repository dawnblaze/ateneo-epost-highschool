﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CivilStatusController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "CivilStatusName"; break;
                    default: sortProperty = "CivilStatusName"; break;
                }

                var civilStatus = _unitOfWork.CivilStatusRepository.GetList(q => q.CivilStatusName.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.AcademicYearRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = civilStatus
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var civilStatusList = _unitOfWork.CivilStatusRepository.GetList(q => q.CivilStatusName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.CivilStatusId, label = q.CivilStatusName });
            return Json(civilStatusList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var civilStatus = new CivilStatus
                {
                    CivilStatusName = null
                };
                return PartialView(CivilStatusVm(civilStatus));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Civil Status.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(CivilStatus civilStatus)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CivilStatusRepository.Insert(civilStatus);
                Guid civilStatusGuid = civilStatus.CivilStatusId;
                civilStatus = _unitOfWork.CivilStatusRepository.GetOne(q => q.CivilStatusId == civilStatusGuid);

                return Json(new { success = true, message = "You have successfully created a new Civil Status : " + civilStatus.CivilStatusName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Civil Status failed.Please check if there is already an existing Civil Status : " + civilStatus + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                CivilStatus civilStatus = _unitOfWork.CivilStatusRepository.GetOne(q => q.CivilStatusId == id);
                return PartialView(CivilStatusVm(civilStatus));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Civil Status.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(CivilStatus civilStatus)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CivilStatusRepository.Update(civilStatus);

                return Json(new { success = true, message = "You have successfully edited a Civil Status : " + civilStatus.CivilStatusName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Civil Status named: " + civilStatus.CivilStatusName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                CivilStatus civilStatus = _unitOfWork.CivilStatusRepository.GetOne(r => r.CivilStatusId == id);
                return PartialView(CivilStatusVm(civilStatus));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Civil Status.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(CivilStatus civilStatus)
        {
            try
            {
                _unitOfWork.CivilStatusRepository.Delete(civilStatus);

                return Json(new { success = true, message = "You have successfully deleted a Civil Status : " + civilStatus.CivilStatusName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public CivilStatusVM CivilStatusVm(CivilStatus civilStatus)
        {
            var civilStatusVm = new CivilStatusVM
            {
                CivilStatus = civilStatus
            };
            return civilStatusVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}