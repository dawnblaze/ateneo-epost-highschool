﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    public class SchoolActivityController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "SchoolActivityName";
                        break;
                    case 2:
                        sortProperty = "Description";
                        break;
                    case 3:
                        sortProperty = "AcademicYear.AcademicYearName";
                        break;
                    case 4:
                        sortProperty = "StartDate";
                        break;
                    case 5:
                        sortProperty = "EndDate";
                        break;
                    default:
                        sortProperty = "SchoolActivityName";
                        break;
                }

                var schoolActivity =
                    _unitOfWork.SchoolActivityRepository.GetList(
                        q =>
                            q.SchoolActivityName.Contains(searchText) || q.Description.Contains(searchText) ||
                            q.AcademicYear.AcademicYearName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "AcademicYear");

                var recordCount = _unitOfWork.SchoolActivityRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = schoolActivity
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var schoolActivityList =
                _unitOfWork.SchoolActivityRepository.GetList(
                    q =>
                        q.SchoolActivityName.Contains(searchText) || q.Description.Contains(searchText) ||
                        q.AcademicYear.AcademicYearName.Contains(searchText), iDisplayLength: maxResult)
                    .Select(q => new { id = q.SchoolActivityId, label = q.SchoolActivityName });
            return Json(schoolActivityList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var schoolActivity = new SchoolActivity
                {
                    AcademicYear = null,
                };
                return PartialView(SchoolActivityVm(schoolActivity));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new School Activity.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(SchoolActivity schoolActivity)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.SchoolActivityRepository.Insert(schoolActivity);

                Guid schoolActivityGuid = schoolActivity.SchoolActivityId;
                schoolActivity = _unitOfWork.SchoolActivityRepository.GetOne(q => q.SchoolActivityId == schoolActivityGuid,
                    includeProperties: "AcademicYear");

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully created a new School Activity: " + schoolActivity.SchoolActivityName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new School Activity failed. Please check if there is already an existing School Activity: " +
                    schoolActivity.SchoolActivityName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                SchoolActivity schoolActivity = _unitOfWork.SchoolActivityRepository.GetOne(q => q.SchoolActivityId == id,
                    "AcademicYear");
                return PartialView(SchoolActivityVm(schoolActivity));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a School Activity.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(SchoolActivity schoolActivity)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.SchoolActivityRepository.Update(schoolActivity);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully edited a School Activity: " + schoolActivity.SchoolActivityName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a School Activity named: " +
                    schoolActivity.SchoolActivityName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                SchoolActivity schoolActivity = _unitOfWork.SchoolActivityRepository.GetOne(r => r.SchoolActivityId == id);
                return PartialView(SchoolActivityVm(schoolActivity));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a School Activity.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(SchoolActivity schoolActivity)
        {
            try
            {
                _unitOfWork.SchoolActivityRepository.Delete(schoolActivity);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully deleted a School Activity: " + schoolActivity.SchoolActivityName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public SchoolActivityVM SchoolActivityVm(SchoolActivity schoolActivity)
        {
            var schoolActivityVm = new SchoolActivityVM
            {
                SchoolActivity = schoolActivity,
                AcademicYearVM = new AcademicYearVM
                {
                    AcademicYear = schoolActivity.AcademicYear,
                    AcademicYearList =
                        new SelectList(_unitOfWork.AcademicYearRepository.GetList().OrderBy(ay => ay.AcademicYearName),
                            "AcademicYearId", "AcademicYearName")
                },
            };

            return schoolActivityVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
