﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class GenderController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "GenderName"; break;
                    default: sortProperty = "GenderName"; break;
                }

                var gender = _unitOfWork.GenderRepository.GetList(q => q.GenderName.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.GenderRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = gender
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var genderList = _unitOfWork.GenderRepository.GetList(q => q.GenderName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.GenderId, label = q.GenderName });
            return Json(genderList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var gender = new Gender
                {
                    GenderName = null
                };
                return PartialView(GenderVm(gender));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Gender.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Gender gender)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.GenderRepository.Insert(gender);
                Guid genderGuid = gender.GenderId;
                gender = _unitOfWork.GenderRepository.GetOne(q => q.GenderId == genderGuid);

                return Json(new { success = true, message = "You have successfully created a new gender : " + gender.GenderName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Gender failed.Please check if there is already an existing gender : " + gender.GenderName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Gender gender = _unitOfWork.GenderRepository.GetOne(q => q.GenderId == id);
                return PartialView(GenderVm(gender));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Gender.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Gender gender)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.GenderRepository.Update(gender);

                return Json(new { success = true, message = "You have successfully edited a Gender : " + gender.GenderName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Gender named: " + gender.GenderName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Gender gender = _unitOfWork.GenderRepository.GetOne(r => r.GenderId == id);
                return PartialView(GenderVm(gender));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Gender.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Gender gender)
        {
            try
            {
                _unitOfWork.GenderRepository.Delete(gender);

                return Json(new { success = true, message = "You have successfully deleted a Gender : " + gender.GenderName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public GenderVM GenderVm(Gender gender)
        {
            var genderVm = new GenderVM
            {
                Gender = gender,

            };
            return genderVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}