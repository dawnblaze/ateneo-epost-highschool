﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class EmployeeTypeController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "EmployeeTypeName"; break;
                    default: sortProperty = "EmployeeTypeName"; break;
                }

                var employeeType = _unitOfWork.EmployeeTypeRepository.GetList(q => q.EmployeeTypeName.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.EmployeeTypeRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = employeeType
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var employeeTypeList = _unitOfWork.EmployeeTypeRepository.GetList(q => q.EmployeeTypeName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.EmployeeTypeId, label = q.EmployeeTypeName });
            return Json(employeeTypeList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var employeeType = new EmployeeType
                {
                    EmployeeTypeName = null
                };
                return PartialView(EmployeeTypeVm(employeeType));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Employee Type.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(EmployeeType employeeType)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.EmployeeTypeRepository.Insert(employeeType);
                Guid employeeTypeGuid = employeeType.EmployeeTypeId;
                employeeType = _unitOfWork.EmployeeTypeRepository.GetOne(q => q.EmployeeTypeId == employeeTypeGuid);

                return Json(new { success = true, message = "You have successfully created a new Employee Type : " + employeeType.EmployeeTypeName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Employee Type failed.Please check if there is already an existing Employee Type : " + employeeType.EmployeeTypeName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                EmployeeType employeeType = _unitOfWork.EmployeeTypeRepository.GetOne(q => q.EmployeeTypeId == id);
                return PartialView(EmployeeTypeVm(employeeType));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit an Employee Type.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(EmployeeType employeeType)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.EmployeeTypeRepository.Update(employeeType);

                return Json(new { success = true, message = "You have successfully edited a Employee Type : " + employeeType.EmployeeTypeName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Employee Type named: " + employeeType.EmployeeTypeName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                EmployeeType employeeType = _unitOfWork.EmployeeTypeRepository.GetOne(r => r.EmployeeTypeId == id);
                return PartialView(EmployeeTypeVm(employeeType));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete an Employee Type.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(EmployeeType employeeType)
        {
            try
            {
                _unitOfWork.EmployeeTypeRepository.Delete(employeeType);

                return Json(new { success = true, message = "You have successfully deleted a Employee Type : " + employeeType.EmployeeTypeName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public EmployeeTypeVM EmployeeTypeVm(EmployeeType employeeType)
        {
            var employeeTypeVm = new EmployeeTypeVM
            {
                EmployeeType = employeeType
            };
            return employeeTypeVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
