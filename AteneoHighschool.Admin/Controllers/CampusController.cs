﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CampusController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "CampusName"; break;
                    case 2: sortProperty = "School.SchoolName"; break;
                    case 3: sortProperty = "CampusCode"; break;
                    default: sortProperty = "School.SchoolName"; break;
                }

                var campus =
                    _unitOfWork.CampusRepository.GetList(
                        q =>
                            q.CampusName.Contains(searchText) || q.CampusCode.Contains(searchText) ||
                            q.School.SchoolName.Contains(searchText) || q.Location.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0, includeProperties: "School");

                var recordCount = _unitOfWork.CampusRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = campus
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var campusList =
                _unitOfWork.CampusRepository.GetList(
                    q =>
                        q.CampusName.Contains(searchText) || q.CampusCode.Contains(searchText) ||
                        q.School.SchoolName.Contains(searchText) || q.Location.Contains(searchText),
                    iDisplayLength: maxResult)
                    .Select(q => new {id = q.CampusId, label = q.CampusName});
            return Json(campusList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var campus = new Campus
                {
                    School = null,
                };
                return PartialView(CampusVm(campus));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Campus.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Campus campus)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }
                
                _unitOfWork.CampusRepository.Insert(campus);
                Guid campusGuid = campus.CampusId;
                campus = _unitOfWork.CampusRepository.GetOne(q => q.CampusId == campusGuid, "School");

                return Json(new { success = true, message = "You have successfully created a new Campus : " + campus.CampusName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Campus failed. Please check if there is already an existing Campus : " + campus.CampusName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Campus campus = _unitOfWork.CampusRepository.GetOne(q => q.CampusId == id, "School");
                return PartialView(CampusVm(campus));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Campus.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Campus campus)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CampusRepository.Update(campus);

                return Json(new { success = true, message = "You have successfully edited a Campus : " + campus.CampusName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Campus named: " + campus.CampusName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Campus campus = _unitOfWork.CampusRepository.GetOne(r => r.CampusId == id);
                return PartialView(CampusVm(campus));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Campus.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Campus campus)
        {
            try
            {
                _unitOfWork.CampusRepository.Delete(campus);

                return Json(new { success = true, message = "You have successfully deleted a Campus : " + campus.CampusName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public CampusVM CampusVm(Campus campus)
        {
            var campusVm = new CampusVM
            {
                Campus = campus,
                SchoolVM = new SchoolVM
                {
                    School = campus.School,
                    SchoolList = new SelectList(_unitOfWork.SchoolRepository.GetList().OrderBy(s => s.SchoolName), "SchoolId", "SchoolName")
                },
            };
            return campusVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
