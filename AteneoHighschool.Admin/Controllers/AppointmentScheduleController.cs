﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;
using AteneoHighschool.DataAccess.Utility;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Subject Teacher, Subject Coordinator, Level Leader")]
    public class AppointmentScheduleController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "Employee.Person.FullName";
                        break;
                    default:
                        sortProperty = "Employee.Person.FullName";
                        break;
                }

                var appointmentSchedule =
                    _unitOfWork.AppointmentScheduleRepository.GetList(
                        q =>
                            q.Employee.PersonId == user.UserId,
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "Employee, Employee.Person, Schedule");

                var recordCount = _unitOfWork.AppointmentScheduleRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = appointmentSchedule
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
            var appointmentScheduleList = _unitOfWork.AppointmentScheduleRepository.GetList(q =>
                q.Employee.PersonId == user.UserId, iDisplayLength: maxResult,
                includeProperties: "Employee, Employee.Person")
                .Select(q => new {id = q.AppointmentScheduleId, label = q.Employee.Person.FullName});
            return Json(appointmentScheduleList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher, Subject Coordinator, Level Leader"))
            {
                var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);
                var appointmentSchedule = new AppointmentSchedule
                {
                    EmployeeId = employee.EmployeeId,
                };
                return PartialView(AppointmentScheduleVm(appointmentSchedule));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Appointment Schedule.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(AppointmentSchedule appointmentSchedule)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }
                _unitOfWork.AppointmentScheduleRepository.Insert(appointmentSchedule);
                Guid appointmentScheduleGuid = appointmentSchedule.AppointmentScheduleId;
                appointmentSchedule =
                    _unitOfWork.AppointmentScheduleRepository.GetOne(
                        q => q.AppointmentScheduleId == appointmentScheduleGuid);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully created a new Appointment Schedule : " +
                                appointmentSchedule.AppointmentScheduleId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Appointment Schedule failed. Please check if there is already an existing Appointment Schedule : " +
                    appointmentSchedule.AppointmentScheduleId + "." + ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher, Subject Coordinator, Level Leader"))
            {
                var appointmentSchedule =
                    _unitOfWork.AppointmentScheduleRepository.GetOne(q => q.AppointmentScheduleId == id);
                return PartialView(AppointmentScheduleVm(appointmentSchedule));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit an Appointment Schedule.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(AppointmentSchedule appointmentSchedule)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.AppointmentScheduleRepository.Update(appointmentSchedule);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully edited an Appointment Schedule : " +
                                appointmentSchedule.AppointmentScheduleId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already an Appointment Schedule named: " +
                    appointmentSchedule.AppointmentScheduleId + ".");
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher, Subject Coordinator, Level Leader"))
            {
                var appointmentSchedule = _unitOfWork.AppointmentScheduleRepository.GetOne(r => r.AppointmentScheduleId == id);
                return PartialView(AppointmentScheduleVm(appointmentSchedule));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete an Appointment Schedule.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(AppointmentSchedule appointmentSchedule)
        {
            try
            {
                _unitOfWork.AppointmentScheduleRepository.Delete(appointmentSchedule);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully deleted an Appointment Schedule : " +
                                appointmentSchedule.AppointmentScheduleId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult GenerateAppointmentSchedule()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Subject Teacher, Subject Coordinator, Level Leader"))
            {
                AdoUtility.ExecuteNonQuery(string.Format(@"DELETE Appointment"));

                var user = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name);
                var currentAcademicYear = _unitOfWork.AcademicYearRepository.GetOne(q => q.StartDate <= DateTime.Now && q.EndDate >= DateTime.Now);
                var employee = _unitOfWork.EmployeeRepository.GetOne(q => q.PersonId == user.UserId);
                var appointmentSchedule = _unitOfWork.AppointmentScheduleRepository.GetList(q => q.EmployeeId == employee.EmployeeId);
                var dateCounter = currentAcademicYear.StartDate;
                var loopCounter =
                    Convert.ToInt32((currentAcademicYear.EndDate - currentAcademicYear.StartDate).TotalDays);

                for (var counter = 0; counter <= loopCounter; counter++) //number of school days in a school year
                {
                    if (dateCounter.DayOfWeek != DayOfWeek.Saturday && dateCounter.DayOfWeek != DayOfWeek.Sunday)
                        //school days excluding Saturday and Sunday
                    {
                        foreach (var item in appointmentSchedule)
                        {
                            var schedule = _unitOfWork.ScheduleRepository.GetOne(q => q.ScheduleId == item.ScheduleId);
                            if (dateCounter.DayOfWeek.ToString() == schedule.ScheduleName)
                            {
                                var appointment = new Appointment
                                {
                                        EmployeeId = item.EmployeeId,
                                        ParentId = null,
                                        StudentId = null,
                                        ScheduleDate = dateCounter,
                                        TimeStart = item.TimeStart,
                                        TimeEnd = item.TimeEnd
                                };
                                _unitOfWork.AppointmentRepository.Insert(appointment);
                            }
                        }
                    }
                    dateCounter = dateCounter.AddDays(1);
                }
                return RedirectToAction("Index", "AppointmentSchedule");
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to generate Appointment Schedules.";
            return PartialView("_Prohibited");
        }

        public AppointmentScheduleVM AppointmentScheduleVm(AppointmentSchedule appointmentSchedule)
        {
            var appointmentScheduleVm = new AppointmentScheduleVM
            {
                AppointmentSchedule = appointmentSchedule,
                EmployeeVM = new EmployeeVM
                {
                    Employee = appointmentSchedule.Employee,
                    EmployeeList =
                        new SelectList(
                            _unitOfWork.EmployeeRepository.GetList(includeProperties: "Person")
                                .OrderBy(et => et.Person.FullName), "EmployeeId",
                            "Person.FullName")
                },
                ScheduleVM = new ScheduleVM
                {
                    Schedule = appointmentSchedule.Schedule,
                    ScheduleList = 
                        new SelectList(
                            _unitOfWork.ScheduleRepository.GetList()
                                .OrderBy(x => x.Sequence), "ScheduleId",
                            "ScheduleName")
                }
            };
            return appointmentScheduleVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
