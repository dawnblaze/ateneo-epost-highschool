﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RelationshipController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "Parent.Person.FullName";
                        break;
                    case 2:
                        sortProperty = "Student.Person.FullName";
                        break;
                    default:
                        sortProperty = "Parent.Person.FullName";
                        break;
                }

                var relationship =
                    _unitOfWork.RelationshipRepository.GetList(
                        q =>
                            q.Parent.Person.FirstName.Contains(searchText) ||
                            q.Parent.Person.MiddleName.Contains(searchText) ||
                            q.Parent.Person.LastName.Contains(searchText)
                            || q.Student.Person.FirstName.Contains(searchText) ||
                            q.Student.Person.MiddleName.Contains(searchText) ||
                            q.Student.Person.LastName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "Parent, Parent.Person, Student, Student.Person");

                var recordCount = _unitOfWork.RelationshipRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = relationship
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var relationshipList = _unitOfWork.RelationshipRepository.GetList(
                q =>
                    q.Parent.Person.FirstName.Contains(searchText) || q.Parent.Person.MiddleName.Contains(searchText) ||
                    q.Parent.Person.LastName.Contains(searchText)
                    || q.Student.Person.FirstName.Contains(searchText) ||
                    q.Student.Person.MiddleName.Contains(searchText) || q.Student.Person.LastName.Contains(searchText),
                iDisplayLength: maxResult).Select(q => new {id = q.RelationshipId, label = q.Parent.Person.FullName});
            return Json(relationshipList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var relationship = new Relationship
                {
                    Parent = null,
                    Student = null,
                };
                return PartialView(RelationshipVm(relationship));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Relationship.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Relationship relationship)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.RelationshipRepository.Insert(relationship);
                Guid relationshipGuid = relationship.RelationshipId;
                relationship = _unitOfWork.RelationshipRepository.GetOne(q => q.RelationshipId == relationshipGuid,
                    includeProperties: "Parent, Parent.Person, Student, Student.Person");

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully created a new Relationship : Parent " +
                                relationship.Parent.Person.FullName + " to Child " +
                                relationship.Student.Person.FullName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Relationship failed.Please check if there is already an existing Relationship : Parent " +
                    relationship.Parent.Person.FullName + " to Child " + relationship.Student.Person.FullName + "." +
                    ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Relationship relationship = _unitOfWork.RelationshipRepository.GetOne(q => q.RelationshipId == id,
                    "Parent, Student");
                return PartialView(RelationshipVm(relationship));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Relationship.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Relationship relationship)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.RelationshipRepository.Update(relationship);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully edited a Relationship."
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Relationship.");
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Relationship relationship = _unitOfWork.RelationshipRepository.GetOne(r => r.RelationshipId == id);
                return PartialView(RelationshipVm(relationship));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Relationship.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Relationship relationship)
        {
            try
            {
                _unitOfWork.RelationshipRepository.Delete(relationship);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully deleted a Relationship."
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public RelationshipVM RelationshipVm(Relationship relationship)
        {
            var relationshipVm = new RelationshipVM
            {
                Relationship = relationship,
                ParentVM = new ParentVM
                {
                    Parent = relationship.Parent,
                    ParentList =
                        new SelectList(
                            _unitOfWork.ParentRepository.GetList(includeProperties: "Person")
                                .OrderBy(p => p.Person.FullName), "ParentId",
                            "Person.FullName")
                },
                StudentVM = new StudentVM
                {
                    Student = relationship.Student,
                    StudentList =
                        new SelectList(
                            _unitOfWork.StudentRepository.GetList(includeProperties: "Person")
                                .OrderBy(s => s.Person.FullName), "StudentId", "Person.FullName")
                },
            };

            return relationshipVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
