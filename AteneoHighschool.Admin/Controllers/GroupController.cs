﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class GroupController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "GroupName";
                        break;
                    case 2:
                        sortProperty = "ProficiencyLevel.Sequence";
                        break;
                    default:
                        sortProperty = "ProficiencyLevel.Sequence";
                        break;
                }

                var group = _unitOfWork.GroupRepository.GetList(q => q.GroupName.Contains(searchText),
                    iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty,
                    sortOrder: param.sSortDir_0, includeProperties: "ProficiencyLevel, Employee, Employee.Person");

                var recordCount = _unitOfWork.GroupRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = group
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var groupList =
                _unitOfWork.GroupRepository.GetList(q => q.GroupName.Contains(searchText), iDisplayLength: maxResult)
                    .Select(q => new {id = q.GroupId, label = q.GroupName});
            return Json(groupList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var group = new Group
                {
                    GroupName = null
                };
                return PartialView(GroupVm(group));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Group.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Group group)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.GroupRepository.Insert(group);
                Guid groupGuid = group.GroupId;
                group = _unitOfWork.GroupRepository.GetOne(q => q.GroupId == groupGuid);

                return
                    Json(
                        new {success = true, message = "You have successfully created a new Group : " + group.GroupName});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Group failed.Please check if there is already an existing Group : " +
                    group.GroupName + "." + ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Group group = _unitOfWork.GroupRepository.GetOne(q => q.GroupId == id);
                return PartialView(GroupVm(group));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Group.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Group group)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.GroupRepository.Update(group);

                return Json(new {success = true, message = "You have successfully edited a Group : " + group.GroupName});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a Group named: " + group.GroupName + ".");
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Group group = _unitOfWork.GroupRepository.GetOne(r => r.GroupId == id);
                return PartialView(GroupVm(group));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Group.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Group group)
        {
            try
            {
                _unitOfWork.GroupRepository.Delete(group);

                return Json(new {success = true, message = "You have successfully deleted a Group : " + group.GroupName});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public GroupVM GroupVm(Group group)
        {
            var groupVm = new GroupVM
            {
                Group = group,
                EmployeeVM = new EmployeeVM
                {
                    Employee = group.Employee,
                    EmployeeList =
                        new SelectList(_unitOfWork.EmployeeRepository.GetList().OrderBy(q => q.Person.FullName),
                            "EmployeeId", "Person.FullName")
                },
                ProficiencyLevelVM = new ProficiencyLevelVM
                {
                    ProficiencyLevel = group.ProficiencyLevel,
                    ProficiencyLevelList =
                        new SelectList(_unitOfWork.ProficiencyLevelRepository.GetList().OrderBy(pl => pl.Sequence),
                            "ProficiencyLevelId", "ProficiencyLevelName")
                }

            };
            return groupVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
