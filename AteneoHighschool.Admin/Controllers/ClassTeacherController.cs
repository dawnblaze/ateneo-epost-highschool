﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;
using Microsoft.Ajax.Utilities;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ClassTeacherController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "CurriculumClass.CurriculumClassName"; break;
                    case 2: sortProperty = "Employee.FullName"; break;
                    default: sortProperty = "CurriculumClass.CurriculumClassName"; break;
                }

                var classTeacher =
                    _unitOfWork.ClassTeacherRepository.GetList(
                        q =>
                            q.Employee.Person.FirstName.Contains(searchText) ||
                            q.Employee.Person.MiddleName.Contains(searchText) ||
                            q.Employee.Person.LastName.Contains(searchText) ||
                            q.CurriculumClass.CurriculumClassName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "CurriculumClass, Employee, Employee.Person");

                var recordCount = _unitOfWork.EmployeeRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = classTeacher
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var classTeacherList = _unitOfWork.ClassTeacherRepository.GetList(q =>
                q.Employee.Person.FirstName.Contains(searchText) ||
                q.Employee.Person.MiddleName.Contains(searchText) ||
                q.Employee.Person.LastName.Contains(searchText) ||
                q.CurriculumClass.CurriculumClassName.Contains(searchText), iDisplayLength: maxResult, includeProperties: "Employee, Employee.Person")
                .Select(q => new {id = q.ClassTeacherId, label = q.Employee.Person.FullName});
            return Json(classTeacherList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var classTeacher = new ClassTeacher
                {
                    CurriculumClass = null,
                    Employee = null,
                };
                return PartialView(ClassTeacherVm(classTeacher));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Class Teacher.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(ClassTeacher classTeacher)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }
                var existingTeacher = _unitOfWork.ClassTeacherRepository.GetOne(q => q.CurriculumClassId == classTeacher.CurriculumClassId);

                if (existingTeacher == null)
                {
                    _unitOfWork.ClassTeacherRepository.Insert(classTeacher);

                    var classTeacherGuid = classTeacher.ClassTeacherId;

                    classTeacher = _unitOfWork.ClassTeacherRepository.GetOne(q => q.ClassTeacherId == classTeacherGuid,
                            includeProperties: "CurriculumClass, Employee, Employee.Person");

                    return
                        Json(
                            new
                            {
                                success = true,
                                message =
                                    "You have successfully created a new Class Teacher : " +
                                    classTeacher.Employee.Person.FullName
                            });
                }

                ModelState.AddModelError("",
                    "Creation of new Class Teacher failed. Please check if there is already an existing Class Teacher to Class.");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Class Teacher failed."  + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var classTeacher = _unitOfWork.ClassTeacherRepository.GetOne(q => q.ClassTeacherId == id,
                    "CurriculumClass, Employee, Employee.Person");
                return PartialView(ClassTeacherVm(classTeacher));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Class Teacher.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(ClassTeacher classTeacher)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.ClassTeacherRepository.Update(classTeacher);

                return Json(new { success = true, message = "You have successfully edited a Class Teacher." });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Class Teacher.");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var classTeacher = _unitOfWork.ClassTeacherRepository.GetOne(r => r.ClassTeacherId == id);
                return PartialView(ClassTeacherVm(classTeacher));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Class Teacher.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(ClassTeacher classTeacher)
        {
            try
            {
                _unitOfWork.ClassTeacherRepository.Delete(classTeacher);

                return Json(new { success = true, message = "You have successfully deleted a Class Teacher" });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ClassTeacherVM ClassTeacherVm(ClassTeacher classTeacher)
        {
            var classTeacherVm = new ClassTeacherVM
            {
                ClassTeacher = classTeacher,
                CurriculumClassVM = new CurriculumClassVM
                {
                    CurriculumClass = classTeacher.CurriculumClass,
                    CurriculumClassList =
                        new SelectList(
                            _unitOfWork.CurriculumClassRepository.GetList().OrderBy(cc => cc.CurriculumClassName),
                            "CurriculumClassId", "CurriculumClassName")
                },
                EmployeeVM = new EmployeeVM
                {
                    Employee = classTeacher.Employee,
                    EmployeeList =
                        new SelectList(_unitOfWork.EmployeeRepository.GetList(includeProperties: "Person").OrderBy(et => et.Person.FullName), "EmployeeId",
                            "Person.FullName")
                }
            };

            return classTeacherVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
