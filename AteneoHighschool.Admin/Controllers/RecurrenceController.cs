﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RecurrenceController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index(Guid id)
        {
            CurriculumClass curriculumClass = _unitOfWork.CurriculumClassRepository.GetOne(q => q.CurriculumClassId == id);
            return PartialView(curriculumClass);
        }

        public ActionResult GetListJson(DataTableParamVM param, Guid curriculumClassId)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "Schedule.Sequence"; break;
                    default: sortProperty = "Schedule.Sequence"; break;
                }

                var recurrence =
                    _unitOfWork.RecurrenceRepository.GetList(
                        q => q.CurriculumClassId == curriculumClassId || curriculumClassId == null,
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty,
                        sortOrder: param.sSortDir_0, includeProperties: "CurriculumClass, Schedule");

                var recordCount = _unitOfWork.RecurrenceRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = recurrence
                },
                  JsonRequestBehavior.AllowGet);
                 
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Create(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                CurriculumClass curriculumClass =
                    _unitOfWork.CurriculumClassRepository.GetOne(q => q.CurriculumClassId == id);
                AcademicYear academicYear =
                    _unitOfWork.AcademicYearRepository.GetOne(q => q.AcademicYearId == curriculumClass.AcademicYearId);
                var recurrence = new Recurrence
                {
                    CurriculumClassId = curriculumClass.CurriculumClassId,
                    CurriculumClass = curriculumClass,
                    StartDate = academicYear.StartDate,
                    EndDate = academicYear.EndDate,
                    Schedule = null
                };
                return PartialView(RecurrenceVm(recurrence));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Recurrence.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Recurrence recurrence)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.RecurrenceRepository.Insert(recurrence);
                Guid recurrenceGuid = recurrence.RecurrenceId;
                recurrence = _unitOfWork.RecurrenceRepository.GetOne(q => q.RecurrenceId == recurrenceGuid,
                    "CurriculumClass, Schedule");

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully created a new Recurrence : " + recurrence.RecurrenceId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Recurrence failed. Please check if there is already an existing Recurrence : " +
                    recurrence.RecurrenceId + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Recurrence recurrence = _unitOfWork.RecurrenceRepository.GetOne(q => q.RecurrenceId == id,
                    "CurriculumClass, Schedule");
                return PartialView(RecurrenceVm(recurrence));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Recurrence.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Recurrence recurrence)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.RecurrenceRepository.Update(recurrence);

                return Json(new { success = true, message = "You have successfully edited a Recurrence : " + recurrence.RecurrenceId });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Recurrence: " + recurrence.RecurrenceId + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Recurrence recurrence = _unitOfWork.RecurrenceRepository.GetOne(r => r.RecurrenceId == id);
                return PartialView(RecurrenceVm(recurrence));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Recurrence.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Recurrence recurrence)
        {
            try
            {
                _unitOfWork.RecurrenceRepository.Delete(recurrence);

                return Json(new { success = true, message = "You have successfully deleted a Recurrence : " + recurrence.RecurrenceId });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public RecurrenceVM RecurrenceVm(Recurrence recurrence)
        {
            var recurrenceVm = new RecurrenceVM
            {
                Recurrence = recurrence,
                CurriculumClassVM = new CurriculumClassVM
                {
                    CurriculumClass = recurrence.CurriculumClass,
                    CurriculumClassList = new SelectList(_unitOfWork.CurriculumClassRepository.GetList().OrderBy(cc => cc.CurriculumClassName), "CurriculumClassId", "CurriculumClassName")
                },
                ScheduleVM = new ScheduleVM
                {
                    Schedule = recurrence.Schedule,
                    ScheduleList = new SelectList(_unitOfWork.ScheduleRepository.GetList().OrderBy(s => s.Sequence), "ScheduleId", "ScheduleName")
                }
            };

            return recurrenceVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
