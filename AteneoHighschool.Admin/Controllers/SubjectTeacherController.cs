﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class SubjectTeacherController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "Curriculum.CurriculumName";
                        break;
                    case 2:
                        sortProperty = "Employee.Person.FullName";
                        break;
                    default:
                        sortProperty = "Curriculum.CurriculumName";
                        break;
                }

                var subjectTeacher =
                    _unitOfWork.SubjectTeacherRepository.GetList(
                        q =>
                            //q.Employee.Person.FullName.Contains(searchText) ||
                            q.Curriculum.CurriculumName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "Employee, Curriculum, Employee.Person");

                var recordCount = _unitOfWork.SubjectTeacherRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = subjectTeacher
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var subjectTeacherList =
                _unitOfWork.SubjectTeacherRepository.GetList(
                    q =>
                        //q.Employee.Person.FullName.Contains(searchText) ||
                        q.Curriculum.CurriculumName.Contains(searchText),
                    iDisplayLength: maxResult, includeProperties: "Employee, Employee.Person")
                    .Select(q => new {id = q.SubjectTeacherId, label = q.Employee.Person.FullName});
            return Json(subjectTeacherList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var subjectTeacher = new SubjectTeacher
                {
                    Employee = null,
                    Curriculum = null,
                };
                return PartialView(SubjectTeacherVm(subjectTeacher));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Subject Teacher.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(SubjectTeacher subjectTeacher)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }
                _unitOfWork.SubjectTeacherRepository.Insert(subjectTeacher);
                Guid subjectTeacherGuid = subjectTeacher.SubjectTeacherId;
                subjectTeacher =
                    _unitOfWork.SubjectTeacherRepository.GetOne(
                        q => q.SubjectTeacherId == subjectTeacherGuid, includeProperties: "Employee");

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully created a new Subject Teacher : " +
                                subjectTeacher.SubjectTeacherId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Subject Teacher failed. Please check if there is already an existing Subject Teacher : " +
                    subjectTeacher.SubjectTeacherId + "." + ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                SubjectTeacher subjectTeacher =
                    _unitOfWork.SubjectTeacherRepository.GetOne(q => q.SubjectTeacherId == id,
                        "Employee");
                return PartialView(SubjectTeacherVm(subjectTeacher));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Subject Teacher.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(SubjectTeacher subjectTeacher)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.SubjectTeacherRepository.Update(subjectTeacher);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully edited a Subject Teacher : " +
                                subjectTeacher.SubjectTeacherId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a Subject Teacher named: " +
                    subjectTeacher.SubjectTeacherId + ".");
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public SubjectTeacherVM SubjectTeacherVm(SubjectTeacher subjectTeacher)
        {
            var subjectTeacherVm = new SubjectTeacherVM
            {
                SubjectTeacher = subjectTeacher,
                EmployeeVM = new EmployeeVM
                {
                    Employee = subjectTeacher.Employee,
                    EmployeeList =
                        new SelectList(
                            _unitOfWork.EmployeeRepository.GetList(
                                q => q.EmployeeType.EmployeeTypeName == "Subject Teacher",
                                includeProperties: "Employee.Person").OrderBy(q => q.Person.FullName),
                            "EmployeeId",
                            "Person.FullName")
                },
                CurriculumVM = new CurriculumVM
                {
                    Curriculum = subjectTeacher.Curriculum,
                    CurriculumList =
                        new SelectList(_unitOfWork.CurriculumRepository.GetList().OrderBy(q => q.CurriculumName),
                            "CurriculumId", "CurriculumName")
                }
            };
            return subjectTeacherVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
