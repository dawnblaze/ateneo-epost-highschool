﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CurriculumItemController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "CurriculumItemName"; break;
                    case 2: sortProperty = "Description"; break;
                    case 3: sortProperty = "Units"; break;
                    case 4: sortProperty = "Sequence"; break;
                    case 5: sortProperty = "ProficiencyLevel.ProficiencyLevelName"; break;
                    default: sortProperty = "ProficiencyLevel.ProficiencyLevelName"; break;
                }

                var curriculumItem =
                    _unitOfWork.CurriculumItemRepository.GetList(
                        q =>
                            q.CurriculumItemName.Contains(searchText) || q.Description.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0, includeProperties: "Curriculum, ProficiencyLevel");

                var recordCount = _unitOfWork.CurriculumItemRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = curriculumItem
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var curriculumItemList =
                _unitOfWork.CurriculumItemRepository.GetList(
                    q =>
                        q.CurriculumItemName.Contains(searchText) || q.Description.Contains(searchText),
                    iDisplayLength: maxResult)
                    .Select(q => new { id = q.CurriculumItemId, label = q.CurriculumItemName });
            return Json(curriculumItemList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var curriculumItem = new CurriculumItem
                {
                    Curriculum = null,
                };
                return PartialView(CurriculumItemVm(curriculumItem));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Curriculum Item.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(CurriculumItem curriculumItem)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CurriculumItemRepository.Insert(curriculumItem);
                Guid curriculumItemGuid = curriculumItem.CurriculumItemId;
                curriculumItem =
                    _unitOfWork.CurriculumItemRepository.GetOne(q => q.CurriculumItemId == curriculumItemGuid,
                        includeProperties: "Curriculum");

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully created a new Curriculum Item : " +
                                curriculumItem.CurriculumItemName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Curriculum Item failed. Please check if there is already an existing Curriculum Item : " +
                    curriculumItem.CurriculumItemName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                CurriculumItem curriculumItem =
                    _unitOfWork.CurriculumItemRepository.GetOne(q => q.CurriculumItemId == id,
                        "Curriculum");
                return PartialView(CurriculumItemVm(curriculumItem));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Curriculum Item.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(CurriculumItem curriculumItem)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CurriculumItemRepository.Update(curriculumItem);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully edited a Curriculum Item : " + curriculumItem.CurriculumItemName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a Curriculum Item named: " +
                    curriculumItem.CurriculumItemName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                CurriculumItem curriculumItem =
                    _unitOfWork.CurriculumItemRepository.GetOne(q => q.CurriculumItemId == id);
                return PartialView(CurriculumItemVm(curriculumItem));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Curriculum Item.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(CurriculumItem curriculumItem)
        {
            try
            {
                _unitOfWork.CurriculumItemRepository.Delete(curriculumItem);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully deleted a Curriculum Item : " + curriculumItem.CurriculumItemName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public CurriculumItemVM CurriculumItemVm(CurriculumItem curriculumItem)
        {
            var curriculumItemVm = new CurriculumItemVM
            {
                CurriculumItem = curriculumItem,
                CurriculumVM = new CurriculumVM
                {
                    Curriculum = curriculumItem.Curriculum,
                    CurriculumList =
                        new SelectList(_unitOfWork.CurriculumRepository.GetList().OrderBy(c => c.CurriculumName),
                            "CurriculumId", "CurriculumName")
                },
                ProficeLevelVM = new ProficiencyLevelVM
                {
                    ProficiencyLevel = curriculumItem.ProficiencyLevel,
                    ProficiencyLevelList = 
                        new SelectList(_unitOfWork.ProficiencyLevelRepository.GetList().OrderBy(pl => pl.ProficiencyLevelName),
                            "ProficiencyLevelId", "ProficiencyLevelName")
                },
            };
            return curriculumItemVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
