﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;
using AteneoHighschool.DataAccess.Utility;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ExceptionController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "ExceptionName";
                        break;
                    case 2:
                        sortProperty = "Description";
                        break;
                    case 3:
                        sortProperty = "AcademicYear.AcademicYearName";
                        break;
                    case 4:
                        sortProperty = "StartDate";
                        break;
                    case 5:
                        sortProperty = "EndDate";
                        break;
                    default:
                        sortProperty = "ExceptionName";
                        break;
                }

                var exception =
                    _unitOfWork.ExceptionRepository.GetList(
                        q =>
                            q.ExceptionName.Contains(searchText) || q.Description.Contains(searchText) ||
                            q.AcademicYear.AcademicYearName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "AcademicYear");

                var recordCount = _unitOfWork.ExceptionRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = exception
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var exceptionList =
                _unitOfWork.ExceptionRepository.GetList(
                    q =>
                        q.ExceptionName.Contains(searchText) || q.Description.Contains(searchText) ||
                        q.AcademicYear.AcademicYearName.Contains(searchText), iDisplayLength: maxResult)
                    .Select(q => new {id = q.ExceptionId, label = q.ExceptionName});
            return Json(exceptionList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var exception = new Data.Exception
                {
                    AcademicYear = null,
                };
                return PartialView(ExceptionVm(exception));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Exception.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Data.Exception exception)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                //var exceptionType = _unitOfWork.ExceptionTypeRepository.GetOne(q => q.ExceptionTypeId == exception.ExceptionTypeId);

                //if (exceptionType.ExceptionTypeName == "Exception")
                //{
                //    AdoUtility.ExecuteNonQuery(@"DELETE PathwayItem WHERE StartDate >= '" +
                //                               exception.StartDate.Add(new TimeSpan(0, 1, 0, 0)) +
                //                               "' AND StartDate <= '" +
                //                               exception.StartDate.Add(new TimeSpan(0, 23, 0, 0)) + "'");
                //}

                //AdoUtility.ExecuteNonQuery(@"DELETE PathwayItem WHERE StartDate >= '" +
                //                               exception.StartDate.Add(new TimeSpan(0, 1, 0, 0)) +
                //                               "' AND StartDate <= '" +
                //                               exception.StartDate.Add(new TimeSpan(0, 23, 0, 0)) + "'");

                AdoUtility.ExecuteNonQuery(@"UPDATE PathwayItem SET IsActive = '0' WHERE StartDate >= '" +
                                               exception.StartDate.Add(new TimeSpan(0, 1, 0, 0)) +
                                               "' AND StartDate <= '" +
                                               exception.StartDate.Add(new TimeSpan(0, 23, 0, 0)) + "'");

                _unitOfWork.ExceptionRepository.Insert(exception);
                
                Guid exceptionGuid = exception.ExceptionId;
                exception = _unitOfWork.ExceptionRepository.GetOne(q => q.ExceptionId == exceptionGuid,
                    includeProperties: "AcademicYear");

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully created a new Exception: " + exception.ExceptionName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Exception failed. Please check if there is already an existing Exception: " +
                    exception.ExceptionName + "." + ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Data.Exception exception = _unitOfWork.ExceptionRepository.GetOne(q => q.ExceptionId == id,
                    "AcademicYear");
                return PartialView(ExceptionVm(exception));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit an Exception.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Data.Exception exception)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.ExceptionRepository.Update(exception);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully edited a Exception: " + exception.ExceptionName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already an Exception named: " + exception.ExceptionName + ".");
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Data.Exception exception = _unitOfWork.ExceptionRepository.GetOne(r => r.ExceptionId == id);
                return PartialView(ExceptionVm(exception));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete an Exception.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Data.Exception exception)
        {
            try
            {
                AdoUtility.ExecuteNonQuery(@"UPDATE PathwayItem SET IsActive = '1' WHERE StartDate >= '" +
                                               exception.StartDate.Add(new TimeSpan(0, 1, 0, 0))/*.ToString("dd/MM/yy hh:mm:ss")*/ +
                                               "' AND StartDate <= '" +
                                               exception.StartDate.Add(new TimeSpan(0, 23, 0, 0))/*.ToString("dd/MM/yy hh:mm:ss")*/ + "'");

                _unitOfWork.ExceptionRepository.Delete(exception);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully deleted a Exception: " + exception.ExceptionName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ExceptionVM ExceptionVm(Data.Exception exception)
        {
            var exceptionVm = new ExceptionVM
            {
                Exception = exception,
                //ExceptionTypeVM = new ExceptionTypeVM
                //{
                //    ExceptionType = exception.ExceptionType,
                //    ExceptionTypeList = 
                //        new SelectList(_unitOfWork.ExceptionTypeRepository.GetList().OrderBy(x => x.ExceptionTypeName),
                //            "ExceptionTypeId", "ExceptionTypeName")
                //},
                AcademicYearVM = new AcademicYearVM
                {
                    AcademicYear = exception.AcademicYear,
                    AcademicYearList =
                        new SelectList(_unitOfWork.AcademicYearRepository.GetList().OrderBy(ay => ay.AcademicYearName),
                            "AcademicYearId", "AcademicYearName")
                },
            };

            return exceptionVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
