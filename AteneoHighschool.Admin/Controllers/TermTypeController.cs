﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class TermTypeController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "TermTypeName"; break;
                    default: sortProperty = "TermTypeName"; break;
                }

                var termType = _unitOfWork.TermTypeRepository.GetList(q => q.TermTypeName.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.TermTypeRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = termType
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var termTypeList = _unitOfWork.TermTypeRepository.GetList(q => q.TermTypeName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.TermTypeId, label = q.TermTypeName });
            return Json(termTypeList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var termType = new TermType
                {
                    TermTypeName = null
                };
                return PartialView(TermTypeVm(termType));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Term Type.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(TermType termType)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.TermTypeRepository.Insert(termType);
                Guid termTypeGuid = termType.TermTypeId;
                termType = _unitOfWork.TermTypeRepository.GetOne(q => q.TermTypeId == termTypeGuid);

                return Json(new { success = true, message = "You have successfully created a new Term Type: " + termType.TermTypeName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Term Type failed.Please check if there is already an existing Term Type named: " + termType.TermTypeName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                TermType termType = _unitOfWork.TermTypeRepository.GetOne(q => q.TermTypeId == id);
                return PartialView(TermTypeVm(termType));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Term Type.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(TermType termType)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.TermTypeRepository.Update(termType);

                return Json(new { success = true, message = "You have successfully edited a Term Type : " + termType.TermTypeName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Term Type named: " + termType.TermTypeName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                TermType termType = _unitOfWork.TermTypeRepository.GetOne(r => r.TermTypeId == id);
                return PartialView(TermTypeVm(termType));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Term Type.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(TermType termType)
        {
            try
            {
                _unitOfWork.TermTypeRepository.Delete(termType);

                return Json(new { success = true, message = "You have successfully deleted a Term Type : " + termType.TermTypeName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public TermTypeVM TermTypeVm(TermType termType)
        {
            var termTypeVm = new TermTypeVM
            {
                TermType = termType
            };
            return termTypeVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
