﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;
using AteneoHighschool.DataAccess.Utility;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AcademicYearController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "AcademicYearName"; break;
                    default: sortProperty = "AcademicYearName"; break;
                }

                var academicYear =
                    _unitOfWork.AcademicYearRepository.GetList(q => q.AcademicYearName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.AcademicYearRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = academicYear
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var academicYearList = _unitOfWork.AcademicYearRepository.GetList(q => q.AcademicYearName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.AcademicYearId, label = q.AcademicYearName });
            return Json(academicYearList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var academicYear = new AcademicYear
                {
                    AcademicYearName = null
                };
                return PartialView(AcademicYearVm(academicYear));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Academic Year.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(AcademicYear academicYear)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }
                _unitOfWork.AcademicYearRepository.Insert(academicYear);
                Guid academicYearGuid = academicYear.AcademicYearId;
                academicYear = _unitOfWork.AcademicYearRepository.GetOne(q => q.AcademicYearId == academicYearGuid);

                return Json(new { success = true, message = "You have successfully created a new Academic Year : " + academicYear.AcademicYearName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Academic Year failed. Please check if there is already an existing Academic Year : " + academicYear.AcademicYearName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                AcademicYear academicYear = _unitOfWork.AcademicYearRepository.GetOne(q => q.AcademicYearId == id);
                return PartialView(AcademicYearVm(academicYear));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit an Academic Year.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(AcademicYear academicYear)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.AcademicYearRepository.Update(academicYear);

                return Json(new { success = true, message = "You have successfully edited an Academic Year : " + academicYear.AcademicYearName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already an Academic Year named: " + academicYear.AcademicYearName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                AcademicYear academicYear = _unitOfWork.AcademicYearRepository.GetOne(r => r.AcademicYearId == id);
                return PartialView(AcademicYearVm(academicYear));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete an Academic Year.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(AcademicYear academicYear)
        {
            try
            {
                _unitOfWork.AcademicYearRepository.Delete(academicYear);

                return Json(new { success = true, message = "You have successfully deleted an Academic Year : " + academicYear.AcademicYearName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [ActionName("Detail")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Detail(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                AcademicYear academicYear = _unitOfWork.AcademicYearRepository.GetOne(q => q.AcademicYearId == id);
                return PartialView(AcademicYearVm(academicYear));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to view an Academic Year.";
            return PartialView("_Prohibited");
        }

        public ActionResult GenerateSyllabus(Guid academicYearId)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                AdoUtility.ExecuteNonQuery(@"DELETE Hats");
                AdoUtility.ExecuteNonQuery(@"DELETE Exception");
                AdoUtility.ExecuteNonQuery(@"EXEC DeleteAllPathwayItem");

                #region Temporary Code, needs to be polished

                IEnumerable<CurriculumClass> classList =
                    _unitOfWork.CurriculumClassRepository.GetList().Where(x => x.AcademicYearId == academicYearId);

                foreach (var classListItem in classList)
                {
                    var item = classListItem;
                    var curriculum = _unitOfWork.CurriculumRepository.GetOne(x => x.CurriculumId == item.CurriculumId);
                    var recurrence =
                        _unitOfWork.RecurrenceRepository.GetList(x => x.CurriculumClassId == item.CurriculumClassId);
                    var academicYear =
                        _unitOfWork.AcademicYearRepository.GetOne(x => x.AcademicYearId == item.AcademicYearId);

                    //var dateCounter = academicYear.StartDate;

                    //var loopCounter = Convert.ToInt32((academicYear.EndDate - academicYear.StartDate).TotalDays);

                    var dateCounter = Convert.ToDateTime("1/5/2015 12:00:00 AM");
                    var loopCounter =
                        Convert.ToInt32(
                            (Convert.ToDateTime("2/20/2015 12:00:00 AM") - Convert.ToDateTime("1/5/2015 12:00:00 AM"))
                                .TotalDays);

                    for (int counter = 0; counter <= loopCounter; counter++) //number of school days in a school year
                    {
                        if (dateCounter.DayOfWeek != DayOfWeek.Saturday && dateCounter.DayOfWeek != DayOfWeek.Sunday)
                            //school days excluding Saturday and Sunday
                        {
                            foreach (var recurrenceItem in recurrence)
                            {
                                var schedule =
                                    _unitOfWork.ScheduleRepository.GetOne(x => x.ScheduleId == recurrenceItem.ScheduleId);

                                if (dateCounter.DayOfWeek.ToString() == schedule.ScheduleName)
                                {
                                    var pathwayItem = new PathwayItem
                                    {
                                        StartDate = dateCounter + recurrenceItem.StartTime,
                                        EndDate = dateCounter + recurrenceItem.EndTime,
                                        CurriculumId = classListItem.CurriculumId,
                                        //CurriculumName = curriculum.CurriculumName,
                                        CurriculumClassId = classListItem.CurriculumClassId,
                                        //CurriculumClassName = classListItem.CurriculumClassName,
                                        AcademicYearId = classListItem.AcademicYearId,
                                        //AcademicYearName = academicYear.AcademicYearName,
                                    };
                                    _unitOfWork.PathwayItemRepository.Insert(pathwayItem);
                                }
                            }
                        }
                        dateCounter = dateCounter.AddDays(1);
                    }
                }

                #endregion Temporary Code, needs to be polished

                return RedirectToAction("Index", "AcademicYear");
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to generate Syllabus.";
            return PartialView("_Prohibited");
        }

        public AcademicYearVM AcademicYearVm(AcademicYear academicYear)
        {
            var academicYearVm = new AcademicYearVM
            {
                AcademicYear = academicYear
            };
            return academicYearVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}