﻿using System.Web.Mvc;
using System.Web.Security;
using System.Web.Routing;
using AteneoHighschool.Data.Access;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    public class AccountController : Controller
    {
        private GenericMembershipProvider _membershipProvider = new GenericMembershipProvider();
        private GenericRoleProvider _roleProvider = new GenericRoleProvider();

        protected override void Initialize(RequestContext requestContext)
        {
            if (_membershipProvider == null)
            {
                _membershipProvider = new GenericMembershipProvider();
            }

            if (_roleProvider == null)
            {
                _roleProvider = new GenericRoleProvider();
            }

            base.Initialize(requestContext);
        }

        [AllowAnonymous]
        public ActionResult LogIn()
        {
            var model = new LogIn();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LogIn(LogIn logIn)
        {
            if (ModelState.IsValid)
            {
                if (_membershipProvider.ValidateUser(logIn.UserName, logIn.Password))
                {
                    FormsAuthentication.SetAuthCookie(logIn.UserName, logIn.RememberMe);
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
            }
            return View(logIn);
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }
    }
}
