﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CurriculumController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "CurriculumName"; break;
                    default: sortProperty = "CurriculumName"; break;
                }

                var curriculum = _unitOfWork.CurriculumRepository.GetList(q => q.CurriculumName.Contains(searchText),
                    iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty,
                    sortOrder: param.sSortDir_0, includeProperties: "AcademicYear, Course");

                var recordCount = _unitOfWork.CurriculumRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = curriculum
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var curriculumList =
                _unitOfWork.CurriculumRepository.GetList(q => q.CurriculumName.Contains(searchText),
                    iDisplayLength: maxResult).Select(q => new {id = q.CurriculumId, label = q.CurriculumName});
            return Json(curriculumList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var curriculum = new Curriculum
                {
                    CurriculumName = null
                };
                return PartialView(CurriculumVm(curriculum));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Curriculum.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Curriculum curriculum)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CurriculumRepository.Insert(curriculum);
                Guid curriculumGuid = curriculum.CurriculumId;
                curriculum = _unitOfWork.CurriculumRepository.GetOne(q => q.CurriculumId == curriculumGuid);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully created a new Curriculum: " + curriculum.CurriculumName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Curriculum failed.Please check if there is already an existing Curriculum: " +
                    curriculum.CurriculumName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Curriculum curriculum = _unitOfWork.CurriculumRepository.GetOne(q => q.CurriculumId == id);
                return PartialView(CurriculumVm(curriculum));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Curriculum.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Curriculum curriculum)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CurriculumRepository.Update(curriculum);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully edited a Curriculum: " + curriculum.CurriculumName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a Curriculum named: " + curriculum.CurriculumName +
                    ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Curriculum curriculum = _unitOfWork.CurriculumRepository.GetOne(r => r.CurriculumId == id);
                return PartialView(CurriculumVm(curriculum));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Curriculum.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Curriculum curriculum)
        {
            try
            {
                _unitOfWork.CurriculumRepository.Delete(curriculum);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully deleted a Curriculum: " + curriculum.CurriculumName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [ActionName("Detail")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Detail(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Curriculum curriculum = _unitOfWork.CurriculumRepository.GetOne(q => q.CurriculumId == id);
                return PartialView(CurriculumVm(curriculum));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to view a Curriculum.";
            return PartialView("_Prohibited");
        }

        public ActionResult CurriculumItems(Guid curriclumId)
        {
            var curriculumItem = _unitOfWork.CurriculumItemRepository.GetList(q => q.CurriculumId == curriclumId,
                includeProperties: "Curriculum, Curriculum.AcademicYear, ProficiencyLevel");

            return PartialView("_CurriculumItem", curriculumItem);
        }

        public CurriculumVM CurriculumVm(Curriculum curriculum)
        {
            var curriculumVm = new CurriculumVM
            {
                Curriculum = curriculum,
                AcademicYearVM = new AcademicYearVM
                {
                    AcademicYear = curriculum.AcademicYear,
                    AcademicYearList =
                        new SelectList(_unitOfWork.AcademicYearRepository.GetList().OrderBy(ay => ay.AcademicYearName),
                            "AcademicYearId", "AcademicYearName")
                },
                CourseVM = new CourseVM
                {
                    Course = curriculum.Course,
                    CourseList = 
                        new SelectList(_unitOfWork.CourseRepository.GetList().OrderBy(c => c.CourseName),
                            "CourseId", "CourseName")
                }
            };
            return curriculumVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
