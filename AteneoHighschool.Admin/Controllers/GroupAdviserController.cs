﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class GroupAdviserController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "Group.GroupName"; break;
                    case 2: sortProperty = "Employee.FullName"; break;
                    default: sortProperty = "Group.GroupName"; break;
                }

                var groupAdviser = _unitOfWork.GroupAdviserRepository.GetList(q => q.Group.GroupName.Contains(searchText) || q.Employee.Person.FirstName.Contains(searchText) || q.Employee.Person.MiddleName.Contains(searchText) || q.Employee.Person.LastName.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0, includeProperties: "Employee, Employee.Person, Group");

                var recordCount = _unitOfWork.GroupAdviserRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = groupAdviser
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var groupAdviserList = _unitOfWork.GroupAdviserRepository.GetList(q => q.Group.GroupName.Contains(searchText) || q.Employee.Person.FirstName.Contains(searchText) || q.Employee.Person.MiddleName.Contains(searchText) || q.Employee.Person.LastName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.GroupAdviserId, label = q.Group.GroupName });
            return Json(groupAdviserList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var groupAdviser = new GroupAdviser
                {
                    Group = null,
                    Employee = null,
                };
                return PartialView(GroupAdviserVm(groupAdviser));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Group Adviser.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(GroupAdviser groupAdviser)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.GroupAdviserRepository.Insert(groupAdviser);
                Guid groupAdviserGuid = groupAdviser.GroupAdviserId;
                groupAdviser = _unitOfWork.GroupAdviserRepository.GetOne(q => q.GroupAdviserId == groupAdviserGuid,
                    includeProperties: "Group, Employee, Employee.Person");

                return Json(new { success = true, message = "You have successfully created a new Group Adviser: " + groupAdviser.Employee.Person.FullName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Group Adviser failed. Please check if there is already an existing Group Adviser: " +
                    groupAdviser.Employee.Person.FullName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                GroupAdviser groupAdviser = _unitOfWork.GroupAdviserRepository.GetOne(q => q.GroupAdviserId == id,
                    "Group, Employee");
                return PartialView(GroupAdviserVm(groupAdviser));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Group Adviser.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(GroupAdviser groupAdviser)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.GroupAdviserRepository.Update(groupAdviser);

                return Json(new { success = true, message = "You have successfully edited a Group Adviser." });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Group Adviser named.");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                GroupAdviser groupAdviser = _unitOfWork.GroupAdviserRepository.GetOne(r => r.GroupAdviserId == id);
                return PartialView(GroupAdviserVm(groupAdviser));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Group Adviser.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(GroupAdviser groupAdviser)
        {
            try
            {
                _unitOfWork.GroupAdviserRepository.Delete(groupAdviser);

                return Json(new { success = true, message = "You have successfully deleted a Group Adviser."});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public GroupAdviserVM GroupAdviserVm(GroupAdviser groupAdviser)
        {
            var groupAdviserVm = new GroupAdviserVM
            {
                GroupAdviser = groupAdviser,
                EmployeeVM = new EmployeeVM
                {
                    Employee = groupAdviser.Employee,
                    EmployeeList = new SelectList(_unitOfWork.EmployeeRepository.GetList(includeProperties: "Person").OrderBy(e => e.Person.FullName), "EmployeeId", "Person.FullName")
                },
                GroupVM = new GroupVM
                {
                    Group = groupAdviser.Group,
                    GroupList = new SelectList(_unitOfWork.GroupRepository.GetList().OrderBy(g => g.GroupName), "GroupId", "GroupName")
                },
            };

            return groupAdviserVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
