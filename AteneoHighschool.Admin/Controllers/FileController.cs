﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class FileController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            var file = _unitOfWork.FileRepository.GetList();

            return View(file);
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                File file = _unitOfWork.FileRepository.GetOne(q => q.FileId == id);
                return PartialView(FileVm(file));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a File.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(File file)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.FileRepository.Update(file);

                return Json(new { success = true, message = "You have successfully edited a File : " + file.FileName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a File named: " + file.FileName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult UpdateFileStatus(Guid[] fileId, string status)
        {
            var uniqueFileIds = fileId.Distinct().ToArray();

            var fileList = new List<File>();

            foreach (var item in uniqueFileIds)
            {
                var getFiles = _unitOfWork.FileRepository.GetList(q => q.FileId == item);
                fileList.AddRange(getFiles);
            }

            foreach (var item in fileList)
            {
                var updateFile = _unitOfWork.FileRepository.GetOne(q => q.FileId == item.FileId);
                updateFile.Status = status;
                _unitOfWork.FileRepository.Update(updateFile);
            }

            return RedirectToAction("Index");
        }

        public FileContentResult DownloadAttachment(Guid fileId)
        {
            var file = _unitOfWork.FileRepository.GetOne(q => q.FileId == fileId);
            return File(file.Data, file.MimeType, file.FileName);
        }

        public FileVM FileVm(File file)
        {
            var fileVm = new FileVM
            {
                File = file
            };
            return fileVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
