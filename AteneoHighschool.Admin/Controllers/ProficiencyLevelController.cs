﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ProficiencyLevelController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "ProficiencyLevelName"; break;
                    case 2: sortProperty = "Sequence"; break;
                    case 3: sortProperty = "Category.CategoryName"; break;
                    default: sortProperty = "Sequence"; break;
                }

                var proficiencyLevel = _unitOfWork.ProficiencyLevelRepository.GetList(q => q.ProficiencyLevelName.Contains(searchText) || q.Category.CategoryName.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0, includeProperties: "Category");

                var recordCount = _unitOfWork.ProficiencyLevelRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = proficiencyLevel
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var personList = _unitOfWork.PersonRepository.GetList(q => q.LastName.Contains(searchText) || q.FirstName.Contains(searchText) || q.MiddleName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.PersonId, label = q.FullName });
            return Json(personList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var proficiencyLevel = new ProficiencyLevel
                {
                    Category = null
                };
                return PartialView(ProficiencyLevelVm(proficiencyLevel));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Proficiency Level.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(ProficiencyLevel proficiencyLevel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.ProficiencyLevelRepository.Insert(proficiencyLevel);
                Guid proficiencyLevelGuid = proficiencyLevel.ProficiencyLevelId;
                proficiencyLevel =
                    _unitOfWork.ProficiencyLevelRepository.GetOne(q => q.ProficiencyLevelId == proficiencyLevelGuid,
                        includeProperties: "Category");

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully created a new Proficiency Level: " +
                                proficiencyLevel.ProficiencyLevelName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Proficiency Level failed.Please check if there is already an existing Proficiency Level : " +
                    proficiencyLevel.ProficiencyLevelName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                ProficiencyLevel proficiencyLevel =
                    _unitOfWork.ProficiencyLevelRepository.GetOne(q => q.ProficiencyLevelId == id,
                        "Category");
                return PartialView(ProficiencyLevelVm(proficiencyLevel));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Proficiency Level.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(ProficiencyLevel proficiencyLevel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.ProficiencyLevelRepository.Update(proficiencyLevel);

                return Json(new { success = true, message = "You have successfully edited a Proficiency Level : " + proficiencyLevel.ProficiencyLevelName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Proficiency Level named: " + proficiencyLevel.ProficiencyLevelName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                ProficiencyLevel proficiencyLevel =
                    _unitOfWork.ProficiencyLevelRepository.GetOne(r => r.ProficiencyLevelId == id);
                return PartialView(ProficiencyLevelVm(proficiencyLevel));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Proficiency Level.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(ProficiencyLevel proficiencyLevel)
        {
            try
            {
                _unitOfWork.ProficiencyLevelRepository.Delete(proficiencyLevel);

                return Json(new { success = true, message = "You have successfully deleted a Proficiency Level : " + proficiencyLevel.ProficiencyLevelName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ProficiencyLevelVM ProficiencyLevelVm(ProficiencyLevel proficiencyLevel)
        {
            var proficiencyLevelVm = new ProficiencyLevelVM
            {
                ProficiencyLevel = proficiencyLevel,
                CategoryVM = new CategoryVM
                {
                    Category = proficiencyLevel.Category,
                    CategoryList = new SelectList(_unitOfWork.CategoryRepository.GetList().OrderBy(c => c.Sequence), "CategoryId", "CategoryName")
                },
            };

            return proficiencyLevelVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }
    }
}
