﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    public class AnnouncementController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "AnnouncementName";
                        break;
                    case 2:
                        sortProperty = "Description";
                        break;
                    case 3:
                        sortProperty = "AcademicYear.AcademicYearName";
                        break;
                    case 4:
                        sortProperty = "StartDate";
                        break;
                    case 5:
                        sortProperty = "EndDate";
                        break;
                    default:
                        sortProperty = "AnnouncementName";
                        break;
                }

                var announcement =
                    _unitOfWork.AnnouncementRepository.GetList(
                        q =>
                            q.AnnouncementName.Contains(searchText) || q.Description.Contains(searchText) ||
                            q.AcademicYear.AcademicYearName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "AcademicYear");

                var recordCount = _unitOfWork.AnnouncementRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = announcement
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var announcementList =
                _unitOfWork.AnnouncementRepository.GetList(
                    q =>
                        q.AnnouncementName.Contains(searchText) || q.Description.Contains(searchText) ||
                        q.AcademicYear.AcademicYearName.Contains(searchText), iDisplayLength: maxResult)
                    .Select(q => new { id = q.AnnouncementId, label = q.AnnouncementName });
            return Json(announcementList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var announcement = new Announcement
                {
                    AcademicYear = null,
                };
                return PartialView(AnnouncementVm(announcement));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Announcement.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Announcement announcement)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.AnnouncementRepository.Insert(announcement);

                Guid announcementGuid = announcement.AnnouncementId;
                announcement = _unitOfWork.AnnouncementRepository.GetOne(q => q.AnnouncementId == announcementGuid,
                    includeProperties: "AcademicYear");

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully created a new Announcement: " + announcement.AnnouncementName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Announcement failed. Please check if there is already an existing Announcement: " +
                    announcement.AnnouncementName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Announcement announcement = _unitOfWork.AnnouncementRepository.GetOne(q => q.AnnouncementId == id,
                    "AcademicYear");
                return PartialView(AnnouncementVm(announcement));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit an Exception.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Announcement announcement)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.AnnouncementRepository.Update(announcement);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully edited an Announcement: " + announcement.AnnouncementName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already an Announcement named: " + announcement.AnnouncementName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Announcement announcement = _unitOfWork.AnnouncementRepository.GetOne(r => r.AnnouncementId == id);
                return PartialView(AnnouncementVm(announcement));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete an Announcement.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Announcement announcement)
        {
            try
            {
                _unitOfWork.AnnouncementRepository.Delete(announcement);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully deleted an Announcement: " + announcement.AnnouncementName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public AnnouncementVM AnnouncementVm(Announcement announcement)
        {
            var announcementVm = new AnnouncementVM
            {
                Announcement = announcement,
                AcademicYearVM = new AcademicYearVM
                {
                    AcademicYear = announcement.AcademicYear,
                    AcademicYearList =
                        new SelectList(_unitOfWork.AcademicYearRepository.GetList().OrderBy(ay => ay.AcademicYearName),
                            "AcademicYearId", "AcademicYearName")
                },
            };

            return announcementVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
