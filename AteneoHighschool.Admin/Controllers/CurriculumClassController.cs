﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CurriculumClassController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "CurriculumClassName"; break;
                    case 2: sortProperty = "Curriculum.CurriculumName"; break;
                    default: sortProperty = "CurriculumClassName"; break;
                }

                var curriculumClass =
                    _unitOfWork.CurriculumClassRepository.GetList(
                        q =>
                            q.CurriculumClassName.Contains(searchText) ||
                            q.Curriculum.CurriculumName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "Curriculum, Group, AcademicYear");

                var recordCount = _unitOfWork.CurriculumClassRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = curriculumClass
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var curriculumClassList =
                _unitOfWork.CurriculumClassRepository.GetList(
                    q => q.CurriculumClassName.Contains(searchText) || q.Curriculum.CurriculumName.Contains(searchText),
                    iDisplayLength: maxResult)
                    .Select(q => new {id = q.CurriculumClassId, label = q.CurriculumClassName});
            return Json(curriculumClassList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var curriculumClass = new CurriculumClass
                {
                    Group = null,
                    Curriculum = null,
                };
                return PartialView(CurriculumClassVm(curriculumClass));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Curriculum Class.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(CurriculumClass curriculumClass)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                var group = _unitOfWork.GroupRepository.GetOne(q => q.GroupId == curriculumClass.GroupId);
                var curriculum = _unitOfWork.CurriculumRepository.GetOne(q => q.CurriculumId == curriculumClass.CurriculumId);

                curriculumClass.CurriculumClassName = group.GroupName + " (" + curriculum.CurriculumName + ")";

                _unitOfWork.CurriculumClassRepository.Insert(curriculumClass);
                Guid curriculumClassGuid = curriculumClass.CurriculumClassId;
                curriculumClass =
                    _unitOfWork.CurriculumClassRepository.GetOne(q => q.CurriculumClassId == curriculumClassGuid,
                        "Curriculum, Group");

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully created a new Curriculum Class: " +
                                curriculumClass.Group.GroupName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Curriculum Class failed. Please check if there is already an existing Curriculum Class: " +
                    curriculumClass.CurriculumClassName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                CurriculumClass curriculumClass =
                    _unitOfWork.CurriculumClassRepository.GetOne(q => q.CurriculumClassId == id,
                        "Curriculum, Group");
                return PartialView(CurriculumClassVm(curriculumClass));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Curriculum Class.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(CurriculumClass curriculumClass)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CurriculumClassRepository.Update(curriculumClass);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully edited a Curriculum Class: " +
                                curriculumClass.CurriculumClassName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a Curriculum Class named: " +
                    curriculumClass.CurriculumClassName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                CurriculumClass curriculumClass =
                    _unitOfWork.CurriculumClassRepository.GetOne(r => r.CurriculumClassId == id,
                        "Curriculum, Group");
                return PartialView(CurriculumClassVm(curriculumClass));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Curriculum Class.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(CurriculumClass curriculumClass)
        {
            try
            {
                _unitOfWork.CurriculumClassRepository.Delete(curriculumClass);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully deleted a Curriculum Class: " +
                                curriculumClass.CurriculumClassName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public CurriculumClassVM CurriculumClassVm(CurriculumClass curriculumClass)
        {
            var curriculumClassVm = new CurriculumClassVM
            {
                CurriculumClass = curriculumClass,
                CurriculumVM = new CurriculumVM
                {
                    Curriculum = curriculumClass.Curriculum,
                    CurriculumList =
                        new SelectList(_unitOfWork.CurriculumRepository.GetList().OrderBy(c => c.CurriculumName),
                            "CurriculumId", "CurriculumName")
                },
                GroupVM = new GroupVM
                {
                    Group = curriculumClass.Group,
                    GroupList =
                        new SelectList(_unitOfWork.GroupRepository.GetList().OrderBy(g => g.GroupName), "GroupId",
                            "GroupName")
                },
                AcademicYearVM = new AcademicYearVM
                {
                    AcademicYear = curriculumClass.AcademicYear,
                    AcademicYearList = 
                        new SelectList(_unitOfWork.AcademicYearRepository.GetList().OrderBy(ay => ay.AcademicYearName), "AcademicYearId",
                            "AcademicYearName")
                },
            };

            return curriculumClassVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
