﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CourseController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "CourseName"; break;
                    case 2: sortProperty = "Campus.CampusName"; break;
                    case 3: sortProperty = "CourseCode"; break;
                    default: sortProperty = "CourseName"; break;
                }

                var course =
                    _unitOfWork.CourseRepository.GetList(
                        q =>
                            q.CourseName.Contains(searchText) || q.CourseCode.Contains(searchText) ||
                            q.Campus.CampusName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0, includeProperties: "Campus");

                var recordCount = _unitOfWork.CourseRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = course
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var courseList =
                _unitOfWork.CourseRepository.GetList(
                    q =>
                        q.CourseName.Contains(searchText) || q.CourseCode.Contains(searchText) ||
                        q.Campus.CampusName.Contains(searchText),
                    iDisplayLength: maxResult)
                    .Select(q => new {id = q.CourseId, label = q.CourseName});
            return Json(courseList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var course = new Course
                {
                    Campus = null,
                };
                return PartialView(CourseVm(course));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Course.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Course course)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CourseRepository.Insert(course);
                Guid courseGuid = course.CourseId;
                course = _unitOfWork.CourseRepository.GetOne(q => q.CourseId == courseGuid, includeProperties: "Campus");

                return Json(new { success = true, message = "You have successfully created a new Course : " + course.CourseName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Course failed. Please check if there is already an existing Course : " + course.CourseName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Course course = _unitOfWork.CourseRepository.GetOne(q => q.CourseId == id, includeProperties: "Campus");
                return PartialView(CourseVm(course));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Course.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Course course)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CourseRepository.Update(course);

                return Json(new { success = true, message = "You have successfully edited a Course : " + course.CourseName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Course named: " + course.CourseName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Course course = _unitOfWork.CourseRepository.GetOne(r => r.CourseId == id);
                return PartialView(CourseVm(course));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Course.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Course course)
        {
            try
            {
                _unitOfWork.CourseRepository.Delete(course);

                return Json(new { success = true, message = "You have successfully deleted a Course : " + course.CourseName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public CourseVM CourseVm(Course course)
        {
            var courseVm = new CourseVM
            {
                Course = course,
                CampusVM = new CampusVM
                {
                    Campus = course.Campus,
                    CampusList =
                        new SelectList(_unitOfWork.CampusRepository.GetList().OrderBy(c => c.CampusName), "CampusId",
                            "CampusName")
                },
            };
            return courseVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
