﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Subject Teacher, Subject Coordinator, Level Leader")]
    public class TeacherFileController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            var userId = _unitOfWork.UserRepository.GetOne(q => q.UserName == User.Identity.Name).UserId;
            var employeeId = _unitOfWork.EmployeeRepository.GetOne(q => q.Person.PersonId == userId).EmployeeId;
            var teacherFile = _unitOfWork.TeacherFileRepository.GetList(q => q.EmployeeId == employeeId, includeProperties: "File, Employee, Employee.Person");

            return View(teacherFile);
        }
    }
}
