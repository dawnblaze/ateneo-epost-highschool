﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CategoryController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "CategoryName"; break;
                    default: sortProperty = "Sequence"; break;
                }

                var category = _unitOfWork.CategoryRepository.GetList(q => q.CategoryName.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.CategoryRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = category
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var categoryList = _unitOfWork.CategoryRepository.GetList(q => q.CategoryName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.CategoryId, label = q.CategoryName });
            return Json(categoryList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var category = new Category
                {
                    CategoryName = null
                };
                return PartialView(CategoryVm(category));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Category.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Category category)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CategoryRepository.Insert(category);
                Guid categoryGuid = category.CategoryId;
                category = _unitOfWork.CategoryRepository.GetOne(q => q.CategoryId == categoryGuid);

                return Json(new { success = true, message = "You have successfully created a new Category : " + category.CategoryName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Category failed.Please check if there is already an existing Category : " + category.CategoryName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Category category = _unitOfWork.CategoryRepository.GetOne(q => q.CategoryId == id);
                return PartialView(CategoryVm(category));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Category.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Category category)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.CategoryRepository.Update(category);

                return Json(new { success = true, message = "You have successfully edited a Category : " + category.CategoryName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Category named: " + category.CategoryName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Category category = _unitOfWork.CategoryRepository.GetOne(r => r.CategoryId == id);
                return PartialView(CategoryVm(category));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Category.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Category category)
        {
            try
            {
                _unitOfWork.CategoryRepository.Delete(category);

                return Json(new { success = true, message = "You have successfully deleted a Category : " + category.CategoryName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public CategoryVM CategoryVm(Category category)
        {
            var categoryVm = new CategoryVM
            {
                Category = category,

            };
            return categoryVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
