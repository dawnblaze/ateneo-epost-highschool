﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ScheduleController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "Sequence"; break;
                    default: sortProperty = "Sequence"; break;
                }

                var academicYear = _unitOfWork.ScheduleRepository.GetList(q => q.ScheduleName.Contains(searchText),
                    iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty,
                    sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.ScheduleRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = academicYear
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var scheduleList =
                _unitOfWork.ScheduleRepository.GetList(q => q.ScheduleName.Contains(searchText),
                    iDisplayLength: maxResult).Select(q => new { id = q.ScheduleId, label = q.ScheduleName });
            return Json(scheduleList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var schedule = new Schedule
                {
                    ScheduleName = null
                };
                return PartialView(ScheduleVm(schedule));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Schedule.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Schedule schedule)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.ScheduleRepository.Insert(schedule);
                Guid scheduleGuid = schedule.ScheduleId;
                schedule = _unitOfWork.ScheduleRepository.GetOne(q => q.ScheduleId == scheduleGuid);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully created a new Schedule : " + schedule.ScheduleName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Schedule failed. Please check if there is already an existing Schedule : " +
                    schedule.ScheduleName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Schedule schedule = _unitOfWork.ScheduleRepository.GetOne(q => q.ScheduleId == id);
                return PartialView(ScheduleVm(schedule));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Schedule.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Schedule schedule)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.ScheduleRepository.Update(schedule);

                return Json(new { success = true, message = "You have successfully edited a Schedule : " + schedule.ScheduleName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Schedule named: " + schedule.ScheduleName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Schedule schedule = _unitOfWork.ScheduleRepository.GetOne(r => r.ScheduleId == id);
                return PartialView(ScheduleVm(schedule));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Schedule.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Schedule schedule)
        {
            try
            {
                _unitOfWork.ScheduleRepository.Delete(schedule);

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully deleted a Schedule : " + schedule.ScheduleName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ScheduleVM ScheduleVm(Schedule schedule)
        {
            var scheduleVm = new ScheduleVM
            {
                Schedule = schedule
            };
            return scheduleVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
