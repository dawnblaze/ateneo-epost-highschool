﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    public class LevelLeaderController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "ProficiencyLevel.ProficiencyLevelName";
                        break;
                    case 2:
                        sortProperty = "Employee.Person.FullName";
                        break;
                    default:
                        sortProperty = "ProficiencyLevel.ProficiencyLevelName";
                        break;
                }

                var levelLeader =
                    _unitOfWork.LevelLeaderRepository.GetList(
                        q =>
                            //q.Employee.Person.FullName.Contains(searchText) ||
                            q.ProficiencyLevel.ProficiencyLevelName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "Employee, ProficiencyLevel, Employee.Person");

                var recordCount = _unitOfWork.LevelLeaderRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = levelLeader
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var levelLeaderList =
                _unitOfWork.LevelLeaderRepository.GetList(
                    q =>
                        //q.Employee.Person.FullName.Contains(searchText) ||
                        q.ProficiencyLevel.ProficiencyLevelName.Contains(searchText),
                    iDisplayLength: maxResult, includeProperties: "Employee, Employee.Person, ProficiencyLevel")
                    .Select(q => new { id = q.LevelLeaderId, label = q.Employee.Person.FullName });
            return Json(levelLeaderList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var levelLeader = new LevelLeader
                {
                    Employee = null,
                    ProficiencyLevel = null,
                };
                return PartialView(LevelLeaderVm(levelLeader));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Level Leader.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(LevelLeader levelLeader)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }
                _unitOfWork.LevelLeaderRepository.Insert(levelLeader);
                Guid levelLeaderGuid = levelLeader.LevelLeaderId;
                levelLeader =
                    _unitOfWork.LevelLeaderRepository.GetOne(
                        q => q.LevelLeaderId == levelLeaderGuid,
                        includeProperties: "Employee, Employee.Person, ProficiencyLevel");

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully created a new Level Leader : " +
                                levelLeader.Employee.Person.FullName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Level Leader failed. Please check if there is already an existing Level Leader : " +
                    levelLeader.Employee.Person.FullName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var levelLeader =
                    _unitOfWork.LevelLeaderRepository.GetOne(q => q.LevelLeaderId == id,
                        "Employee");
                return PartialView(LevelLeaderVm(levelLeader));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Level Leader.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(LevelLeader levelLeader)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.LevelLeaderRepository.Update(levelLeader);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully edited a Level Leader : " +
                                levelLeader.LevelLeaderId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a Level Leader named: " +
                    levelLeader.LevelLeaderId + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var levelLeader =
                    _unitOfWork.LevelLeaderRepository.GetOne(q => q.LevelLeaderId == id);
                return PartialView(LevelLeaderVm(levelLeader));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Level Leader.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(LevelLeader levelLeader)
        {
            try
            {
                _unitOfWork.LevelLeaderRepository.Delete(levelLeader);

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully deleted a Level Leader : " +
                                levelLeader.LevelLeaderId
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public LevelLeaderVM LevelLeaderVm(LevelLeader levelLeader)
        {
            var levelLeaderVm = new LevelLeaderVM
            {
                LevelLeader = levelLeader,
                EmployeeVM = new EmployeeVM
                {
                    Employee = levelLeader.Employee,
                    EmployeeList =
                        new SelectList(
                            _unitOfWork.EmployeeRepository.GetList(includeProperties: "Person")
                                .OrderBy(q => q.Person.FullName),
                            "EmployeeId", "Person.FullName")
                },
                ProficiencyLevelVM = new ProficiencyLevelVM
                {
                    ProficiencyLevel = levelLeader.ProficiencyLevel,
                    ProficiencyLevelList = 
                        new SelectList(_unitOfWork.ProficiencyLevelRepository.GetList().OrderBy(q => q.Sequence),
                            "ProficiencyLevelId", "ProficiencyLevelName")
                }
            };
            return levelLeaderVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
