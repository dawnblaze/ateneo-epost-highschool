﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class PersonController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            ViewBag.Test = "Watatops";
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);
                //var x = PasswordRepository.GetPassword("welcome");
                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "FullName";
                        break;
                    case 2:
                        sortProperty = "Gender.GenderName";
                        break;
                    case 3:
                        sortProperty = "Age";
                        break;
                    default:
                        sortProperty = "FullName";
                        break;
                }

                var person =
                    _unitOfWork.PersonRepository.GetList(
                        q =>
                            q.FirstName.Contains(searchText) || q.MiddleName.Contains(searchText) ||
                            q.LastName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0, includeProperties: "Gender");

                var recordCount = _unitOfWork.PersonRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = person
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var personList =
                _unitOfWork.PersonRepository.GetList(
                    q =>
                        q.LastName.Contains(searchText) || q.FirstName.Contains(searchText) ||
                        q.MiddleName.Contains(searchText), iDisplayLength: maxResult)
                    .Select(q => new {id = q.PersonId, label = q.FullName});
            return Json(personList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var person = new Person
                {
                    Gender = null,
                    CivilStatus = null,
                    Prefix = null
                };
                return PartialView(PersonVm(person));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Person.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Person person)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }
                person.LastName = "(Test) " + person.LastName; //Temporary Code, for plotting Grade 6 data only
                person.FullName = person.LastName + ", " + person.FirstName + " " + person.MiddleName + " " +
                                  person.Suffix;
                _unitOfWork.PersonRepository.Insert(person);
                Guid personGuid = person.PersonId;
                person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == personGuid,
                    includeProperties: "Gender, Prefix, CivilStatus");

                return
                    Json(
                        new {success = true, message = "You have successfully created a new Person: " + person.FullName});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Person failed. Please check if there is already an existing Person : " +
                    person.FullName + "." + ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Person person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == id,
                    includeProperties: "Gender, Prefix, CivilStatus");
                return PartialView(PersonVm(person));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a new Person.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Person person)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }
                person.FullName = person.LastName + ", " + person.FirstName + " " + person.MiddleName + " " +
                                  person.Suffix;
                _unitOfWork.PersonRepository.Update(person);

                return Json(new {success = true, message = "You have successfully edited a Person : " + person.FullName});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a Person named: " + person.FullName + ".");
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Person person = _unitOfWork.PersonRepository.GetOne(r => r.PersonId == id);
                return PartialView(PersonVm(person));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a new Person.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Person person)
        {
            try
            {
                _unitOfWork.PersonRepository.Delete(person);

                return
                    Json(new {success = true, message = "You have successfully deleted a Person : " + person.FullName});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [ActionName("Detail")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Detail(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Person person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == id);
                return PartialView(PersonVm(person));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to view a Person.";
            return PartialView("_Prohibited");
        }

        public PersonVM PersonVm(Person person)
        {
            var personVm = new PersonVM
            {
                Person = person,
                GenderVM = new GenderVM
                {
                    Gender = person.Gender,
                    GenderList =
                        new SelectList(_unitOfWork.GenderRepository.GetList().OrderBy(g => g.GenderName), "GenderId",
                            "GenderName")
                },
                CivilStatusVM = new CivilStatusVM
                {
                    CivilStatus = person.CivilStatus,
                    CivilStatusList =
                        new SelectList(_unitOfWork.CivilStatusRepository.GetList().OrderBy(cs => cs.CivilStatusName),
                            "CivilStatusId", "CivilStatusName")
                },
                PrefixVM = new PrefixVM
                {
                    Prefix = person.Prefix,
                    PrefixList =
                        new SelectList(_unitOfWork.PrefixRepository.GetList().OrderBy(p => p.PrefixName), "PrefixId",
                            "PrefixName")
                },
            };

            return personVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}