﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;
using AteneoHighschool.DataAccess.Utility;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class TermController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "TermName"; break;
                    case 2: sortProperty = "AcademicYear.AcademicYearName"; break;
                    case 3: sortProperty = "TermType.TermTypeName"; break;
                    case 4: sortProperty = "StartDate"; break;
                    case 5: sortProperty = "EndDate"; break;
                    case 6: sortProperty = "IsActive"; break;
                    default: sortProperty = "TermName"; break;
                }

                var term = _unitOfWork.TermRepository.GetList(q => q.TermName.Contains(searchText) || q.AcademicYear.AcademicYearName.Contains(searchText) || q.TermType.TermTypeName.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0, includeProperties: "AcademicYear, TermType");

                var recordCount = _unitOfWork.TermRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = term
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var termList = _unitOfWork.TermRepository.GetList(q => q.TermName.Contains(searchText) || q.AcademicYear.AcademicYearName.Contains(searchText) || q.TermType.TermTypeName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.TermId, label = q.TermName });
            return Json(termList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var term = new Term
                {
                    AcademicYear = null,
                    TermType = null
                };
                return PartialView(TermVm(term));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Term.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Term term)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.TermRepository.Insert(term);
                Guid termGuid = term.TermId;
                term = _unitOfWork.TermRepository.GetOne(q => q.TermId == termGuid, includeProperties: "AcademicYear, TermType");

                return Json(new { success = true, message = "You have successfully created a new Term : " + term.TermName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Term failed.Please check if there is already an existing Term : " + term.TermName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Term term = _unitOfWork.TermRepository.GetOne(q => q.TermId == id,
                    "AcademicYear, TermType");
                return PartialView(TermVm(term));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Term.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Term term)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.TermRepository.Update(term);

                return Json(new { success = true, message = "You have successfully edited a Term : " + term.TermName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Term named: " + term.TermName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Term term = _unitOfWork.TermRepository.GetOne(r => r.TermId == id);
                return PartialView(TermVm(term));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Term.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Term term)
        {
            try
            {
                _unitOfWork.TermRepository.Delete(term);

                return Json(new { success = true, message = "You have successfully deleted a Term : " + term.TermName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [ActionName("Detail")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Detail(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Term term = _unitOfWork.TermRepository.GetOne(q => q.TermId == id);
                return PartialView(TermVm(term));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to view a Term.";
            return PartialView("_Prohibited");
        }

        public ActionResult GenerateTermSyllabus(Guid termId)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                #region Temporary Code, needs to be polished
                var term = _unitOfWork.TermRepository.GetOne(q => q.TermId == termId);

                AdoUtility.ExecuteNonQuery(string.Format(@"EXEC DeleteTermSyllabus @StartDate = '{0}', @EndDate = '{1}'",
                term.StartDate, term.EndDate.Add(new TimeSpan(0, 23, 0, 0))));

                IEnumerable<CurriculumClass> classList =
                    _unitOfWork.CurriculumClassRepository.GetList().Where(x => x.AcademicYearId == term.AcademicYearId);

                foreach (var classListItem in classList)
                {
                    var item = classListItem;
                    var curriculum = _unitOfWork.CurriculumRepository.GetOne(x => x.CurriculumId == item.CurriculumId);
                    var recurrence =
                        _unitOfWork.RecurrenceRepository.GetList(x => x.CurriculumClassId == item.CurriculumClassId);
                    var academicYear =
                        _unitOfWork.AcademicYearRepository.GetOne(x => x.AcademicYearId == item.AcademicYearId);

                    var dateCounter = term.StartDate;
                    var loopCounter =
                        Convert.ToInt32(
                            (term.EndDate - term.StartDate)
                                .TotalDays);

                    for (var counter = 0; counter <= loopCounter; counter++) //number of school days in a school year
                    {
                        if (dateCounter.DayOfWeek != DayOfWeek.Saturday && dateCounter.DayOfWeek != DayOfWeek.Sunday)
                        //school days excluding Saturday and Sunday
                        {
                            foreach (var recurrenceItem in recurrence)
                            {
                                var schedule =
                                    _unitOfWork.ScheduleRepository.GetOne(x => x.ScheduleId == recurrenceItem.ScheduleId);

                                if (dateCounter.DayOfWeek.ToString() == schedule.ScheduleName)
                                {
                                    var pathwayItem = new PathwayItem
                                    {
                                        StartDate = dateCounter + recurrenceItem.StartTime,
                                        EndDate = dateCounter + recurrenceItem.EndTime,
                                        CurriculumId = classListItem.CurriculumId,
                                        //CurriculumName = curriculum.CurriculumName,
                                        CurriculumClassId = classListItem.CurriculumClassId,
                                        //CurriculumClassName = classListItem.CurriculumClassName,
                                        AcademicYearId = classListItem.AcademicYearId,
                                        //AcademicYearName = academicYear.AcademicYearName,
                                        IsActive = true,
                                    };
                                    _unitOfWork.PathwayItemRepository.Insert(pathwayItem);
                                }
                            }
                        }
                        dateCounter = dateCounter.AddDays(1);
                    }
                }

                var exception =
                    _unitOfWork.ExceptionRepository.GetList(
                        q => q.StartDate >= term.StartDate && q.StartDate <= term.EndDate);

                foreach (var item in exception)
                {
                    AdoUtility.ExecuteNonQuery(@"DELETE PathwayItem WHERE CONVERT(DATE, StartDate, 112) = CONVERT(DATE, '" +
                                       item.StartDate.ToString("s") + "', 112)");
                }

                #endregion Temporary Code, needs to be polished

                return RedirectToAction("Index", "Term");
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to generate Syllabus.";
            return PartialView("_Prohibited");
        }

        public TermVM TermVm(Term term)
        {
            var termVm = new TermVM
            {
                Term = term,
                AcademicYearVM = new AcademicYearVM
                {
                    AcademicYear = term.AcademicYear,
                    AcademicYearList = new SelectList(_unitOfWork.AcademicYearRepository.GetList().OrderBy(ay => ay.StartDate), "AcademicYearId", "AcademicYearName")
                },
                TermTypeVM = new TermTypeVM
                {
                    TermType = term.TermType,
                    TermTypeList = 
                        new SelectList(_unitOfWork.TermTypeRepository.GetList().OrderBy(tt => tt.TermTypeName), "TermTypeId", "TermTypeName")
                },
            };

            return termVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
