﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class GroupMemberController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "Group.GroupName";
                        break;
                    case 2:
                        sortProperty = "Student.Person.FullName";
                        break;
                    default:
                        sortProperty = "Group.GroupName";
                        break;
                }

                var groupMember =
                    _unitOfWork.GroupMemberRepository.GetList(
                        q =>
                            q.Student.Person.FirstName.Contains(searchText) ||
                            q.Student.Person.MiddleName.Contains(searchText) ||
                            q.Student.Person.LastName.Contains(searchText) || q.Group.GroupName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "Group, Student, Student.Person");

                var recordCount = _unitOfWork.EmployeeRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = groupMember
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var groupMemberList =
                _unitOfWork.GroupMemberRepository.GetList(
                    q =>
                        q.Student.Person.FirstName.Contains(searchText) ||
                        q.Student.Person.MiddleName.Contains(searchText) ||
                        q.Student.Person.LastName.Contains(searchText) || q.Group.GroupName.Contains(searchText),
                    iDisplayLength: maxResult).Select(q => new {id = q.GroupMemberId, label = q.Group.GroupName});
            return Json(groupMemberList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var groupMember = new GroupMember
                {
                    Student = null,
                    Group = null,
                };
                return PartialView(GroupMemberVm(groupMember));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Group Member.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(GroupMember groupMember)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.GroupMemberRepository.Insert(groupMember);
                Guid groupMemberGuid = groupMember.GroupMemberId;
                groupMember = _unitOfWork.GroupMemberRepository.GetOne(q => q.GroupMemberId == groupMemberGuid,
                    includeProperties: "Group, Student, Student.Person");

                return
                    Json(
                        new
                        {
                            success = true,
                            message =
                                "You have successfully created a new Group Member : " +
                                groupMember.Student.Person.FullName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Group Member failed.Please check if there is already an existing Group Member : " +
                    groupMember.Student.Person.FullName + "." + ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                GroupMember groupMember = _unitOfWork.GroupMemberRepository.GetOne(q => q.GroupMemberId == id,
                    "Group, Student");
                return PartialView(GroupMemberVm(groupMember));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Group Member.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(GroupMember groupMember)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.GroupMemberRepository.Update(groupMember);

                return Json(new {success = true, message = "You have successfully edited a Group Member."});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Group Member.");
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                GroupMember groupMember = _unitOfWork.GroupMemberRepository.GetOne(r => r.GroupMemberId == id);
                return PartialView(GroupMemberVm(groupMember));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Group Member.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(GroupMember groupMember)
        {
            try
            {
                _unitOfWork.GroupMemberRepository.Delete(groupMember);

                return Json(new {success = true, message = "You have successfully deleted a Group Member."});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public GroupMemberVM GroupMemberVm(GroupMember groupMember)
        {
            var groupMemberVm = new GroupMemberVM
            {
                GroupMember = groupMember,
                GroupVM = new GroupVM
                {
                    Group = groupMember.Group,
                    GroupList =
                        new SelectList(_unitOfWork.GroupRepository.GetList().OrderBy(g => g.GroupName), "GroupId",
                            "GroupName")
                },
                StudentVM = new StudentVM
                {
                    Student = groupMember.Student,
                    StudentList =
                        new SelectList(
                            _unitOfWork.StudentRepository.GetList(includeProperties: "Person")
                                .OrderBy(s => s.Person.FullName), "StudentId", "Person.FullName")
                },
            };
            return groupMemberVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
