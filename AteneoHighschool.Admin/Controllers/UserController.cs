﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericMembershipProvider _membershipProvider = new GenericMembershipProvider();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);
                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";
                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "UserName"; break;
                    case 2: sortProperty = "AccessFailedCount"; break;
                    case 3: sortProperty = "IsLockedOut"; break;
                    case 4: sortProperty = "IsActive"; break;
                    default: sortProperty = "UserName"; break;
                }

                var user =
                    _unitOfWork.UserRepository.GetList(
                        q => q.UserName.Contains(searchText) || q.Email.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.UserRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = user
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var userList = _unitOfWork.UserRepository.GetList(q => q.UserName.Contains(searchText) || q.Email.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.UserId, label = q.UserName });
            return Json(userList);
        }

        public ActionResult Create(Guid personId)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var person = _unitOfWork.PersonRepository.GetOne(q => q.PersonId == personId);
                var userName = person.LastName.ToLower() + "." +
                               Regex.Replace(person.FirstName.Trim().ToLower(), @"\s+", "");
                //var x = PasswordRepository.GetPassword("panares.maryjane");
                var user = new User
                {
                    UserId = personId,
                    UserName = userName,
                    Password = userName,
                    Email = "name@email.com",
                };
                return PartialView(UserVm(user));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new User.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var duplicateUser = _unitOfWork.UserRepository.GetOne(q => q.UserId == user.UserId);
                    if (duplicateUser == null)
                    {
                        if (_membershipProvider.CreateUser(user.UserId, user.UserName, user.Email))
                        {
                            return RedirectToAction("Index", "User");
                        }
                    }
                }
                catch (ArgumentException ae)
                {
                    ModelState.AddModelError("", ae.Message);
                }
            }
            ViewBag.ErrorMessage = "Error creating new User.";
            return PartialView(UserVm(user));
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var user = _unitOfWork.UserRepository.GetOne(q => q.UserId == id);
                return PartialView(UserVm(user));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a new User.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(User user)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }
                _unitOfWork.UserRepository.Update(user);

                return Json(new { success = true, message = "You have successfully edited a User: " + user.UserName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    ae.Message + "Please check if there is already a User named: " + user.UserName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [ActionName("Detail")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Detail(Guid id, string successMessage = "")
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                ViewBag.SuccessMessage = successMessage;
                User user = _unitOfWork.UserRepository.GetOne(q => q.UserId == id);
                return PartialView(UserVm(user));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to view a User.";
            return PartialView("_Prohibited");
        }

        public UserVM UserVm(User user)
        {
            var userVm = new UserVM
            {
                User = user
            };
            return userVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
