﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class PrefixController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "PrefixName"; break;
                    default: sortProperty = "PrefixName"; break;
                }

                var prefix = _unitOfWork.PrefixRepository.GetList(q => q.PrefixName.Contains(searchText),
               iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty, sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.PrefixRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = prefix
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var prefixList = _unitOfWork.PrefixRepository.GetList(q => q.PrefixName.Contains(searchText), iDisplayLength: maxResult).Select(q => new { id = q.PrefixId, label = q.PrefixName });
            return Json(prefixList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var prefix = new Prefix
                {
                    PrefixName = null
                };
                return PartialView(PrefixVm(prefix));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Prefix.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Prefix prefix)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.PrefixRepository.Insert(prefix);
                Guid prefixGuid = prefix.PrefixId;
                prefix = _unitOfWork.PrefixRepository.GetOne(q => q.PrefixId == prefixGuid);

                return Json(new { success = true, message = "You have successfully created a new Prefix : " + prefix.PrefixName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Prefix failed.Please check if there is already an existing Prefix named: " + prefix.PrefixName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Prefix prefix = _unitOfWork.PrefixRepository.GetOne(q => q.PrefixId == id);
                return PartialView(PrefixVm(prefix));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Prefix.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Prefix prefix)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.PrefixRepository.Update(prefix);

                return Json(new { success = true, message = "You have successfully edited a Prefix : " + prefix.PrefixName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Prefix named: " + prefix.PrefixName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Prefix prefix = _unitOfWork.PrefixRepository.GetOne(r => r.PrefixId == id);
                return PartialView(PrefixVm(prefix));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Prefix.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Prefix prefix)
        {
            try
            {
                _unitOfWork.PrefixRepository.Delete(prefix);

                return Json(new { success = true, message = "You have successfully deleted a Prefix : " + prefix.PrefixName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public PrefixVM PrefixVm(Prefix prefix)
        {
            var prefixVm = new PrefixVM
            {
                Prefix = prefix
            };
            return prefixVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
