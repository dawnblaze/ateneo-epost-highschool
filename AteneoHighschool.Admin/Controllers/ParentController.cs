﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ParentController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1:
                        sortProperty = "Person.FullName";
                        break;
                    case 4:
                        sortProperty = "Person.Gender";
                        break;
                    case 5:
                        sortProperty = "Person.Age";
                        break;
                    default:
                        sortProperty = "Person.FullName";
                        break;
                }

                var parent =
                    _unitOfWork.ParentRepository.GetList(
                        q =>
                            q.Person.FirstName.Contains(searchText) || q.Person.MiddleName.Contains(searchText) ||
                            q.Person.LastName.Contains(searchText),
                        iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength,
                        sortProperty: sortProperty, sortOrder: param.sSortDir_0,
                        includeProperties: "Person, Person.Gender");

                var recordCount = _unitOfWork.EmployeeRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = parent
                },
                    JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var parentList =
                _unitOfWork.ParentRepository.GetList(
                    q =>
                        q.Person.FirstName.Contains(searchText) || q.Person.MiddleName.Contains(searchText) ||
                        q.Person.LastName.Contains(searchText), iDisplayLength: maxResult)
                    .Select(q => new {id = q.ParentId, label = q.Person.FullName});
            return Json(parentList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var parent = new Parent
                {
                    Person = null,
                };
                return PartialView(ParentVm(parent));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Parent.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Parent parent)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }
                _unitOfWork.ParentRepository.Insert(parent);
                Guid parentGuid = parent.ParentId;
                parent = _unitOfWork.ParentRepository.GetOne(q => q.ParentId == parentGuid, includeProperties: "Person");

                return
                    Json(
                        new
                        {
                            success = true,
                            message = "You have successfully created a new Parent : " + parent.Person.FullName
                        });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("",
                    "Creation of new Employee failed.Please check if there is already an existing Parent : " +
                    parent.Person.FullName + "." + ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Parent parent = _unitOfWork.ParentRepository.GetOne(q => q.ParentId == id, includeProperties: "Person");
                return PartialView(ParentVm(parent));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Parent.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Parent parent)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {success = false, error = GetErrorsFromModelState()});
                }

                _unitOfWork.ParentRepository.Update(parent);

                return Json(new {success = true, message = "You have successfully edited a Parent."});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Parent.");
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Parent parent = _unitOfWork.ParentRepository.GetOne(r => r.ParentId == id,
                    "Person, Person.Gender");
                return PartialView(ParentVm(parent));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Parent.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Parent parent)
        {
            try
            {
                _unitOfWork.ParentRepository.Delete(parent);

                return Json(new {success = true, message = "You have successfully deleted a Parent."});
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new {success = false, error = GetErrorsFromModelState()});
            }
        }

        public ParentVM ParentVm(Parent parent)
        {
            var parentVm = new ParentVM
            {
                Parent = parent,
                PersonVM = new PersonVM
                {
                    Person = parent.Person,
                    PersonList =
                        new SelectList(_unitOfWork.PersonRepository.GetList().OrderBy(p => p.FullName), "PersonId",
                            "FullName")
                },
            };

            return parentVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
