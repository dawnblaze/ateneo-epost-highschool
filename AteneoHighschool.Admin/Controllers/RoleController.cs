﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AteneoHighschool.Data;
using AteneoHighschool.DataAccess;

namespace AteneoHighschool.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RoleController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly GenericRoleProvider _roleProvider = new GenericRoleProvider();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListJson(DataTableParamVM param)
        {
            try
            {
                Thread.Sleep(200);

                string sortProperty;
                string searchText = (!string.IsNullOrEmpty(param.sSearch)) ? param.sSearch : "";

                switch (param.iSortCol_0)
                {
                    case 1: sortProperty = "RoleName"; break;
                    default: sortProperty = "RoleName"; break;
                }

                var termType = _unitOfWork.RoleRepository.GetList(q => q.RoleName.Contains(searchText),
                    iDisplayStart: param.iDisplayStart, iDisplayLength: param.iDisplayLength, sortProperty: sortProperty,
                    sortOrder: param.sSortDir_0);

                var recordCount = _unitOfWork.RoleRepository.Get().Count();

                return Json(new
                {
                    param.sEcho,
                    iTotalRecords = recordCount,
                    iTotalDisplayRecords = recordCount,
                    aaData = termType
                },
                  JsonRequestBehavior.AllowGet);

            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        [HttpPost]
        public JsonResult GetListJson(string searchText, int maxResult)
        {
            var roleList =
                _unitOfWork.RoleRepository.GetList(q => q.RoleName.Contains(searchText), iDisplayLength: maxResult)
                    .Select(q => new {id = q.RoleId, label = q.RoleName});
            return Json(roleList);
        }

        public ActionResult Create()
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                var role = new Role
                {
                    RoleName = null
                };
                return PartialView(RoleVm(role));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to create a new Role.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJson(Role role)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.RoleRepository.Insert(role);
                Guid roleGuid = role.RoleId;
                role = _unitOfWork.RoleRepository.GetOne(q => q.RoleId == roleGuid);

                return Json(new { success = true, message = "You have successfully created a new Role: " + role.RoleName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", "Creation of new Role failed. Please check if there is already an existing Role named: " + role.RoleName + "." + ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Edit(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Role role = _unitOfWork.RoleRepository.GetOne(q => q.RoleId == id);
                return PartialView(RoleVm(role));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to edit a Role.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJson(Role role)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { success = false, error = GetErrorsFromModelState() });
                }

                _unitOfWork.RoleRepository.Update(role);

                return Json(new { success = true, message = "You have successfully edited a Role : " + role.RoleName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message + "Please check if there is already a Role named: " + role.RoleName + ".");
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public ActionResult Delete(Guid id)
        {
            if (_roleProvider.IsUserInUserRole(User.Identity.Name, "Administrator"))
            {
                Role role = _unitOfWork.RoleRepository.GetOne(r => r.RoleId == id);
                return PartialView(RoleVm(role));
            }
            ViewBag.ProhibitedMessage = "Sorry! You have no right to delete a Role.";
            return PartialView("_Prohibited");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJson(Role role)
        {
            try
            {
                _unitOfWork.RoleRepository.Delete(role);

                return Json(new { success = true, message = "You have successfully deleted a Role : " + role.RoleName });
            }
            catch (ArgumentException ae)
            {
                ModelState.AddModelError("", ae.Message);
                return Json(new { success = false, error = GetErrorsFromModelState() });
            }
        }

        public RoleVM RoleVm(Role role)
        {
            var roleVm = new RoleVM
            {
                Role = role
            };
            return roleVm;
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
