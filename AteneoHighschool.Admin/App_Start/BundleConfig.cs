﻿using System.Web.Optimization;

namespace AteneoHighschool.Admin
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.8.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-1.9.1.custom.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.unobtrusive.js",
                        "~/Scripts/jquery.validate.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
            
            //DataTable
            bundles.Add(new ScriptBundle("~/datatable/js").Include(
                        "~/Scripts/DataTable/js/jquery.dataTables.js"));

            bundles.Add(new StyleBundle("~/datatable/css").Include(
                        "~/Scripts/DataTable/css/demo_table_jui.css",
                        "~/Scripts/DataTable/css/data-tables-supplement.css" ));
            //End of DataTable

            //Common
            bundles.Add(new ScriptBundle("~/Common/js").Include(
                    "~/Content/Js/Common/Dialog.js",
                    "~/Content/Js/Common/Autocomplete.js"
                  ));
            //End of Common

            //Gender
            bundles.Add(new ScriptBundle("~/Gender/Index/js").Include(
                    "~/Content/Js/Gender/Index.js"
                    ));
            //End of Gender
        }
    }
}