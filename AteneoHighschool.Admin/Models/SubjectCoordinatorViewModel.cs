﻿namespace AteneoHighschool.Admin.Models
{
    public class SubjectCoordinatorViewModel
    {
        public string CoordinatorName { get; set; }
        public string Subject { get; set; }
        public int AverageMeetings { get; set; }
        public int AverageMeetingsDone { get; set; }
        public int AverageMeetingsRemain { get; set; }
        public int AverageTopicsRemain { get; set; }
        public int AverageBufferMeetings { get; set; }
        //public decimal AverageCompression { get; set; }
        public int AverageMeanScore { get; set; }
    }
}