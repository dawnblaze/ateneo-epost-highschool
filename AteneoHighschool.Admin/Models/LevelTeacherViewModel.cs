﻿namespace AteneoHighschool.Admin.Models
{
    public class LevelTeacherViewModel
    {
        public string LeaderName { get; set; }
        public string ClassName { get; set; }
        public string Level { get; set; }
        public int Students { get; set; }
        public int Meetings { get; set; }
        public int MeetingsDone { get; set; }
        public int MeetingsRemain
        {
            get
            {
                return Meetings - MeetingsDone;
            }
        }
        public int TopicsRemain
        {
            get
            {
                return Meetings - MeetingsDone;
            }
        }
        public int BufferMeetings { get; set; }
        public int MeanScore { get; set; }
    }
}