﻿namespace AteneoHighschool.Admin.Models
{
    public class LevelLeaderViewModel
    {
        public string LevelLeaderName { get; set; }
        public string Level { get; set; }
        public int AverageMeetings { get; set; }
        public int AverageMeetingsDone { get; set; }
        public int AverageMeetingsRemain { get; set; }
        public int AverageTopicsRemain { get; set; }
        public int AverageBufferMeetings { get; set; }
        public int AverageMeanScore { get; set; }
    }
}