﻿$(function () {
    var DeletePrefixForm = $("#DeletePrefixForm");
    $(DeletePrefixForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeletePrefixDialog"].dialog('close');
            PrefixDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


