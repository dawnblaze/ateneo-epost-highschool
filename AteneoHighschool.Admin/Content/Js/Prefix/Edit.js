﻿$(function () {
    var EditPrefixForm = $("#EditPrefixForm");
    $(EditPrefixForm).submit(function (e) {
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditPrefixForm, json.message);
            d["EditPrefixDialog"].dialog('close');
            PrefixDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});
