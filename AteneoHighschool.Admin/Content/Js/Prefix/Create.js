﻿$(function () {
    var PrefixForm = $("#CreatePrefixForm");
    $(CreatePrefixForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreatePrefixForm);
            displaySuccessMessage(CreatePrefixForm, json.message);
            PrefixDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});