﻿$(function () {
    var CreateAppointmentScheduleForm = $("#CreateAppointmentScheduleForm");
    $(CreateAppointmentScheduleForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateAppointmentScheduleForm);
            displaySuccessMessage(CreateAppointmentScheduleForm, json.message);
            AppointmentScheduleDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});