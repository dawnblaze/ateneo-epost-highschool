﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    AppointmentScheduleDT = $('#AppointmentScheduleDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/AppointmentSchedule/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "AppointmentScheduleId", "bSortable": false, "bVisible": false },
            { "mData": "Employee.Person.FullName", "bSearchable": true, "bVisible": true, "sWidth": "40%" },
            { "mData": "Schedule.ScheduleName", "bSearchable": true, "bVisible": true, "sWidth": "40%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Appointment Schedule" title="Click this Icon to Edit Appointment Schedule" data-url="AppointmentSchedule/Edit/' + oObj.aData["AppointmentScheduleId"] + '" data-id="EditAppointmentScheduleDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Appointment Schedule" title="Click this Icon to Delete Appointment Schedule" data-url="AppointmentSchedule/Delete/' + oObj.aData["AppointmentScheduleId"] + '" data-id="DeleteAppointmentScheduleDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#AppointmentScheduleDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#AppointmentScheduleDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});