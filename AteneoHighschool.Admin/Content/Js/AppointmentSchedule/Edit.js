﻿$(function () {
    var EditAppointmentScheduleForm = $("#EditAppointmentScheduleForm");
    $(EditAppointmentScheduleForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditAppointmentScheduleForm, json.message);
            d["EditAppointmentScheduleDialog"].dialog('close');
            AppointmentScheduleDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


