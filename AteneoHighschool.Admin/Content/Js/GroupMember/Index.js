﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    GroupMemberDT = $('#GroupMemberDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/GroupMember/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "GroupMemberId", "bSortable": false, "bVisible": false },
            { "mData": "Group.GroupName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            { "mData": "Student.Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Student.Person.LastName", "bSearchable": true, "bVisible": false },
            { "mData": "Student.Person.FirstName", "bSearchable": true, "bVisible": false },
            { "mData": "Student.Person.MiddleName", "bSearchable": true, "bVisible": false },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Group Member" title="Click this Icon to Edit Group Member" data-url="GroupMember/Edit/' + oObj.aData["GroupMemberId"] + '" data-id="EditGroupMemberDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Group Member" title="Click this Icon to Delete Group Member" data-url="GroupMember/Delete/' + oObj.aData["GroupMemberId"] + '" data-id="DeleteGroupMemberDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#GroupMemberDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#GroupMemberDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});