﻿$(function () {
    var EditGroupMemberForm = $("#EditGroupMemberForm");
    $(EditGroupMemberForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditGroupMemberForm, json.message);
            d["EditGroupMemberDialog"].dialog('close');
            GroupMemberDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


