﻿$(function () {
    var DeleteGroupMemberForm = $("#DeleteGroupMemberForm");
    $(DeleteGroupMemberForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteGroupMemberDialog"].dialog('close');
            GroupMemberDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


