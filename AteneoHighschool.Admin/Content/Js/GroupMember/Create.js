﻿$(function () {
    var CreateGroupMemberForm = $("#CreateGroupMemberForm");
    $(CreateGroupMemberForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateGroupMemberForm);
            displaySuccessMessage(CreateGroupMemberForm, json.message);
            GroupMemberDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});