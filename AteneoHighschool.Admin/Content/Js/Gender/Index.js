﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    GenderDT = $('#GenderDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Gender/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "GenderId", "bSortable": false, "bVisible": false },
            { "mData": "GenderName", "bSearchable": true, "sWidth": "10%" },
            {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Edit Gender" title="Click this Icon to Edit Gender " data-url="Gender/Edit/' + oObj.aData["GenderId"] + '" data-id="EditGenderDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
            }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Gender" title="Click this Icon to Delete Gender" data-url="Gender/Delete/' + oObj.aData["GenderId"] + '" data-id="DeleteGenderDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#GenderDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#GenderDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});