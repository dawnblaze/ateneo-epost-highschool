﻿$(function () {
    var EditGenderForm = $("#EditGenderForm");
    //    formValidator(EditGenderForm);
    $(EditGenderForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditGenderForm, json.message);
            d["EditGenderDialog"].dialog('close');
            GenderDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


