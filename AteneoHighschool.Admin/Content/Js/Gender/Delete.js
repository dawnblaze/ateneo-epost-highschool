﻿$(function () {
    var DeleteGenderForm = $("#DeleteGenderForm");
    $(DeleteGenderForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteGenderDialog"].dialog('close');
            GenderDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


