﻿$(function () {
    var CreateGenderForm = $("#CreateGenderForm");
    // formValidator(CreateGenderForm);
    $(CreateGenderForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateGenderForm);
            displaySuccessMessage(CreateGenderForm, json.message);
            GenderDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});