﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    ParentDT = $('#ParentDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Parent/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "ParentId", "bSortable": false, "bVisible": false },
            { "mData": "Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.Gender.GenderName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.Age", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.LastName", "bSearchable": true, "bVisible": false },
            { "mData": "Person.FirstName", "bSearchable": true, "bVisible": false },
            { "mData": "Person.MiddleName", "bSearchable": true, "bVisible": false },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<a href="Person/Detail/' + oObj.aData["PersonId"] + '" title="Click this Icon to View Person Details" class="link ui-icon ui-icon-person">' + oObj.aData["FullName"] + '</a>';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Parent" title="Click this Icon to Edit Parent " data-url="Parent/Edit/' + oObj.aData["ParentId"] + '" data-id="EditParentDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Parent" title="Click this Icon to Delete Parent" data-url="Parent/Delete/' + oObj.aData["ParentId"] + '" data-id="DeleteParentDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#ParentDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#ParentDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});