﻿$(function () {
    var DeleteParentForm = $("#DeleteParentForm");
    $(DeleteParentForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteParentDialog"].dialog('close');
            ParentDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


