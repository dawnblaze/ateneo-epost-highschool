﻿$(function () {
    var CreateParentForm = $("#CreateParentForm");
    $(CreateParentForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateParentForm);
            displaySuccessMessage(CreateParentForm, json.message);
            ParentDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});