﻿$(function () {
    var EditParentForm = $("#EditParentForm");
    $(EditParentForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditParentForm, json.message);
            d["EditParentDialog"].dialog('close');
            ParentDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


