﻿$(function () {
    var EditCurriculumItemForm = $("#EditCurriculumItemForm");
    $(EditCurriculumItemForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditCurriculumItemForm, json.message);
            d["EditCurriculumItemDialog"].dialog('close');
            CurriculumItemDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


