﻿$(function () {
    var DeleteCurriculumItemForm = $("#DeleteCurriculumItemForm");
    $(DeleteCurriculumItemForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteCurriculumItemDialog"].dialog('close');
            CurriculumItemDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


