﻿$(function () {
    var CreateCurriculumItemForm = $("#CreateCurriculumItemForm");
    $(CreateCurriculumItemForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateCurriculumItemForm);
            displaySuccessMessage(CreateCurriculumItemForm, json.message);
            CurriculumItemDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});