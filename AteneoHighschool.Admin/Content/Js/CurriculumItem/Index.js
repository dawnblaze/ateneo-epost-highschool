﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    CurriculumItemDT = $('#CurriculumItemDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/CurriculumItem/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "CurriculumItemId", "bSortable": false, "bVisible": false },
            { "mData": "CurriculumItemName", "bSearchable": true, "bVisible": true, "sWidth": "10%" },
            { "mData": "Description", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            { "mData": "Units", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            { "mData": "Sequence", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            { "mData": "Curriculum.CurriculumName", "bSearchable": true, "bVisible": true, "sWidth": "10%" },
            { "mData": "ProficiencyLevel.ProficiencyLevelName", "bSearchable": true, "bVisible": true, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Curriculum Item" title="Click this Icon to Edit CurriculumItem" data-url="CurriculumItem/Edit/' + oObj.aData["CurriculumItemId"] + '" data-id="EditCurriculumItemDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Curriculum Item" title="Click this Icon to Delete CurriculumItem" data-url="CurriculumItem/Delete/' + oObj.aData["CurriculumItemId"] + '" data-id="DeleteCurriculumItemDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#CurriculumItemDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#CurriculumItemDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});