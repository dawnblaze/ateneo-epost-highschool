﻿$(function () {
    var DeleteTermTypeForm = $("#DeleteTermTypeForm");
    $(DeleteTermTypeForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteTermTypeDialog"].dialog('close');
            TermTypeDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


