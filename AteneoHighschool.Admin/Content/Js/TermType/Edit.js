﻿$(function () {
    var EditTermTypeForm = $("#EditTermTypeForm");
    $(EditTermTypeForm).submit(function (e) {
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditTermTypeForm, json.message);
            d["EditTermTypeDialog"].dialog('close');
            TermTypeDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});
