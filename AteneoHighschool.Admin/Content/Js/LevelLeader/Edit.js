﻿$(function () {
    var EditLevelLeaderForm = $("#EditLevelLeaderForm");
    $(EditLevelLeaderForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditLevelLeaderForm, json.message);
            d["EditLevelLeaderDialog"].dialog('close');
            LevelLeaderDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


