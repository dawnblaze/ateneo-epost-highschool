﻿$(function () {
    var CreateLevelLeaderForm = $("#CreateLevelLeaderForm");
    $(CreateLevelLeaderForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateLevelLeaderForm);
            displaySuccessMessage(CreateLevelLeaderForm, json.message);
            LevelLeaderDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});