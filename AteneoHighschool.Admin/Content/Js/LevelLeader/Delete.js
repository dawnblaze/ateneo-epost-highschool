﻿$(function () {
    var DeleteLevelLeaderForm = $("#DeleteLevelLeaderForm");
    $(DeleteLevelLeaderForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteLevelLeaderDialog"].dialog('close');
            LevelLeaderDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


