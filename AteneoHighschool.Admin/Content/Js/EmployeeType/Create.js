﻿$(function () {
    var CreateEmployeeTypeForm = $("#CreateEmployeeTypeForm");
    $(CreateEmployeeTypeForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateEmployeeTypeForm);
            displaySuccessMessage(CreateEmployeeTypeForm, json.message);
            EmployeeTypeDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});