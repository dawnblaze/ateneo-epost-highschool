﻿$(function () {
    var DeleteEmployeeTypeForm = $("#DeleteEmployeeTypeForm");
    $(DeleteEmployeeTypeForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteEmployeeTypeDialog"].dialog('close');
            EmployeeTypeDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


