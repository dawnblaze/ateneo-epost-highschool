﻿$(function () {
    var EditEmployeeTypeForm = $("#EditEmployeeTypeForm");
    $(EditEmployeeTypeForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditEmployeeTypeForm, json.message);
            d["EditEmployeeTypeDialog"].dialog('close');
            EmployeeTypeDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


