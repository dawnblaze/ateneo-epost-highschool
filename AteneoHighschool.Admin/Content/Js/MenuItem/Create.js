﻿$(function () {
    var CreateMenuItemForm = $("#CreateMenuItemForm");
    $(CreateMenuItemForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateMenuItemForm);
            displaySuccessMessage(CreateMenuItemForm, json.message);
            MenuItemDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});