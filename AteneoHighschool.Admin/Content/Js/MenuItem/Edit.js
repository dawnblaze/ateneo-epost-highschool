﻿$(function () {
    var EditMenuItemForm = $("#EditMenuItemForm");
    $(EditMenuItemForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditMenuItemForm, json.message);
            d["EditMenuItemDialog"].dialog('close');
            MenuItemDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


