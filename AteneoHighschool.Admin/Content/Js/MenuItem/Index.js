﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    MenuItemDT = $('#MenuItemDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/MenuItem/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "MenuItemId", "bSortable": false, "bVisible": false },
            { "mData": "Name", "bSearchable": true, "bVisible": true, "sWidth": "20%" },
            { "mData": "ActionName", "bSearchable": true, "bVisible": true, "sWidth": "20%" },
            { "mData": "ControllerName", "bSearchable": true, "bVisible": true, "sWidth": "20%" },
            { "mData": "Sequence", "bSearchable": true, "bVisible": true, "sWidth": "20%" },
            { "mData": "MainMenu.Name", "bSearchable": true, "bVisible": true, "sWidth": "20%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Menu Item" title="Click this Icon to Edit Menu Item" data-url="MenuItem/Edit/' + oObj.aData["MenuItemId"] + '" data-id="EditMenuItemDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Menu Item" title="Click this Icon to Delete Menu Item" data-url="MenuItem/Delete/' + oObj.aData["MenuItemId"] + '" data-id="DeleteMenuItemDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#MenuItemDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#MenuItemDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});