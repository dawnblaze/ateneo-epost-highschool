﻿$(function () {
    var DeleteMenuItemForm = $("#DeleteMenuItemForm");
    $(DeleteMenuItemForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteMenuItemDialog"].dialog('close');
            MenuItemDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


