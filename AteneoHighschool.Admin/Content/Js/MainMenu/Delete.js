﻿$(function () {
    var DeleteMainMenuForm = $("#DeleteMainMenuForm");
    $(DeleteMainMenuForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteMainMenuDialog"].dialog('close');
            MainMenuDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


