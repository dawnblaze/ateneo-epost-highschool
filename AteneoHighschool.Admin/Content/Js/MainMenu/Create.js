﻿$(function () {
    var CreateMainMenuForm = $("#CreateMainMenuForm");
    $(CreateMainMenuForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateMainMenuForm);
            displaySuccessMessage(CreateMainMenuForm, json.message);
            MainMenuDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});