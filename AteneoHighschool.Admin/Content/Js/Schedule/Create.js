﻿$(function () {
    var CreateScheduleForm = $("#CreateScheduleForm");
    $(CreateScheduleForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateScheduleForm);
            displaySuccessMessage(CreateScheduleForm, json.message);
            ScheduleDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});