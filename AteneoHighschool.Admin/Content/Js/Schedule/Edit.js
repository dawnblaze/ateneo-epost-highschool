﻿$(function () {
    var EditScheduleForm = $("#EditScheduleForm");
    $(EditScheduleForm).submit(function (e) {
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditScheduleForm, json.message);
            d["EditScheduleDialog"].dialog('close');
            ScheduleDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});
