﻿$(function () {
    var DeleteCategoryForm = $("#DeleteCategoryForm");
    $(DeleteCategoryForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteCategoryDialog"].dialog('close');
            CategoryDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


