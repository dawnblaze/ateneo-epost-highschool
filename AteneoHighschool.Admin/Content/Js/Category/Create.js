﻿$(function () {
    var CreateCategoryForm = $("#CreateCategoryForm");
    // formValidator(CreateGenderForm);
    $(CreateCategoryForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateCategoryForm);
            displaySuccessMessage(CreateCategoryForm, json.message);
            CategoryDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});