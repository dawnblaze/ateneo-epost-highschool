﻿$(function () {
    var EditCategoryForm = $("#EditCategoryForm");
    //    formValidator(EditGenderForm);
    $(EditCategoryForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditCategoryForm, json.message);
            d["EditCategoryDialog"].dialog('close');
            CategoryDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


