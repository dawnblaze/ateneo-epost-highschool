﻿$(function () {
    var CreateStudentForm = $("#CreateStudentForm");
    $(CreateStudentForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateStudentForm);
            displaySuccessMessage(CreateStudentForm, json.message);
            StudentDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});