﻿$(function () {
    var EditStudentForm = $("#EditStudentForm");
    $(EditStudentForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditStudentForm, json.message);
            d["EditStudentDialog"].dialog('close');
            StudentDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


