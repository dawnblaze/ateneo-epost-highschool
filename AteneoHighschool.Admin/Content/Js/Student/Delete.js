﻿$(function () {
    var DeleteStudentForm = $("#DeleteStudentForm");
    $(DeleteStudentForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteStudentDialog"].dialog('close');
            StudentDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


