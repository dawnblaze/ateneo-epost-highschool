﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    StudentDT = $('#StudentDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Student/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "StudentId", "bSortable": false, "bVisible": false },
            { "mData": "StudentNumber", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.Gender.GenderName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.Age", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.LastName", "bSearchable": true, "bVisible": false },
            { "mData": "Person.FirstName", "bSearchable": true, "bVisible": false },
            { "mData": "Person.MiddleName", "bSearchable": true, "bVisible": false },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<a href="Person/Detail/' + oObj.aData["PersonId"] + '" title="Click this Icon to View Person Details" class="link ui-icon ui-icon-person">' + oObj.aData["FullName"] + '</a>';
                }
            },
            {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Edit Student" title="Click this Icon to Edit Student " data-url="Student/Edit/' + oObj.aData["StudentId"] + '" data-id="EditStudentDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
            }
        },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Student" title="Click this Icon to Delete Student" data-url="Student/Delete/' + oObj.aData["StudentId"] + '" data-id="DeleteStudentDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#StudentDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#StudentDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});