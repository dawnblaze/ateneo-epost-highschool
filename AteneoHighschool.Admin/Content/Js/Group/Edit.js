﻿$(function () {
    var EditGroupForm = $("#EditGroupForm");
    $(EditGroupForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditGroupForm, json.message);
            d["EditGroupDialog"].dialog('close');
            GroupDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


