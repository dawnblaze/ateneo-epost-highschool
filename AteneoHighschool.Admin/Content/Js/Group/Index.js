﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    GroupDT = $('#GroupDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Group/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "GroupId", "bSortable": false, "bVisible": false },
            { "mData": "GroupName", "bSearchable": true, "bVisible": true, "sWidth": "35%" },
            { "mData": "Description", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            { "mData": "Employee.Person.FullName", "bSearchable": false, "bVisible": true, "sWidth": "25%" },
            { "mData": "ProficiencyLevel.ProficiencyLevelName", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Group" title="Click this Icon to Edit Group" data-url="Group/Edit/' + oObj.aData["GroupId"] + '" data-id="EditGroupDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Group" title="Click this Icon to Delete Group" data-url="Group/Delete/' + oObj.aData["GroupId"] + '" data-id="DeleteGroupDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#GroupDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#GroupDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});