﻿$(function () {
    var CreateGroupForm = $("#CreateGroupForm");
    $(CreateGroupForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateGroupForm);
            displaySuccessMessage(CreateGroupForm, json.message);
            GroupDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});