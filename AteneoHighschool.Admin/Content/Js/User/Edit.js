﻿$(function () {
    var EditUserForm = $("#EditUserForm");
    $(EditUserForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditUserForm, json.message);
            d["EditUserDialog"].dialog('close');
            UserDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


