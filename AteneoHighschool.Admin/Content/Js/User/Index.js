﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    UserDT = $('#UserDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/User/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
        { "sType": "numeric", "mData": "UserId", "bSortable": false, "bVisible": false },
        { "mData": "UserName", "bSearchable": true, "sWidth": "10%" },
        { "mData": "Email", "bSearchable": true, "sWidth": "10%" },
        { "mData": "AccessFailedCount", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
        { "mData": "IsLockedOut", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
        { "mData": "IsActive", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
        {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<a href="User/Detail/' + oObj.aData["UserId"] + '" title="Click this Icon to View User Details" class="link ui-icon ui-icon-person">' + oObj.aData["UserName"] + '</a>';
            }
        },
        {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Edit User" title="Click this Icon to Edit User" data-url="User/Edit/' + oObj.aData["UserId"] + '" data-id="EditUserDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
            }
        },
        ],
        "fnDrawCallback": function () {
            $('table#UserDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#UserDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});