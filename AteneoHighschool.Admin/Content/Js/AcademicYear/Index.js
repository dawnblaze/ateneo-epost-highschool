﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    AcademicYearDT = $('#AcademicYearDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/AcademicYear/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "AcademicYearId", "bSortable": false, "bVisible": false },
            { "mData": "AcademicYearName", "bSearchable": true, "bVisible": true, "sWidth": "50%" },
            { "mData": "Description", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            {
                "mData": null,
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['StartDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            {
                "mData": null,
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['EndDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<a href="AcademicYear/Detail/' + oObj.aData["AcademicYearId"] + '" title="Click this Icon to View Academic Year Details" class="link ui-icon ui-icon-info">' + oObj.aData["AcademicYearName"] + '</a>';
                }
            },
            {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Edit Academic Year" title="Click this Icon to Edit Academic Year" data-url="AcademicYear/Edit/' + oObj.aData["AcademicYearId"] + '" data-id="EditAcademicYearDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
            }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Academic Year" title="Click this Icon to Delete Academice Year" data-url="AcademicYear/Delete/' + oObj.aData["AcademicYearId"] + '" data-id="DeleteAcademicYearDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#AcademicYearDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#AcademicYearDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});