﻿$(function () {
    var CreateAcademicYearForm = $("#CreateAcademicYearForm");
    $(CreateAcademicYearForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateAcademicYearForm);
            displaySuccessMessage(CreateAcademicYearForm, json.message);
            AcademicYearDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});