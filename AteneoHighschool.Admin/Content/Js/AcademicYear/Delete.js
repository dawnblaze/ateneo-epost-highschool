﻿$(function () {
    var DeleteAcademicYearForm = $("#DeleteAcademicYearForm");
    $(DeleteAcademicYearForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteAcademicYearDialog"].dialog('close');
            AcademicYearDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


