﻿$(function () {
    var EditAcademicYearForm = $("#EditAcademicYearForm");
    $(EditAcademicYearForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditAcademicYearForm, json.message);
            d["EditAcademicYearDialog"].dialog('close');
            AcademicYearDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


