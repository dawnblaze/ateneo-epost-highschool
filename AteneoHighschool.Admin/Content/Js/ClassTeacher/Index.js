﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    ClassTeacherDT = $('#ClassTeacherDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/ClassTeacher/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "ClassTeacherId", "bSortable": false, "bVisible": false },
            { "mData": "CurriculumClass.CurriculumClassName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            { "mData": "Employee.Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Employee.Person.LastName", "bSearchable": true, "bVisible": false },
            { "mData": "Employee.Person.FirstName", "bSearchable": true, "bVisible": false },
            { "mData": "Employee.Person.MiddleName", "bSearchable": true, "bVisible": false },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Class Teacher" title="Click this Icon to Edit Class Teacher" data-url="ClassTeacher/Edit/' + oObj.aData["ClassTeacherId"] + '" data-id="EditClassTeacherDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Class Teacher" title="Click this Icon to Delete Class Teacher" data-url="ClassTeacher/Delete/' + oObj.aData["ClassTeacherId"] + '" data-id="DeleteClassTeacherDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#ClassTeacherDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#ClassTeacherDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});