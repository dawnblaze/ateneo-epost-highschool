﻿$(function () {
    var DeleteClassTeacherForm = $("#DeleteClassTeacherForm");
    $(DeleteClassTeacherForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteClassTeacherDialog"].dialog('close');
            ClassTeacherDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


