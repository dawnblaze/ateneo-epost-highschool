﻿$(function () {
    var EditClassTeacherForm = $("#EditClassTeacherForm");
    $(EditClassTeacherForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditClassTeacherForm, json.message);
            d["EditClassTeacherDialog"].dialog('close');
            ClassTeacherDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


