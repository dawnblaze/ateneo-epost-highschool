﻿$(function () {
    var CreateClassTeacherForm = $("#CreateClassTeacherForm");
    $(CreateClassTeacherForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateClassTeacherForm);
            displaySuccessMessage(CreateClassTeacherForm, json.message);
            ClassTeacherDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});