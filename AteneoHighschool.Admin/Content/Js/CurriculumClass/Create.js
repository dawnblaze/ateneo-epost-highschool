﻿$(function () {
    var CreateCurriculumClassForm = $("#CreateCurriculumClassForm");
    $(CreateCurriculumClassForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateCurriculumClassForm);
            displaySuccessMessage(CreateCurriculumClassForm, json.message);
            CurriculumClassDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});