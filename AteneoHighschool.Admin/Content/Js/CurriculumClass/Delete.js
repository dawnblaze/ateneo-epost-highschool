﻿$(function () {
    var DeleteCurriculumClassForm = $("#DeleteCurriculumClassForm");
    $(DeleteCurriculumClassForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteCurriculumClassDialog"].dialog('close');
            CurriculumClassDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


