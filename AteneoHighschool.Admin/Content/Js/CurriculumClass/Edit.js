﻿$(function () {
    var EditCurriculumClassForm = $("#EditCurriculumClassForm");
    $(EditCurriculumClassForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditCurriculumClassForm, json.message);
            d["EditCurriculumClassDialog"].dialog('close');
            CurriculumClassDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


