﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    CurriculumClassDT = $('#CurriculumClassDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/CurriculumClass/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "CurriculumClassId", "bSortable": false, "bVisible": false },
            { "mData": "CurriculumClassName", "bSearchable": true, "bVisible": true, "sWidth": "50%" },
            { "mData": "Curriculum.CurriculumName", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            { "mData": "AcademicYear.AcademicYearName", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
//                    return '<span data-dialog-title="View Recurrence" title="Click this Icon to View Recurrence " data-url="Recurrence/Index/' + oObj.aData["CurriculumClassId"] + '" data-id="ViewRecurrenceDialog" data-process-button="Update" onclick="loadAndShowDialogContent($(this))" class="link ui-icon ui-icon-document" />';
//                    return '<a href="Recurrence/Index/' + oObj.aData["CurriculumClassId"] + '" title="Click this Icon to View Recurrences" class="link ui-icon ui-icon-document">' + oObj.aData["CurriculumClassName"] + '</a>';
                    return '<span data-dialog-title="View Recurrences" title="Click this Icon to View Recurrences" data-url="Recurrence/Index/' + oObj.aData["CurriculumClassId"] + '" data-id="EditRecurrenceDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-document">';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Curriculum Class" title="Click this Icon to Edit Curriculum Class" data-url="CurriculumClass/Edit/' + oObj.aData["CurriculumClassId"] + '" data-id="EditCurriculumClassDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Curriculum Class" title="Click this Icon to Delete Curriculum Class" data-url="CurriculumClass/Delete/' + oObj.aData["CurriculumClassId"] + '" data-id="DeleteCurriculumClassDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#CurriculumClassDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#CurriculumClassDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});