﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    AnnouncementDT = $('#AnnouncementDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Announcement/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "AnnouncementId", "bSortable": false, "bVisible": false },
            { "mData": "AnnouncementName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            { "mData": "Description", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            { "mData": "AcademicYear.AcademicYearName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "sWidth": "10%",
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['StartDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "sWidth": "10%",
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['EndDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Announcement" title="Click this Icon to Edit Announcement" data-url="Announcement/Edit/' + oObj.aData["AnnouncementId"] + '" data-id="EditAnnouncementDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Announcement" title="Click this Icon to Delete Announcement" data-url="Announcement/Delete/' + oObj.aData["AnnouncementId"] + '" data-id="DeleteAnnouncementDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#AnnouncementDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#AnnouncementDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});