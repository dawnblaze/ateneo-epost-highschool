﻿$(function () {
    var DeleteAnnouncementForm = $("#DeleteAnnouncementForm");
    $(DeleteAnnouncementForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteAnnouncementDialog"].dialog('close');
            AnnouncementDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


