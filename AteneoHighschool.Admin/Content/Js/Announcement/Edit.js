﻿$(function () {
    var EditAnnouncementForm = $("#EditAnnouncementForm");
    $(EditAnnouncementForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditAnnouncementForm, json.message);
            d["EditAnnouncementDialog"].dialog('close');
            AnnouncementDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


