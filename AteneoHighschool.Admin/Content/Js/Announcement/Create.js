﻿$(function () {
    var CreateAnnouncementForm = $("#CreateAnnouncementForm");
    $(CreateAnnouncementForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateAnnouncementForm);
            displaySuccessMessage(CreateAnnouncementForm, json.message);
            AnnouncementDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});