﻿$(function () {
    var EditTermForm = $("#EditTermForm");
    $(EditTermForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditTermForm, json.message);
            d["EditTermDialog"].dialog('close');
            TermDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


