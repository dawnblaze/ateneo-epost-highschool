﻿$(function () {
    var CreateTermForm = $("#CreateTermForm");
    $(CreateTermForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateTermForm);
            displaySuccessMessage(CreateTermForm, json.message);
            TermDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});