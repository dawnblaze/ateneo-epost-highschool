﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    TermDT = $('#TermDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Term/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
        { "sType": "numeric", "mData": "TermId", "bSortable": false, "bVisible": false },
        { "mData": "TermName", "bSearchable": true, "sWidth": "10%" },
        { "mData": "Description", "bSearchable": true, "sWidth": "10%" },
        { "mData": "AcademicYear.AcademicYearName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
        { "mData": "TermType.TermTypeName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
        {
            "mData": null,
            "bSearchable": false,
            "sWidth": "10%",
            "fnRender": function(oObj) {
                var dateGraduated = new Date(parseInt(oObj.aData['StartDate'].replace("/Date(", "").replace(")/", ""), 10));
                return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
            }
        },
        {
            "mData": null,
            "bSearchable": false,
            "sWidth": "10%",
            "fnRender": function(oObj) {
                var dateGraduated = new Date(parseInt(oObj.aData['EndDate'].replace("/Date(", "").replace(")/", ""), 10));
                return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
            }
        },
        { "mData": "IsActive", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
        {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<a href="Term/Detail/' + oObj.aData["TermId"] + '" title="Click this Icon to View Term Details" class="link ui-icon ui-icon-info">' + oObj.aData["TermName"] + '</a>';
            }
        },
        {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Edit Term" title="Click this Icon to Edit Term" data-url="Term/Edit/' + oObj.aData["TermId"] + '" data-id="EditTermDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
            }
        },
        {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Delete Term" title="Click this Icon to Delete Term" data-url="Term/Delete/' + oObj.aData["TermId"] + '" data-id="DeleteTermDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
            }
        }
        ],
        "fnDrawCallback": function () {
            $('table#TermDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#TermDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});