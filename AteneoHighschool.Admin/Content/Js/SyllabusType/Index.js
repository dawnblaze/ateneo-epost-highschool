﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    SyllabusTypeDT = $('#SyllabusTypeDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/SyllabusType/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "SyllabusTypeId", "bSortable": false, "bVisible": false },
            { "mData": "SyllabusTypeName", "bSearchable": true, "bVisible": true, "sWidth": "50%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Syllabus Type" title="Click this Icon to Edit Syllabus Type" data-url="SyllabusType/Edit/' + oObj.aData["SyllabusTypeId"] + '" data-id="EditSyllabusTypeDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Syllabus Type" title="Click this Icon to Delete Syllabus Type" data-url="SyllabusType/Delete/' + oObj.aData["SyllabusTypeId"] + '" data-id="DeleteSyllabusTypeDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#SyllabusTypeDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#SyllabusTypeDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});