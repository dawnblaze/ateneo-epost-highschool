﻿$(function () {
    var DeleteSyllabusTypeForm = $("#DeleteSyllabusTypeForm");
    $(DeleteSyllabusTypeForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteSyllabusTypeDialog"].dialog('close');
            SyllabusTypeDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


