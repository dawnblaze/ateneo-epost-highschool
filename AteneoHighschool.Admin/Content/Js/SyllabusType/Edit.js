﻿$(function () {
    var EditSyllabusTypeForm = $("#EditSyllabusTypeForm");
    $(EditSyllabusTypeForm).submit(function (e) {
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditSyllabusTypeForm, json.message);
            d["EditSyllabusTypeDialog"].dialog('close');
            SyllabusTypeDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});
