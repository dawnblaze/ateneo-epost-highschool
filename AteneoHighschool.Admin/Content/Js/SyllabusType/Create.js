﻿$(function () {
    var CreateSyllabusTypeForm = $("#CreateSyllabusTypeForm");
    $(CreateSyllabusTypeForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateSyllabusTypeForm);
            displaySuccessMessage(CreateSyllabusTypeForm, json.message);
            SyllabusTypeDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});