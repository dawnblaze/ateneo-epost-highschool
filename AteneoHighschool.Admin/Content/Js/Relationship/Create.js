﻿$(function () {
    var CreateRelationshipForm = $("#CreateRelationshipForm");
    $(CreateRelationshipForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateRelationshipForm);
            displaySuccessMessage(CreateRelationshipForm, json.message);
            RelationshipDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});