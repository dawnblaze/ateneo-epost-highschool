﻿$(function () {
    var EditRelationshipForm = $("#EditRelationshipForm");
    $(EditRelationshipForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditRelationshipForm, json.message);
            d["EditRelationshipDialog"].dialog('close');
            RelationshipDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


