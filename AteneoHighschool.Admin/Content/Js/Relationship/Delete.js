﻿$(function () {
    var DeleteRelationshipForm = $("#DeleteRelationshipForm");
    $(DeleteRelationshipForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteRelationshipDialog"].dialog('close');
            RelationshipDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


