﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    RelationshipDT = $('#RelationshipDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Relationship/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "RelationshipId", "bSortable": false, "bVisible": false },
            { "mData": "Parent.Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Student.Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Parent.Person.LastName", "bSearchable": true, "bVisible": false },
            { "mData": "Parent.Person.FirstName", "bSearchable": true, "bVisible": false },
            { "mData": "Parent.Person.MiddleName", "bSearchable": true, "bVisible": false },
            { "mData": "Student.Person.LastName", "bSearchable": true, "bVisible": false },
            { "mData": "Student.Person.FirstName", "bSearchable": true, "bVisible": false },
            { "mData": "Student.Person.MiddleName", "bSearchable": true, "bVisible": false },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Relationship" title="Click this Icon to Edit Relationship" data-url="Relationship/Edit/' + oObj.aData["RelationshipId"] + '" data-id="EditRelationshipDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Relationship" title="Click this Icon to Delete Relationship" data-url="Relationship/Delete/' + oObj.aData["RelationshipId"] + '" data-id="DeleteRelationshipDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#RelationshipDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#RelationshipDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});