﻿$(function () {
    var DeletePersonForm = $("#DeletePersonForm");
    $(DeletePersonForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeletePersonDialog"].dialog('close');
            PersonDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


