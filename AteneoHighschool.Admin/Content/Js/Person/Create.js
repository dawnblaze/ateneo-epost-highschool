﻿$(function () {
    var CreatePersonForm = $("#CreatePersonForm");
    $(CreatePersonForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreatePersonForm);
            displaySuccessMessage(CreatePersonForm, json.message);
            PersonDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});