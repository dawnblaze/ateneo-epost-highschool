﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    PersonDT = $('#PersonDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Person/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "PersonId", "bSortable": false, "bVisible": false },
            { "mData": "FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Gender.GenderName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Age", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "sWidth": "10%",
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['BirthDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            { "mData": "LastName", "bSearchable": true, "bVisible": false },
            { "mData": "FirstName", "bSearchable": true, "bVisible": false },
            { "mData": "MiddleName", "bSearchable": true, "bVisible": false },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<a href="Person/Detail/' + oObj.aData["PersonId"] + '" title="Click this Icon to View Person Details" class="link ui-icon ui-icon-person">' + oObj.aData["FullName"] + '</a>';
                }
            },
            {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Edit Person" title="Click this Icon to Edit Person " data-url="Person/Edit/' + oObj.aData["PersonId"] + '" data-id="EditPersonDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
            }
        },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Person" title="Click this Icon to Delete Person" data-url="Person/Delete/' + oObj.aData["PersonId"] + '" data-id="DeletePersonDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#PersonDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#PersonDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});