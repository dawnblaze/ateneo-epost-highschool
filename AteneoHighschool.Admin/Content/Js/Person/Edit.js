﻿$(function () {
    var EditPersonForm = $("#EditPersonForm");
    $(EditPersonForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditPersonForm, json.message);
            d["EditPersonDialog"].dialog('close');
            PersonDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


