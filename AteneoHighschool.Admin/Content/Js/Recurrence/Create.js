﻿$(function () {
    var CreateRecurrenceForm = $("#CreateRecurrenceForm");
    $(CreateRecurrenceForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateRecurrenceForm);
            displaySuccessMessage(CreateRecurrenceForm, json.message);
            RecurrenceDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});