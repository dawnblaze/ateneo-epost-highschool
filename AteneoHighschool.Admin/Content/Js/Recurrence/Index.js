﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    RecurrenceDT = $('#RecurrenceDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Recurrence/GetListJson",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "CurriculumClassId", "value": $("#CurriculumClassId").val() });
        },
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
        { "sType": "numeric", "mData": "RecurrenceId", "bSortable": false, "bVisible": false },
        { "mData": "Schedule.ScheduleName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Recurrence" title="Click this Icon to Edit Recurrence" data-url="Recurrence/Edit/' + oObj.aData["RecurrenceId"] + '" data-id="EditRecurrenceDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Recurrence" title="Click this Icon to Delete Recurrence" data-url="Recurrence/Delete/' + oObj.aData["RecurrenceId"] + '" data-id="DeleteRecurrenceDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#RecurrenceDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#RecurrenceDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});