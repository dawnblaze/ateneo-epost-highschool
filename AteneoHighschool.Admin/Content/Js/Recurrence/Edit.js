﻿$(function () {
    var EditRecurrenceForm = $("#EditRecurrenceForm");
    $(EditRecurrenceForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditRecurrenceForm, json.message);
            d["EditRecurrenceDialog"].dialog('close');
            RecurrenceDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


