﻿$(function () {
    var DeleteRecurrenceForm = $("#DeleteRecurrenceForm");
    $(DeleteRecurrenceForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteRecurrenceDialog"].dialog('close');
            RecurrenceDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


