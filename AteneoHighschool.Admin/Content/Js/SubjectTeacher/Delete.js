﻿$(function () {
    var DeleteSubjectTeacherForm = $("#DeleteSubjectTeacherForm");
    $(DeleteSubjectTeacherForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteSubjectTeacherDialog"].dialog('close');
            SubjectTeacherDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


