﻿$(function () {
    var EditSubjectTeacherForm = $("#EditSubjectTeacherForm");
    $(EditSubjectTeacherForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditSubjectTeacherForm, json.message);
            d["EditSubjectTeacherDialog"].dialog('close');
            SubjectTeacherDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


