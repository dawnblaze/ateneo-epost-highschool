﻿$(function () {
    var CreateSubjectTeacherForm = $("#CreateSubjectTeacherForm");
    $(CreateSubjectTeacherForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateSubjectTeacherForm);
            displaySuccessMessage(CreateSubjectTeacherForm, json.message);
            SubjectTeacherDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});