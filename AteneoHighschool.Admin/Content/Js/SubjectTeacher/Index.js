﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    SubjectTeacherDT = $('#SubjectTeacherDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/SubjectTeacher/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "SubjectTeacherId", "bSortable": false, "bVisible": false },
            { "mData": "Employee.Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Curriculum.CurriculumName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Subject Teacher" title="Click this Icon to Edit Subject Teacher" data-url="SubjectTeacher/Edit/' + oObj.aData["SubjectTeacherId"] + '" data-id="EditSubjectTeacherDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Subject Teacher" title="Click this Icon to Delete Subject Teacher" data-url="SubjectTeacher/Delete/' + oObj.aData["SubjectTeacherId"] + '" data-id="DeleteSubjectTeacherDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#SubjectTeacherDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#SubjectTeacherDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});