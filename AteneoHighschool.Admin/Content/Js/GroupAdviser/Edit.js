﻿$(function () {
    var EditGroupAdviserForm = $("#EditGroupAdviserForm");
    $(EditGroupAdviserForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditGroupAdviserForm, json.message);
            d["EditGroupAdviserDialog"].dialog('close');
            GroupAdviserDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


