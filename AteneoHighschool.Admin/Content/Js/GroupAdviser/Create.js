﻿$(function () {
    var CreateGroupAdviserForm = $("#CreateGroupAdviserForm");
    $(CreateGroupAdviserForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateGroupAdviserForm);
            displaySuccessMessage(CreateGroupAdviserForm, json.message);
            GroupAdviserDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});