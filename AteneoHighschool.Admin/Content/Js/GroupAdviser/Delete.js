﻿$(function () {
    var DeleteGroupAdviserForm = $("#DeleteGroupAdviserForm");
    $(DeleteGroupAdviserForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteGroupAdviserDialog"].dialog('close');
            GroupAdviserDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


