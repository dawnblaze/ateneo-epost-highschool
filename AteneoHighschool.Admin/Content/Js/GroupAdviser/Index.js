﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    GroupAdviserDT = $('#GroupAdviserDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/GroupAdviser/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "GroupAdviserId", "bSortable": false, "bVisible": false },
            { "mData": "Group.GroupName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            { "mData": "Employee.Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Employee.Person.FirstName", "bVisible": false, "bSearchable": true, "sWidth": "10%" },
            { "mData": "Employee.Person.MiddleName", "bVisible": false, "bSearchable": true, "sWidth": "10%" },
            { "mData": "Employee.Person.LastName", "bVisible": false, "bSearchable": true, "sWidth": "10%" },
            {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Edit Group Adviser" title="Click this Icon to Edit Group Adviser" data-url="GroupAdviser/Edit/' + oObj.aData["GroupAdviserId"] + '" data-id="EditGroupAdviserDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
            }
        },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Group Adviser" title="Click this Icon to Delete Group Adviser" data-url="GroupAdviser/Delete/' + oObj.aData["GroupAdviserId"] + '" data-id="DeleteGroupAdviserDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#GroupAdviserDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#GroupAdviserDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});