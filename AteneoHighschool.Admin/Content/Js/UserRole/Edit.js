﻿$(function () {
    var EditUserRoleForm = $("#EditUserRoleForm");
    $(EditUserRoleForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditUserRoleForm, json.message);
            d["EditUserRoleDialog"].dialog('close');
            UserRoleDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


