﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    UserRoleDT = $('#UserRoleDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/UserRole/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
        { "sType": "numeric", "mData": "UserRoleId", "bSortable": false, "bVisible": false },
        { "mData": "User.UserName", "bSearchable": true, "sWidth": "10%" },
        { "mData": "Role.RoleName", "bSearchable": true, "sWidth": "10%" },
        { "mData": "IsActive", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete User Role" title="Click this Icon to Delete User Role" data-url="UserRole/Delete/' + oObj.aData["UserRoleId"] + '" data-id="DeleteUserRoleDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#UserRoleDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#UserRoleDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});