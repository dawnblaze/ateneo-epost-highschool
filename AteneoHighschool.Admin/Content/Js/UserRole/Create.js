﻿$(function () {
    var CreateUserRoleForm = $("#CreateTermForm");
    $(CreateUserRoleForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateUserRoleForm);
            displaySuccessMessage(CreateUserRoleForm, json.message);
            UserRoleDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});