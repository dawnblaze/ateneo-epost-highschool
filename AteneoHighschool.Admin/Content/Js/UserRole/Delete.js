﻿$(function () {
    var DeleteUserRoleForm = $("#DeleteTermForm");
    $(DeleteUserRoleForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteUserRoleDialog"].dialog('close');
            UserRoleDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


