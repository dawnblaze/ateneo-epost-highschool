﻿$(function () {
    var CreateSchoolForm = $("#CreateSchoolForm");
    $(CreateSchoolForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateSchoolForm);
            displaySuccessMessage(CreateSchoolForm, json.message);
            SchoolDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});