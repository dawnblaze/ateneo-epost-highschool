﻿$(function () {
    var EditSchoolForm = $("#EditSchoolForm");
    $(EditSchoolForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditSchoolForm, json.message);
            d["EditSchoolDialog"].dialog('close');
            SchoolDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


