﻿$(function () {
    var DeleteSchoolForm = $("#DeleteSchoolForm");
    $(DeleteSchoolForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteSchoolDialog"].dialog('close');
            SchoolDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


