﻿$(function () {
    var EditRoleForm = $("#EditRoleForm");
    $(EditRoleForm).submit(function (e) {
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditRoleForm, json.message);
            d["EditRoleDialog"].dialog('close');
            RoleDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});
