﻿$(function () {
    var CreateRoleForm = $("#CreateRoleForm");
    $(CreateRoleForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateRoleForm);
            displaySuccessMessage(CreateRoleForm, json.message);
            RoleDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});