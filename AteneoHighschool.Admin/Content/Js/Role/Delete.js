﻿$(function () {
    var DeleteRoleForm = $("#DeleteRoleForm");
    $(DeleteRoleForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteRoleDialog"].dialog('close');
            RoleDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


