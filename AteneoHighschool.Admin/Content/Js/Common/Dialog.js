﻿//Dialog.js Contains generic implementations of Jquery UI Dialog functions which includes validations
//reagz  092012
var d = {};  //dialog object container
var hasError = false;
$(document).ready(function () {
    $(".uiButton").button();  //set a <a> or <button> tag to jquery ui button  

    d["loadingDialog"] = $('<div class="modal-popup">Please wait...<img src="/Content/Images/Common/loadera32.gif" /></div>').dialog({ // Create the jQuery UI dialog
        autoOpen: false,
        title: "Loading",
        modal: true,
        resizable: false,
        draggable: false,
        width: 200,
        height: 100,
        show: "blind",
        hide: "blind"
    });

});


function getValidationSummaryErrors($form) {
    // We verify if we created it beforehand
    var errorSummary = $form.find('.validation-summary-errors, .validation-summary-valid');
    if (!errorSummary.length) {
//        errorSummary = $('<fieldset><div class="validation-summary-errors"><span>Please correct the errors and try again.</span><ul></ul></div></fieldset>')
        //                .prependTo($form);
        errorSummary = $('<fieldset><div class="validation-summary-errors"><span></span><ul></ul></div></fieldset>')
                .prependTo($form);
    }
    return errorSummary;
}

function displayError($form, errors) {

    var errorSummary = getValidationSummaryErrors($form)
            .removeClass('validation-summary-valid')
            .addClass('validation-summary-errors');
    var items = $.map(errors, function (error) {
        return '<li>' + error + '</li>';
    }).join('');

    var ul = errorSummary
        .find('ul')
        .empty()
        .append(items);

}


function displaySuccessMessage($form, message) {
    var messageFieldSet = $('<div class="message-success"><fieldset><span>' + message + '</span><ul></ul></fieldset></div>').prependTo($form);
    $form.find('.message-success').delay(2000).slideUp('slow', function () {
        $(this).remove();
    });
}


function resetForm($form) {
    // We reset the form so we make sure unobtrusive errors get cleared out.
    try {
        $form[0].reset();
        getValidationSummaryErrors($form).removeClass('validation-summary-errors').addClass('validation-summary-valid');
    }
    catch (error) {
        hasError = true;
    };
}



function showErrorMessageDialog(errorMessage) {
    d["errorMessageDialog"] = $('<div class="modal-popup"><p class="message-error">' + errorMessage + '</p></div>').dialog({ // Create the jQuery UI dialog
        autoOpen: false,
        title: "Error Message",
        modal: true,
        resizable: false,
        draggable: false,
        width: 600,
        height: 100,
        show: "blind",
        hide: "blind"
    });

    d["errorMessageDialog"].dialog("open");
}


function formSubmitHandler($form) {
    var json = false;
    if (!$form.valid || $form.valid()) {
        d["loadingDialog"].dialog("open");
        $.ajax({
            async: false,
            type: 'POST',
            url: $form.attr('action'),
            data: $form.serializeArray(),
            dataType: 'json',
            /*contentType: 'application/json; charset=utf-8',*/
            error: function (result) {
                d["loadingDialog"].dialog("close");
                alert("Ooops! Sorry server connection failed.");

            },
            success: function (data) {
                json = data;
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                d["loadingDialog"].dialog("close");
            }
        });
    }
    return json;
}

//load info and show dialog based on the specific html element (a, span, button)
function loadAndShowDialog(elementTag) {

    var url = elementTag.data('url');
    var id = elementTag.data('id');
    var processButtonName = elementTag.data('process-button');
    rowSelected(elementTag);

    if (!d[id]) {
        d[id] = $();
        d["loadingDialog"].dialog("open");
        $.get(url)
                .done(function (content) {
                    d[id] = $('<div class="modal-popup">' + content + '</div>')
                        .hide() // Hide the dialog for now so we prevent flicker
                        .appendTo(document.body)
                        .filter('div') // Filter for the div tag only, script tags could surface
                        .dialog({ // Create the jQuery UI dialog
                            title: elementTag.data('dialog-title'),
                            modal: true,
                            show: "blind",
                            hide: "blind",
                            resizable: true,
                            draggable: true,
                            position: ['center', 20],
                            width: elementTag.data('dialog-width') || 600,
                            // beforeClose: function () { resetForm($(this).find('form')); },
                            close: function () {
                                resetForm($(this).find('form'));
                                if (hasError) {
                                    $(this).remove();
                                    d[id] = null;
                                }
                            },
                            focus: function () {
                                $(':input', this).keyup(function (event) {
                                    if (event.keyCode == 13) {
                                        $('.ui-dialog-buttonpane button:first').click();
                                    }
                                });
                            },
                            open: function () {
                                var $form = $("div").closest('form');
                                var buttons = $(this).dialog("option", "buttons"); // getter                             
                                try {
                                    if ($form.attr('id').length > 0) {
                                        $.extend(buttons, [
                                            { text: processButtonName, click: function () { $(this).find('form').submit(); } },
                                            { text: "Cancel", click: function () { $(this).dialog("close"); } }
                                        ]);
                                        $(this).dialog("option", "buttons", buttons); // setter

                                    }
                                    else {
                                        $.extend(buttons, [
                                            { text: "Close", click: function () { $(this).dialog("close"); } }
                                        ]);
                                    }
                                }
                                catch (error) { }
                                d["loadingDialog"].dialog("close");
                            }
                        })
                        .find('form') // Attach logic on forms
                        .end();
                });
    } else {
        d[id].dialog('open');
    }

}

//for datatable row selection - element tag refers to the <span> -> parent - <td> --> <tr> --> <tbody>
function rowSelected(elementTag) {
    // elementTag.parent().parent().parent().children().each(function () { $(this).removeClass("row_selected"); });
    //elementTag.parent().parent().addClass("row_selected");
    elementTag.parent().parent().addClass('row_selected').siblings().removeClass('row_selected');
}


function loadAndShowDialogReload(elementTag) {
    var url = elementTag.data('url');
    var id = elementTag.data('id');
    var processButtonName = elementTag.data('process-button');

    rowSelected(elementTag);

    d[id] = $();
    d["loadingDialog"].dialog("open");

    $.get(url)
            .done(function (content) {
                d[id] = $('<div class="modal-popup">' + content + '</div>')
                    .hide() // Hide the dialog for now so we prevent flicker
                    .appendTo(document.body)
                    .filter('div') // Filter for the div tag only, script tags could surface
                    .dialog({ // Create the jQuery UI dialog
                        title: elementTag.data('dialog-title'),
                        modal: true,
                        resizable: true,
                        show: "blind",
                        hide: "blind",
                        draggable: true,
                        position: ['center', 20],
                        width: elementTag.data('dialog-width') || 600,
                        close: function () {
                            $(this).remove();  //remove the dialog and its content  - reload
                        },
                        focus: function () {
                            $(':input', this).keyup(function (event) {
                                if (event.keyCode == 13) {
                                    $('.ui-dialog-buttonpane button:first').click();
                                }
                            });
                        },
                        open: function () {
                            var $form = $("div").closest('form');
                            var buttons = $(this).dialog("option", "buttons"); // getter                             
                            try {
                                if ($form.attr('id').length > 0) {
                                    $.extend(buttons, [
                                        { text: processButtonName, click: function () { $(this).find('form').submit(); } },
                                        { text: "Cancel", click: function () { $(this).dialog("close"); } }
                                    ]);
                                    $(this).dialog("option", "buttons", buttons); // setter

                                }
                                else {
                                    $.extend(buttons, [
                                        { text: "Close", click: function () { $(this).dialog("close"); } }
                                    ]);
                                }
                            }
                            catch (error) { }
                            d["loadingDialog"].dialog("close");
                        }

                    })
                    .find('form') // Attach logic on forms
                    .end();
            });

}


function loadAndShowDialogContent(elementTag) {
    var url = elementTag.data('url');
    var id = elementTag.data('id');
    rowSelected(elementTag);

    d[id] = $();
    d["loadingDialog"].dialog("open");

    $.get(url)
            .done(function (content) {
                d["loadingDialog"].dialog("close");
                d[id] = $('<div class="modal-popup">' + content + '</div>')
                    .hide() // Hide the dialog for now so we prevent flicker
                    .appendTo(document.body)
                    .filter('div') // Filter for the div tag only, script tags could surface
                    .dialog({ // Create the jQuery UI dialog
                        title: elementTag.data('dialog-title'),
                        modal: true,
                        resizable: true,
                        show: "blind",
                        hide: "blind",
                        draggable: true,
                        position: ['center', 20],
                        width: elementTag.data('dialog-width') || 600,
                        close: function () {

                            $(this).remove();

                        },
                        open: function () {

                            $(this).find('.uiButton').button();
                        }
                    })
                    .find('form') // Attach logic on forms
                    .end();
            });

}



function formValidator($form) {
    if ($form.length > 0) {
        $form.unbind();
        $form.data("validator", null);
        $.validator.unobtrusive.parse(document);
        $form.validate($form.data("unobtrusiveValidation").options);
        $("input:submit").button();
    }
}

/* Get the rows which are currently selected */

function fnGetSelected(oTableLocal) {
    var aReturn = new Array();
    var aTrs = oTableLocal.fnGetNodes();

    for (var i = 0; i < aTrs.length; i++) {
        if ($(aTrs[i]).hasClass('row_selected')) {
            aReturn.push(aTrs[i]);
        }
    }
    return aReturn;
}
