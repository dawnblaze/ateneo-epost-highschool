﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    EmployeeDT = $('#EmployeeDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Employee/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "EmployeeId", "bSortable": false, "bVisible": false },
            { "mData": "EmployeeNumber", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            { "mData": "EmployeeType.EmployeeTypeName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.Gender.GenderName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.Age", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Person.LastName", "bSearchable": true, "bVisible": false },
            { "mData": "Person.FirstName", "bSearchable": true, "bVisible": false },
            { "mData": "Person.MiddleName", "bSearchable": true, "bVisible": false },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<a href="Person/Detail/' + oObj.aData["PersonId"] + '" title="Click this Icon to View Person Details" class="link ui-icon ui-icon-person">' + oObj.aData["FullName"] + '</a>';
                }
            },
            {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Edit Employee" title="Click this Icon to Edit Employee " data-url="Employee/Edit/' + oObj.aData["EmployeeId"] + '" data-id="EditEmployeeDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
            }
        },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Employee" title="Click this Icon to Delete Employee" data-url="Employee/Delete/' + oObj.aData["EmployeeId"] + '" data-id="DeleteEmployeeDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#EmployeeDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#EmployeeDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});