﻿$(function () {
    var EditEmployeeForm = $("#EditEmployeeForm");
    $(EditEmployeeForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditEmployeeForm, json.message);
            d["EditEmployeeDialog"].dialog('close');
            EmployeeDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


