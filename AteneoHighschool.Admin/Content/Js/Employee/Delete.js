﻿$(function () {
    var DeleteEmployeeForm = $("#DeleteEmployeeForm");
    $(DeleteEmployeeForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteEmployeeDialog"].dialog('close');
            EmployeeDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


