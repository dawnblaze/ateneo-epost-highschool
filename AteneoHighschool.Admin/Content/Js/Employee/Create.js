﻿$(function () {
    var CreateEmployeeForm = $("#CreateEmployeeForm");
    $(CreateEmployeeForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateEmployeeForm);
            displaySuccessMessage(CreateEmployeeForm, json.message);
            EmployeeDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});