﻿$(function () {
    var DeleteCurriculumForm = $("#DeleteCurriculumForm");
    $(DeleteCurriculumForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteCurriculumDialog"].dialog('close');
            CurriculumDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


