﻿$(function () {
    var EditCurriculumForm = $("#EditCurriculumForm");
    $(EditCurriculumForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditCurriculumForm, json.message);
            d["EditCurriculumDialog"].dialog('close');
            CurriculumDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


