﻿$(function () {
    var CreateCurriculumForm = $("#CreateCurriculumForm");
    $(CreateCurriculumForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateCurriculumForm);
            displaySuccessMessage(CreateCurriculumForm, json.message);
            CurriculumDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});