﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    CurriculumDT = $('#CurriculumDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Curriculum/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "CurriculumId", "bSortable": false, "bVisible": false },
            { "mData": "CurriculumName", "bSearchable": true, "bVisible": true, "sWidth": "50%" },
            { "mData": "Description", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            { "mData": "AcademicYear.AcademicYearName", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            { "mData": "Course.CourseName", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            {
                "mData": null,
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['StartDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            {
                "mData": null,
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['EndDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    //return '<span data-dialog-title="Framework Detail" title="Click this Icon to View Framework Details" data-url="Framework/Detail/' + oObj.aData["FrameworkId"] + '" class="link ui-icon ui-icon-info" />';
                    return '<a href="Curriculum/Detail/' + oObj.aData["CurriculumId"] + '" title="Click this Icon to View Curriculum Details" class="link ui-icon ui-icon-info">' + oObj.aData["CurriculumName"] + '</a>';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Curriculum" title="Click this Icon to Edit Curriculum" data-url="Curriculum/Edit/' + oObj.aData["CurriculumId"] + '" data-id="EditCurriculumDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Curriculum" title="Click this Icon to Delete Curriculum" data-url="Curriculum/Delete/' + oObj.aData["CurriculumId"] + '" data-id="DeleteCurriculumDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#CurriculumDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#CurriculumDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});