﻿$(function () {
    var DeleteCampusForm = $("#DeleteCampusForm");
    $(DeleteCampusForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteCampusDialog"].dialog('close');
            CampusDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


