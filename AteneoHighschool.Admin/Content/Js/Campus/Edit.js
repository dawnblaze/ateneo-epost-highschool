﻿$(function () {
    var EditCampusForm = $("#EditCampusForm");
    $(EditCampusForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditCampusForm, json.message);
            d["EditCampusDialog"].dialog('close');
            CampusDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


