﻿$(function () {
    var CreateCampusForm = $("#CreateCampusForm");
    $(CreateCampusForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateCampusForm);
            displaySuccessMessage(CreateCampusForm, json.message);
            CampusDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});