﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    CampusDT = $('#CampusDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Campus/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "CampusId", "bSortable": false, "bVisible": false },
            { "mData": "CampusCode", "bSearchable": true, "bVisible": true, "sWidth": "10%" },
            { "mData": "CampusName", "bSearchable": true, "bVisible": true, "sWidth": "30%" },
            { "mData": "School.SchoolName", "bSearchable": true, "bVisible": true, "sWidth": "30%" },
            { "mData": "Description", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            { "mData": "Location", "bSearchable": true, "bVisible": true, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Campus" title="Click this Icon to Edit Campus" data-url="Campus/Edit/' + oObj.aData["CampusId"] + '" data-id="EditCampusDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Campus" title="Click this Icon to Delete Campus" data-url="Campus/Delete/' + oObj.aData["CampusId"] + '" data-id="DeleteCampusDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#CampusDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#CampusDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});