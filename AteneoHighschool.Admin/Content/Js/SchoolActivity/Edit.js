﻿$(function () {
    var EditSchoolActivityForm = $("#EditSchoolActivityForm");
    $(EditSchoolActivityForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditSchoolActivityForm, json.message);
            d["EditSchoolActivityDialog"].dialog('close');
            SchoolActivityDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


