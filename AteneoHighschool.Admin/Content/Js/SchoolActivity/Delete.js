﻿$(function () {
    var DeleteSchoolActivityForm = $("#DeleteSchoolActivityForm");
    $(DeleteSchoolActivityForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteSchoolActivityDialog"].dialog('close');
            SchoolActivityDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


