﻿$(function () {
    var CreateSchoolActivityForm = $("#CreateSchoolActivityForm");
    $(CreateSchoolActivityForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateSchoolActivityForm);
            displaySuccessMessage(CreateSchoolActivityForm, json.message);
            SchoolActivityDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});