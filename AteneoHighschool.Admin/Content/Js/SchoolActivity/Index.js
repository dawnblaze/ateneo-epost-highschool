﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    SchoolActivityDT = $('#SchoolActivityDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/SchoolActivity/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "SchoolActivityId", "bSortable": false, "bVisible": false },
            { "mData": "SchoolActivityName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            { "mData": "Description", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            { "mData": "AcademicYear.AcademicYearName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "sWidth": "10%",
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['StartDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "sWidth": "10%",
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['EndDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit School Activity" title="Click this Icon to Edit School Activity" data-url="SchoolActivity/Edit/' + oObj.aData["SchoolActivityId"] + '" data-id="EditSchoolActivityDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete School Activity" title="Click this Icon to Delete School Activity" data-url="SchoolActivity/Delete/' + oObj.aData["SchoolActivityId"] + '" data-id="DeleteSchoolActivityDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#SchoolActivityDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#SchoolActivityDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});