﻿$(function () {
    var EditCivilStatusForm = $("#EditCivilStatusForm");
    $(EditCivilStatusForm).submit(function (e) {
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditCivilStatusForm, json.message);
            d["EditCivilStatusDialog"].dialog('close');
            CivilStatusDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});
