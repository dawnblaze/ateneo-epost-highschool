﻿$(function () {
    var DeleteCivilStatusForm = $("#DeleteCivilStatusForm");
    $(DeleteCivilStatusForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteCivilStatusDialog"].dialog('close');
            CivilStatusDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


