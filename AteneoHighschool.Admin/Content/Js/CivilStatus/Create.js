﻿$(function () {
    var CreateCivilStatusForm = $("#CreateCivilStatusForm");
    $(CreateCivilStatusForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateCivilStatusForm);
            displaySuccessMessage(CreateCivilStatusForm, json.message);
            CivilStatusDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});