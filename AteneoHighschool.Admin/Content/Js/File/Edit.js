﻿$(function () {
    var EditFileForm = $("#EditFileForm");
    $(EditFileForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditFileForm, json.message);
            d["EditFileDialog"].dialog('close');
            FileDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


