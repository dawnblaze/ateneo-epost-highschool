﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    FileDT = $('#FileDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/File/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "FileId", "bSortable": false, "bVisible": false },
            { "mData": "FileName", "bSearchable": true, "bVisible": true, "sWidth": "50%" },
            { "mData": "MimeType", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            {
                "mData": null,
                "fnRender": function (oObj) {
                    var dateGraduated = new Date(parseInt(oObj.aData['UploadDate'].replace("/Date(", "").replace(")/", ""), 10));
                    return dateGraduated.getMonth() + 1 + "/" + dateGraduated.getDate() + "/" + dateGraduated.getFullYear();
                }
            },
            { "mData": "Status", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit File" title="Click this Icon to Edit File" data-url="File/Edit/' + oObj.aData["FileId"] + '" data-id="EditFileDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete File" title="Click this Icon to Delete File" data-url="File/Delete/' + oObj.aData["FileId"] + '" data-id="DeleteFileDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#FileDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#FileDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});