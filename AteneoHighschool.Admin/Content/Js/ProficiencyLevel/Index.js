﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    ProficiencyLevelDT = $('#ProficiencyLevelDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/ProficiencyLevel/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "ProficiencyLevelId", "bSortable": false, "bVisible": false },
            { "mData": "ProficiencyLevelName", "bSearchable": true, "sWidth": "10%" },
            { "mData": "Description", "bSearchable": true, "sWidth": "10%" },
            { "mData": "Sequence", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Category.CategoryName", "bSortable": true, "bSearchable": true, "sWidth": "10%" },
            {
            "mData": null,
            "bSearchable": false,
            "bSortable": false,
            "sWidth": "5%",
            "fnRender": function (oObj) {
                return '<span data-dialog-title="Edit Proficiency Level" title="Click this Icon to Edit Proficiency Level " data-url="ProficiencyLevel/Edit/' + oObj.aData["ProficiencyLevelId"] + '" data-id="EditProficiencyLevelDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
            }
        },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Proficiency Level" title="Click this Icon to Delete Proficiency Level" data-url="ProficiencyLevel/Delete/' + oObj.aData["ProficiencyLevelId"] + '" data-id="DeleteProficiencyLevelDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#ProficiencyLevelDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#ProficiencyLevelDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});