﻿$(function () {
    var EditProficiencyLevelForm = $("#EditProficiencyLevelForm");
    $(EditProficiencyLevelForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditProficiencyLevelForm, json.message);
            d["EditProficiencyLevelDialog"].dialog('close');
            ProficiencyLevelDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


