﻿$(function () {
    var CreateProficiencyLevelForm = $("#CreateProficiencyLevelForm");
    $(CreateProficiencyLevelForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateProficiencyLevelForm);
            displaySuccessMessage(CreateProficiencyLevelForm, json.message);
            ProficiencyLevelDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});