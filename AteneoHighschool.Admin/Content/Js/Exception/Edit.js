﻿$(function () {
    var EditExceptionForm = $("#EditExceptionForm");
    $(EditExceptionForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditExceptionForm, json.message);
            d["EditExceptionDialog"].dialog('close');
            ExceptionDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


