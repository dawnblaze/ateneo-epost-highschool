﻿$(function () {
    var CreateExceptionForm = $("#CreateExceptionForm");
    $(CreateExceptionForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateExceptionForm);
            displaySuccessMessage(CreateExceptionForm, json.message);
            ExceptionDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});