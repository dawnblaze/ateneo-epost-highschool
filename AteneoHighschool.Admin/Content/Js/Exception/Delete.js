﻿$(function () {
    var DeleteExceptionForm = $("#DeleteExceptionForm");
    $(DeleteExceptionForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteExceptionDialog"].dialog('close');
            ExceptionDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


