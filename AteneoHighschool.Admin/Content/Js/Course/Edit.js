﻿$(function () {
    var EditCourseForm = $("#EditCourseForm");
    $(EditCourseForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditCourseForm, json.message);
            d["EditCourseDialog"].dialog('close');
            CourseDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


