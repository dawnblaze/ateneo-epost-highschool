﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    CourseDT = $('#CourseDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/Course/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "CourseId", "bSortable": false, "bVisible": false },
            { "mData": "CourseCode", "bSearchable": true, "bVisible": true, "sWidth": "10%" },
            { "mData": "CourseName", "bSearchable": true, "bVisible": true, "sWidth": "30%" },
            { "mData": "Description", "bSearchable": false, "bVisible": true, "sWidth": "10%" },
            { "mData": "Campus.CampusName", "bSearchable": true, "bVisible": true, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Course" title="Click this Icon to Edit Course" data-url="Course/Edit/' + oObj.aData["CourseId"] + '" data-id="EditCourseDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Course" title="Click this Icon to Delete Course" data-url="Course/Delete/' + oObj.aData["CourseId"] + '" data-id="DeleteCourseDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#CourseDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#CourseDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});