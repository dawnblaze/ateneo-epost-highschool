﻿$(function () {
    var CreateCourseForm = $("#CreateCourseForm");
    $(CreateCourseForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateCourseForm);
            displaySuccessMessage(CreateCourseForm, json.message);
            CourseDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});