﻿$(function () {
    var DeleteCourseForm = $("#DeleteCourseForm");
    $(DeleteCourseForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteCourseDialog"].dialog('close');
            CourseDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


