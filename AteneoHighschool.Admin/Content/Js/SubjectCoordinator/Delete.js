﻿$(function () {
    var DeleteSubjectCoordinatorForm = $("#DeleteSubjectCoordinatorForm");
    $(DeleteSubjectCoordinatorForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            d["DeleteSubjectCoordinatorDialog"].dialog('close');
            SubjectCoordinatorDT.fnDraw(true);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
            alert("error");
        }
    });
});


