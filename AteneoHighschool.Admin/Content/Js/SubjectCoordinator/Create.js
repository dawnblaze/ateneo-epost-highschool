﻿$(function () {
    var CreateSubjectCoordinatorForm = $("#CreateSubjectCoordinatorForm");
    $(CreateSubjectCoordinatorForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            resetForm(CreateSubjectCoordinatorForm);
            displaySuccessMessage(CreateSubjectCoordinatorForm, json.message);
            SubjectCoordinatorDT.fnDraw();
        } else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});