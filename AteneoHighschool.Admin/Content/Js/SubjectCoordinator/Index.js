﻿$(document).ready(function () {
    //---------------------------------------------------------------------------
    // Initialize the table
    //---------------------------------------------------------------------------
    SubjectCoordinatorDT = $('#SubjectCoordinatorDT').dataTable({
        // "sDom": 'l<"toolbar">frtip',
        /* "sScrollY": "100px",*/
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true, //we are going to use bServerSide if the no. of data to be retrieved is greater than 100
        "sAjaxSource": "/SubjectCoordinator/GetListJson",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "sType": "numeric", "mData": "SubjectCoordinatorId", "bSortable": false, "bVisible": false },
            { "mData": "Curriculum.CurriculumName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            { "mData": "Employee.Person.FullName", "bSortable": true, "bSearchable": false, "sWidth": "10%" },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Edit Subject Coordinator" title="Click this Icon to Edit Subject Coordinator" data-url="SubjectCoordinator/Edit/' + oObj.aData["SubjectCoordinatorId"] + '" data-id="EditSubjectCoordinatorDialog" data-process-button="Update" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-pencil" />';
                }
            },
            {
                "mData": null,
                "bSearchable": false,
                "bSortable": false,
                "sWidth": "5%",
                "fnRender": function (oObj) {
                    return '<span data-dialog-title="Delete Subject Coordinator" title="Click this Icon to Delete Subject Coordinator" data-url="SubjectCoordinator/Delete/' + oObj.aData["SubjectCoordinatorId"] + '" data-id="DeleteSubjectCoordinatorDialog" data-process-button="Delete" onclick="loadAndShowDialogReload($(this))" class="link ui-icon ui-icon-trash"/>';
                }
            }
        ],
        "fnDrawCallback": function () {
            $('table#SubjectCoordinatorDT td').bind('mouseenter', function () { $(this).parent().children().each(function () { $(this).addClass('row-highlight'); }); });
            $('table#SubjectCoordinatorDT td').bind('mouseleave', function () { $(this).parent().children().each(function () { $(this).removeClass('row-highlight'); }); });
        }
    });
});