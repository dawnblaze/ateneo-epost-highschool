﻿$(function () {
    var EditSubjectCoordinatorForm = $("#EditSubjectCoordinatorForm");
    $(EditSubjectCoordinatorForm).submit(function (e) { //listen for submit event
        e.preventDefault();
        var json = formSubmitHandler($(this));
        if (json.success) {
            displaySuccessMessage(EditSubjectCoordinatorForm, json.message);
            d["EditSubjectCoordinatorDialog"].dialog('close');
            SubjectCoordinatorDT.fnDraw(false);
        }
        else if (json.error) {
            displayError($(this), json.error);
        }
        else {
        }
    });
});


