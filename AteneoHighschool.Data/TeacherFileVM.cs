﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class TeacherFileVM
    {
        public TeacherFile TeacherFile { get; set; }

        public EmployeeVM EmployeeVM { get; set; }

        public FileVM FileVM { get; set; }

        public SelectList TeacherFileList { get; set; }
    }
}
