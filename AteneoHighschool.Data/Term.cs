﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Term
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid TermId { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Term")]
        public string TermName { get; set; }

        public string Description { get; set; }

        public Guid TermTypeId { get; set; }
        public TermType TermType { get; set; }

        public Guid AcademicYearId { get; set; }
        public AcademicYear AcademicYear { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        public bool IsActive { get; set; }
    }
}
