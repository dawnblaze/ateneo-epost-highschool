﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class File
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid FileId { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "File Name")]
        public string FileName { get; set; }

        public string Description { get; set; }

        [Required]
        public string MimeType { get; set; }

        [Required]
        public byte[] Data { get; set; }

        public DateTime UploadDate { get; set; }

        public string Status { get; set; }
    }
}
