﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class AcademicYearVM
    {
        public AcademicYear AcademicYear { get; set; }

        public SelectList AcademicYearList { get; set; }
    }
}
