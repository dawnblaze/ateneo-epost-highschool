﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class UserRole
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid UserRoleId { get; set; }

        [Required]
        public Guid UserId { get; set; }
        public User User { get; set; }

        public string UserName { get; set; }

        [Required]
        public Guid RoleId { get; set; }
        public Role Role { get; set; }

        public string RoleName { get; set; }

        [Required]
        public bool IsActive { get; set; }

    }

}