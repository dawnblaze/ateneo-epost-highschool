﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class PersonVM
    {
        public Person Person { get; set; }

        public GenderVM GenderVM { get; set; }

        public CivilStatusVM CivilStatusVM { get; set; }

        public PrefixVM PrefixVM { get; set; }

        public SelectList PersonList { get; set; }
    }
}
