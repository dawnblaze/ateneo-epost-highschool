﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class ParentNotificationSlipVM
    {
        public ParentNotificationSlip ParentNotificationSlip { get; set; }

        public ParentVM ParentVM { get; set; }

        public StudentVM StudentVM { get; set; }

        public CurriculumVM CurriculumVM { get; set; }

        public CurriculumClassVM CurriculumClassVM { get; set; }

        public SelectList ParentNotificationSlipList { get; set; }
    }
}
