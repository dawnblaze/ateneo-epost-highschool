﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class PathwayItemFile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid PathwayItemFileId { get; set; }

        public Guid PathwayItemId { get; set; }
        public PathwayItem PathwayItem { get; set; }

        public Guid CurriculumClassId { get; set; }
        public CurriculumClass CurriculumClass { get; set; }

        public Guid FileId { get; set; }
        public File File { get; set; }
    }
}
