﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class RecurrenceVM
    {
        public Recurrence Recurrence { get; set; }

        public CurriculumClassVM CurriculumClassVM { get; set; }

        public ScheduleVM ScheduleVM { get; set; }

        public SelectList RecurrenceList { get; set; }
    }
}
