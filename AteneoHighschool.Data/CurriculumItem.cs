﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class CurriculumItem
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid CurriculumItemId { get; set; }

        [Required]
        [Display(Name = "Topic")]
        public string CurriculumItemName { get; set; }

        [Required]
        public string Description { get; set; }
        
        public int? Units { get; set; }

        [Required]
        public int Sequence { get; set; }

        public Guid CurriculumId { get; set; }
        public Curriculum Curriculum { get; set; }

        public Guid ProficiencyLevelId { get; set; }
        public ProficiencyLevel ProficiencyLevel { get; set; }
    }
}
