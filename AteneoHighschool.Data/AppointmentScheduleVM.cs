﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class AppointmentScheduleVM
    {
        public AppointmentSchedule AppointmentSchedule { get; set; }

        public EmployeeVM EmployeeVM { get; set; }

        public ScheduleVM ScheduleVM { get; set; }

        public SelectList AppointmentScheduleList { get; set; }
    }
}
