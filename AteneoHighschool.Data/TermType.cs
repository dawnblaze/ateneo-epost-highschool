﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class TermType
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid TermTypeId { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Term Type")]
        public string TermTypeName { get; set; }
    }
}
