﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class ParentNotificationSlip
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid ParentNotificationSlipId { get; set; }

        public Guid SenderId { get; set; }
        public string SenderName { get; set; }
        public Guid RecipientId { get; set; }
        public Parent Parent { get; set; }

        public Guid StudentId { get; set; }
        public Student Student { get; set; }

        public Guid CurriculumId { get; set; }
        public Curriculum Curriculum { get; set; }

        public Guid CurriculumClassId { get; set; }
        public CurriculumClass CurriculumClass { get; set; }

        public string MessageContent { get; set; }

        public DateTime SentDate { get; set; }

        public bool IsRead { get; set; }

        public DateTime? ReadDate { get; set; }
    }
}
