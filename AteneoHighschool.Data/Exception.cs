﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Exception
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid ExceptionId { get; set; }

        [Required]
        [Display(Name = "Exception Name")]
        public string ExceptionName { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid AcademicYearId { get; set; }
        public AcademicYear AcademicYear { get; set; }

        //public Guid ExceptionTypeId { get; set; }
        //public ExceptionType ExceptionType { get; set; }
    }
}
