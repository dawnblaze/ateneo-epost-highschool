﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class ClassTeacherVM
    {
        public ClassTeacher ClassTeacher { get; set; }

        public CurriculumClassVM CurriculumClassVM { get; set; }

        public EmployeeVM EmployeeVM { get; set; }

        public SelectList ClassTeacherList { get; set; }
    }
}
