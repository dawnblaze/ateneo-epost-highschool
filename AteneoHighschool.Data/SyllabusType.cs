﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class SyllabusType
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid SyllabusTypeId { get; set; }

        [Required]
        [Display(Name = "Syllabus Type Name")]
        public string SyllabusTypeName { get; set; }
    }
}
