﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace AteneoHighschool.Data
{
    public class CurriculumClass
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid CurriculumClassId { get; set; }

        [Display(Name = "Class Name")]
        public string CurriculumClassName { get; set; }

        public Guid GroupId { get; set; }
        public Group Group { get; set; }

        public Guid CurriculumId { get; set; }
        public Curriculum Curriculum { get; set; }

        public Guid AcademicYearId { get; set; }
        public AcademicYear AcademicYear { get; set; }
    }
}
