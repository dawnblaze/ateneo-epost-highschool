﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class SchoolVM
    {
        public School School { get; set; }

        public SelectList SchoolList { get; set; }
    }
}
