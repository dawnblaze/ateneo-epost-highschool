﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class StudentVM
    {
        public Student Student { get; set; }

        public PersonVM PersonVM { get; set; }

        public SelectList StudentList { get; set; }
    }
}
