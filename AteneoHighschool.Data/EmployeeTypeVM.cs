﻿using System.Web.Mvc;


namespace AteneoHighschool.Data
{
    public class EmployeeTypeVM
    {
        public EmployeeType EmployeeType { get; set; }

        public SelectList EmployeeTypeList { get; set; }
    }
}
