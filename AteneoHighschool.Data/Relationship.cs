﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Relationship
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid RelationshipId { get; set; }

        public Guid ParentId { get; set; }
        public Parent Parent { get; set; }

        public Guid StudentId { get; set; }
        public Student Student { get; set; }
    }
}
