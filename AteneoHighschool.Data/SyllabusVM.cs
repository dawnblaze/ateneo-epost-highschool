﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class SyllabusVM
    {
        public Syllabus Syllabus { get; set; }

        public SyllabusTypeVM SyllabusTypeVM { get; set; }

        public CurriculumClassVM CurriculumClassVM { get; set; }

        public CurriculumVM CurriculumVM { get; set; }

        public CurriculumItemVM CurriculumItemVM { get; set; }

        public AcademicYearVM AcademicYearVM { get; set; }

        public SelectList SyllabusList { get; set; }
    }
}
