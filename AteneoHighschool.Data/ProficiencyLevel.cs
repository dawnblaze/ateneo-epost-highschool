﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class ProficiencyLevel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid ProficiencyLevelId { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Proficiency Level")]
        public string ProficiencyLevelName { get; set; }

        public string Description { get; set; }

        public int Sequence { get; set; }

        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
