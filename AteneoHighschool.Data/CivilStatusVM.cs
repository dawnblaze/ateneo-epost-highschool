﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class CivilStatusVM
    {
        public CivilStatus CivilStatus { get; set; }

        public SelectList CivilStatusList { get; set; }
    }
}
