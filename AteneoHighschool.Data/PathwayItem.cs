﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AteneoHighschool.Data.Enum;

namespace AteneoHighschool.Data
{
    public class PathwayItem
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid PathwayItemId { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public Guid CurriculumId { get; set; }
        public Curriculum Curriculum { get; set; }
        //public string CurriculumName { get; set; }

        public Guid AcademicYearId { get; set; }
        public AcademicYear AcademicYear { get; set; }
        //public string AcademicYearName { get; set; }

        public Guid CurriculumClassId { get; set; }
        public CurriculumClass CurriculumClass { get; set; }
        //public string CurriculumClassName { get; set; }

        [Display(Name = "Type")]
        public PathwayItemType? PathwayItemType { get; set; }

        public bool IsActive { get; set; }
    }
}
