﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class GenderVM
    {
        public Gender Gender { get; set; }

        public SelectList GenderList { get; set; }
    }
}
