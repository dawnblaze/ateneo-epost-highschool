﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Gender
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid GenderId { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Gender")]
        public string GenderName { get; set; }
    }
}
