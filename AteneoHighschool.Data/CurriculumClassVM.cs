﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class CurriculumClassVM
    {
        public CurriculumClass CurriculumClass { get; set; }

        public GroupVM GroupVM { get; set; }

        public CurriculumVM CurriculumVM { get; set; }

        public AcademicYearVM AcademicYearVM { get; set; }

        public SelectList CurriculumClassList { get; set; }
    }
}
