﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class UserRoleVM
    {
        public UserRole UserRole { get; set; }

        public RoleVM RoleVM { get; set; }

        public SelectList UserRoleList { get; set; }
    }
}
