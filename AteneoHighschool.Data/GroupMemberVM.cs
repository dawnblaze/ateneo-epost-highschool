﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class GroupMemberVM
    {
        public GroupMember GroupMember { get; set; }

        public GroupVM GroupVM { get; set; }

        public StudentVM StudentVM { get; set; }

        public SelectList GroupMemberList { get; set; }
    }
}
