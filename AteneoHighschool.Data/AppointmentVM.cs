﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class AppointmentVM
    {
        public Appointment Appointment { get; set; }

        public EmployeeVM EmployeeVM { get; set; }

        public SelectList AppointmentList { get; set; }
    }
}
