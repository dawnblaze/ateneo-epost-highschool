﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class GroupAdviserVM
    {
        public GroupAdviser GroupAdviser { get; set; }

        public EmployeeVM EmployeeVM { get; set; }

        public GroupVM GroupVM { get; set; }

        public SelectList GroupAdviserList { get; set; }
    }
}
