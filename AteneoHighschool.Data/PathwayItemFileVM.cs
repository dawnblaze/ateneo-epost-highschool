﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class PathwayItemFileVM
    {
        public PathwayItemFile PathwayItemFile { get; set; }

        public PathwayItemVM PathwayItemVM { get; set; }

        public CurriculumClassVM CurriculumClassVM { get; set; }

        public FileVM FileVM { get; set; }

        public SelectList PathwayItemFileList { get; set; }
    }
}
