﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class TermVM
    {
        public Term Term { get; set; }

        public TermTypeVM TermTypeVM { get; set; }

        public AcademicYearVM AcademicYearVM { get; set; }

        public SelectList TermList { get; set; }
    }
}
