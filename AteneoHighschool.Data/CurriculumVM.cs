﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class CurriculumVM
    {
        public Curriculum Curriculum { get; set; }

        public AcademicYearVM AcademicYearVM { get; set; }

        public CourseVM CourseVM { get; set; }

        public SelectList CurriculumList { get; set; }
    }
}
