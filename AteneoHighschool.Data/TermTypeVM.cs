﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class TermTypeVM
    {
        public TermType TermType { get; set; }

        public SelectList TermTypeList { get; set; }
    }
}
