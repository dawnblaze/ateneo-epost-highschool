﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class PrefixVM
    {
        public Prefix Prefix { get; set; }

        public SelectList PrefixList { get; set; }
    }
}
