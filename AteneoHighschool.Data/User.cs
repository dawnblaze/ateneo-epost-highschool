﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AteneoHighschool.Data
{
    public class User
    {
        [Key]
        public Guid UserId { get; set; }

        [Required]
        [StringLength(30)]
        public string UserName { get; set; }

        [Required]
        [StringLength(80)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public int? PhoneNumber { get; set; }

        //for more security
        public int AccessFailedCount { get; set; }
        public DateTime? LockOutDate { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        public bool IsLockedOut { get; set; }

        public bool IsConfirmed { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public string Comment { get; set; }
    }

}