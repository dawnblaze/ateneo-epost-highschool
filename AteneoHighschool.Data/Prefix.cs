﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Prefix
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid PrefixId { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name = "Prefix")]
        public string PrefixName { get; set; }
    }
}
