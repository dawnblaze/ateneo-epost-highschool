﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class ProficiencyLevelVM
    {
        public ProficiencyLevel ProficiencyLevel { get; set; }

        public CategoryVM CategoryVM { get; set; }

        public SelectList ProficiencyLevelList { get; set; }
    }
}
