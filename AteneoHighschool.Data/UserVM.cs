﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class UserVM
    {
        public User User { get; set; }

        public SelectList UserList { get; set; }
    }

}