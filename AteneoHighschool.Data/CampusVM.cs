﻿using System.Web.Mvc;


namespace AteneoHighschool.Data
{
    public class CampusVM
    {
        public Campus Campus { get; set; }

        public SchoolVM SchoolVM { get; set; }

        public SelectList CampusList { get; set; }
    }
}
