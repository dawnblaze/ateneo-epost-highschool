﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class RelationshipVM
    {
        public Relationship Relationship { get; set; }

        public ParentVM ParentVM { get; set; }

        public StudentVM StudentVM { get; set; }

        public SelectList RelationshipList { get; set; }
    }
}
