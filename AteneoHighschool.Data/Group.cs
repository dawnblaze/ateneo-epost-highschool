﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Group
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid GroupId { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Group Name")]
        public string GroupName { get; set; }

        public string Description { get; set; }

        public Guid? EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public Guid ProficiencyLevelId { get; set; }
        public ProficiencyLevel ProficiencyLevel { get; set; }
    }
}
