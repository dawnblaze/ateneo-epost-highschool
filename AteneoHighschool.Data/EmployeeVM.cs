﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class EmployeeVM
    {
        public Employee Employee { get; set; }

        public PersonVM PersonVM { get; set; }

        public EmployeeTypeVM EmployeeTypeVM { get; set; }

        public SelectList EmployeeList { get; set; }
    }
}
