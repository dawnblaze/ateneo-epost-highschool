﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class FileVM
    {
        public File File { get; set; }

        public SelectList FileList { get; set; }
    }
}
