﻿using System.Web.Mvc;


namespace AteneoHighschool.Data
{
    public class SyllabusTypeVM
    {
        public SyllabusType SyllabusType { get; set; }

        public SelectList SyllabusTypeList { get; set; }
    }
}
