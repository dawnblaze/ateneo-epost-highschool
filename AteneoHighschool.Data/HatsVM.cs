﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class HatsVM
    {
        public Hats Hats { get; set; }

        public CurriculumClassVM CurriculumClassVM { get; set; }

        public PathwayItemVM PathwayItemVM { get; set; }

        public SelectList HatsList { get; set; }
    }
}
