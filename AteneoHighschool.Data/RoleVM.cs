﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class RoleVM
    {
        public Role Role { get; set; }

        public SelectList RoleList { get; set; }
    }
}
