﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace AteneoHighschool.Data
{
    public class School
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid SchoolId { get; set; }

        [Display(Name = "School Code")]
        public string SchoolCode { get; set; }

        [Required]
        [Display(Name = "School Name")]
        public string SchoolName { get; set; }

        public string Description { get; set; }
    }
}
