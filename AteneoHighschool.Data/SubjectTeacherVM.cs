﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class SubjectTeacherVM
    {
        public SubjectTeacher SubjectTeacher { get; set; }

        public EmployeeVM EmployeeVM { get; set; }

        public CurriculumVM CurriculumVM { get; set; }

        public SelectList SubjectTeacherList { get; set; }
    }
}
