﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class ScheduleVM
    {
        public Schedule Schedule { get; set; }

        public SelectList ScheduleList { get; set; }
    }
}
