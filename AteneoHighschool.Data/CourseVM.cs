﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class CourseVM
    {
        public Course Course { get; set; }

        public CampusVM CampusVM { get; set; }

        public SelectList CourseList { get; set; }
    }
}
