﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class CategoryVM
    {
        public Category Category { get; set; }

        public SelectList CategoryList { get; set; }
    }
}
