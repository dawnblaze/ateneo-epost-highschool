﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class SchoolActivityVM
    {
        public SchoolActivity SchoolActivity { get; set; }

        public AcademicYearVM AcademicYearVM { get; set; }

        public SelectList SchoolActivityList { get; set; }
    }
}
