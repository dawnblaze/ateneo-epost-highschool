﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Campus
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid CampusId { get; set; }

        public string CampusCode { get; set; }

        [Required]
        [Display(Name = "Campus Name")]
        public string CampusName { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public Guid SchoolId { get; set; }
        public School School { get; set; }
    }
}
