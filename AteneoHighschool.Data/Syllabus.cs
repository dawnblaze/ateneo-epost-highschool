﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Syllabus
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid SyllabusId { get; set; }

        [Required]
        [Display(Name = "Syllabus Name")]
        public string SyllabusName { get; set; }

        [Required]
        public string Description { get; set; }

        public Guid SyllabusTypeId { get; set; }
        public SyllabusType SyllabusType { get; set; }

        public Guid CurriculumClassId { get; set; }
        public CurriculumClass CurriculumClass { get; set; }

        public Guid CurriculumId { get; set; }
        public Curriculum Curriculum { get; set; }

        public Guid CurriculumItemId { get; set; }
        public CurriculumItem CurriculumItem { get; set; }

        public Guid AcademicYearId { get; set; }
        public AcademicYear AcademicYear { get; set; }

        public DateTime StarDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
