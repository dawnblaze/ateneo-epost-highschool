﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class LevelLeaderVM
    {
        public LevelLeader LevelLeader { get; set; }

        public EmployeeVM EmployeeVM { get; set; }

        public ProficiencyLevelVM ProficiencyLevelVM { get; set; }

        public SelectList LevelLeaderList { get; set; }
    }
}
