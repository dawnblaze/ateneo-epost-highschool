﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Employee
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid EmployeeId { get; set; }

        [Required]
        [Display(Name = "Employee Number")]
        public string EmployeeNumber { get; set; }

        public Guid PersonId { get; set; }
        public Person Person { get; set; }

        public Guid EmployeeTypeId { get; set; }
        public EmployeeType EmployeeType { get; set; }
    }
}
