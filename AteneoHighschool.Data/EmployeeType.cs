﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class EmployeeType
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid EmployeeTypeId { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Employee Type")]
        public string EmployeeTypeName { get; set; }
    }
}
