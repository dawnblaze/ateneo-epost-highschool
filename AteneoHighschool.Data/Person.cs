﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class Person
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid PersonId { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [StringLength(50)]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string FullName { get; set; }
        public string Suffix { get; set; }

        public Guid GenderId { get; set; }
        public Gender Gender { get; set; }

        public int Age
        {
            get { return Convert.ToInt32(DateTime.Now.Year - BirthDate.Year); }
        }

        [Required]
        [Display(Name = "Birth Date")]
        public DateTime BirthDate { get; set; }

        public Guid CivilStatusId { get; set; }
        public CivilStatus CivilStatus { get; set; }

        public Guid PrefixId { get; set; }
        public Prefix Prefix { get; set; }

    }
}
