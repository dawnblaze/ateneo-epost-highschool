﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class CurriculumItemVM
    {
        public CurriculumItem CurriculumItem { get; set; }

        public CurriculumVM CurriculumVM { get; set; }

        public ProficiencyLevelVM ProficeLevelVM { get; set; }

        public SelectList CurriculumItemList { get; set; }
    }
}
