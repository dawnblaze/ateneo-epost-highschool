﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class PathwayItemVM
    {
        public PathwayItem PathwayItem { get; set; }

        public CurriculumVM CurriculumVM { get; set; }

        public AcademicYearVM AcademicYearVM { get; set; }

        public CurriculumClassVM CurriculumClassVM { get; set; }

        public SelectList PathwayItemList { get; set; }
    }
}
