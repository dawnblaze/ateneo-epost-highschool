﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class SubjectCoordinatorVM
    {
        public SubjectCoordinator SubjectCoordinator { get; set; }

        public EmployeeVM EmployeeVM { get; set; }

        public CurriculumVM CurriculumVM { get; set; }

        public SelectList SubjectCoordinatorList { get; set; }
    }
}
