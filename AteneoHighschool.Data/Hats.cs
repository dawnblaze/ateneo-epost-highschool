﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AteneoHighschool.Data.Enum;

namespace AteneoHighschool.Data
{
    public class Hats
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid HatsId { get; set; }

        public HatsType HatsType { get; set; }
        public PathwayItemType PathwayItemType { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }

        public Guid CurriculumClassId { get; set; }
        public CurriculumClass CurriculumClass { get; set; }

        public Guid PathwayItemId { get; set; }
        public PathwayItem PathwayItem { get; set; }
    }
}
