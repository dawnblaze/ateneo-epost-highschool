﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AteneoHighschool.Data
{
    public class CivilStatus
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid CivilStatusId { get; set; }

        [Required]
        [StringLength(40)]
        [Display(Name = "Civil Status")]
        public string CivilStatusName { get; set; }
    }
}
