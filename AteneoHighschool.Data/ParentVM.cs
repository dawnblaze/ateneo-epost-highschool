﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class ParentVM
    {
        public Parent Parent { get; set; }

        public PersonVM PersonVM { get; set; }

        public SelectList ParentList { get; set; }
    }
}
