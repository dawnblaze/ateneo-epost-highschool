﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class GroupVM
    {
        public Group Group { get; set; }

        public EmployeeVM EmployeeVM { get; set; }

        public ProficiencyLevelVM ProficiencyLevelVM { get; set; }

        public SelectList GroupList { get; set; }
    }
}
