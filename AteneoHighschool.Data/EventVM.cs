﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class AnnouncementVM
    {
        public Announcement Announcement { get; set; }

        public AcademicYearVM AcademicYearVM { get; set; }

        public SelectList AnnouncementList { get; set; }
    }
}
