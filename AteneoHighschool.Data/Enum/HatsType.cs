﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AteneoHighschool.Data.Enum
{
    [Flags]
    public enum HatsType
    {
        LongTest = 1,
        MasteryTest = 1 << 1,
        Quiz = 1 << 2,
        ProductTask = 1 << 3,
        WrittenHomework = 1 << 4,
        LibraryWork = 1 << 5,
        ClassEvents = 1 << 6,
    }
}
