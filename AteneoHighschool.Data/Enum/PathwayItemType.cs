﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AteneoHighschool.Data.Enum
{
    [Flags]
    public enum PathwayItemType
    {
        Experiment = 1,
        Seminar = 1 << 1,
        Training = 1 << 2,
        Camp = 1 << 3,
        Conference = 1 << 4,
        Workshop = 1 << 5,
        NonSchoolSanctioned = 1 << 6,
    }
}
