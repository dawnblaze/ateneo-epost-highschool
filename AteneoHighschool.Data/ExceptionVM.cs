﻿using System.Web.Mvc;

namespace AteneoHighschool.Data
{
    public class ExceptionVM
    {
        public Exception Exception { get; set; }

        public AcademicYearVM AcademicYearVM { get; set; }

        //public ExceptionTypeVM ExceptionTypeVM { get; set; }

        public SelectList ExceptionList { get; set; }
    }
}
