﻿using System.Web;
using System.Web.Mvc;

namespace AteneoHighschool.Presentation.GenericMVC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}